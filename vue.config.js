const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const isProduction = process.env.NODE_ENV === 'production';

function resolve(dir) {
    return path.resolve(__dirname, dir);
}

module.exports = {
    lintOnSave: false,
    outputDir: resolve('public/'),
    publicPath: '',
    configureWebpack: {
        context: resolve(''),
        devServer: {
            compress: true,
            hot: true
        },
        entry: {
            app: './src/main.js',
            preloader: './src/preloader.js'
        },
        optimization: {
            minimize: false,
            concatenateModules: false
        },
        resolve: {
            modules: ['src/libs', 'node_modules'],
            extensions: ['.js', '.vue', '.json'],
            alias: {
                'vue$': 'vue/dist/vue.esm.js',
                '@src': resolve('src/'),
                '@images': resolve('src/assets/images'),
                'images': resolve('src/assets/images'),
                '@fonts': resolve('src/assets/fonts'),
                '@components': resolve('src/components'),
                '@js': resolve('src/js'),
                '@styles': resolve('src/scss'),
                '@items': resolve('src/structure/items'),
                '@static': resolve('public/static')
            }
        },
        plugins: [
            new webpack.DefinePlugin({
                IS_DEVELOPMENT: process.env.NODE_ENV === 'development',
                IS_PRODUCTION_SBS: process.env.NODE_ENV === 'production-sbs',
                IS_PRODUCTION: process.env.NODE_ENV === 'production'
            }),
            new webpack.ProvidePlugin({
                mapGetters: ['vuex', 'mapGetters'],
                mapActions: ['vuex', 'mapActions'],
                mapMutations: ['vuex', 'mapMutations'],
                mapState: ['vuex', 'mapState']
            }),
            new webpack.HashedModuleIdsPlugin(),
            new HtmlWebpackPlugin({
                filename: 'index.html',
                template: 'src/index.html',
                inject: true
            })
        ],
        module: {
            rules: [
                /* {
                    test: /\.(svg)$/,
                    loader: 'url-loader',
                    query: {
                        limit: 10000,
                    }
                }, */
                {
                    test: /\.(pdf)(\?.*)?$/,
                    loader: 'file-loader'
                },
                {
                    test: /\.js$/,
                    loader: 'babel-loader',
                    include: [
                        resolve('src'),
                        resolve('node_modules/webpack-dev-server/client'),
                        resolve('node_modules/course-structure'),
                        resolve('node_modules/course-xapi'),
                        resolve('node_modules/course-ui'),
                        resolve('node_modules/course-structure-debugger'),
                        resolve('node_modules/course-utils'),
                        resolve('node_modules/course-log'),
                        resolve('node_modules/course-socket-debugger')
                    ]
                }
            ]
        }
    },
    chainWebpack: config => {
        config.plugins.delete('copy');
        config.resolve.modules.prepend('src/libs');
    },
    css: {
        loaderOptions: {
            css: {
                localIdentName: isProduction
                    ? '[hash:base64:5]'
                    : '[name]__[local]--[hash:base64:5]',
                camelCase: 'only'
            },
            sass: {
                data: '@import \'@styles/_mixins.scss\';'
            }
        }
    }
};
