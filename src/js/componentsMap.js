const componentsMap = {
    chapter: {
        template: 'Chapter',
        root: true
    },
    subchapter: {
        template: 'Subchapter',
        root: true
    },
    test_compo: {
        template: 'TestCompo',
        root: true
    },
    td_compo: {
        template: 'TdCompo',
        root: true
    },
    case_compo: {
        template: 'CaseCompo',
        root: true
    },
    // exercises
    exercise_range: {
        template: 'ExerciseRangeCompo',
        root: true
    },
    exercise_checkbox: {
        template: 'ExerciseCheckBoxCompo',
        root: true
    },
    exercise_ballons: {
        template: 'ExerciseBallonsCompo',
        root: true
    },
    // заглушка
    dummy: {
        template: 'DummyCompo',
        root: true
    },
    // additional контент для этого курса (боковое меню)
    glossary: {
        template: 'Glossary',
        root: true
    },
    library: {
        template: 'Library',
        root: true
    },
    notes: {
        template: 'Notes',
        root: true
    },
    profile: {
        template: 'Profile',
        root: true
    },
    shop: {
        template: 'Shop',
        root: true
    },
    // Контент лонгрида ниже
    longread: {
        template: 'LongreadCompo',
        root: true
    },
    videoplayer: {
        template: 'VideoPlayer'
    },
    slide_factory: 'LongreadSlideCompo'
};

export default componentsMap;

export const FEEDBACK_SCREENS_MAP = {
    main: 'FeedbackScreenMain',
    technical: 'FeedbackScreenTechnical',
    material: 'FeedbackScreenMaterial',
    another: 'FeedbackScreenAnother',
    reaction: 'FeedbackScreenReaction',
    completed: 'FeedbackScreenCourseCompleted'
};
