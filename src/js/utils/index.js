export const shuffle = val => {
    for (let i = val.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [val[i], val[j]] = [val[j], val[i]];
    }
    return val;
};

export const computedHelper = (obj, propArray) => {
    const helperObj = {};
    propArray.forEach(prop => {
        if (typeof prop === 'object') {
            Object.keys(prop).forEach(key => {
                helperObj[key] = function() {
                    return this[obj][prop[key]];
                };
            });
        } else {
            helperObj[prop] = function() {
                return this[obj][prop];
            };
        }
    });
    return helperObj;
};

export const arrayShuffle = (array, mutate = false) => {
    if (!mutate) {
        array = [...array];
    }

    let index = array.length;

    while (index) {
        let randomIndex = Math.floor(Math.random() * index);
        index--;

        let tempVal = array[index];
        array[index] = array[randomIndex];
        array[randomIndex] = tempVal;
    }

    return array;
};
