import Vue from 'vue';

import {
    CUIScrollbarVertical,
    CUIScrollbarHorizontal,
    CUIScrollbarDirective,
    CUIVideo,
    CUIProgressbar,
    CUIProgressCircle
} from 'course-ui';

export default () => {
    Vue.component('CUIScrollbarVertical', CUIScrollbarVertical);
    Vue.component('CUIScrollbarHorizontal', CUIScrollbarHorizontal);
    Vue.component('CUIVideo', CUIVideo);
    Vue.component('CUIProgressbar', CUIProgressbar);
    Vue.component('CUIProgressCircle', CUIProgressCircle);
    Vue.directive('scrollbar', CUIScrollbarDirective);
};
