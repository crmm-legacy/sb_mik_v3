import Vue from 'vue';

import Preloader from './PreloaderCompo.vue';

/**
 * Полифиллы для ИЕ должны подключаться здесь, и прописаны в babel.config.js
 */
import '@babel/polyfill';

/* eslint-disable no-new */
new Vue({
    el: '#preloader',
    components: { Preloader },
    template: '<Preloader />'
});
