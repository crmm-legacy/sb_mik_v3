/**
 * Полифиллы для ИЕ должны подключаться здесь, и прописаны в babel.config.js
 */
import '@babel/polyfill';

import Vue from 'vue';

/**
 * Плеер курса
 */
import Player from './Player';

/**
 * Подключаем хранилище курса, структура хранится там же
 */
import store from './vuex/store';

/**
 * Файл подключения глобально используемых компонентов
 */
import registerGlobals from '@js';

/**
 * Подключение глобальных стилей
 */
import './scss/index.scss';

/**
 * Подключение утилит
 */
// import default from 'course-utils'

Vue.config.productionTip = false;
registerGlobals();
/* eslint-disable no-new */
new Vue({
    store,
    el: '#player',
    components: { Player },
    template: '<Player />'
});
