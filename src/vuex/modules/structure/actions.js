export default {
    goNextByLast({ commit, getters }) {
        commit('GO_NEXT_BY_LAST', { getters });
    }
};
