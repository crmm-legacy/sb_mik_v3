export default {
    availableChapters: state => {
        let item = state.currentCourse.currentItem;

        if (item.component === 'chapter') {
            return state.currentCourse.items.filter(chapter => {
                return chapter.items.some(subchapter => subchapter.items.length);
            });
        }
        return {};
    },
    currentChapter: (state, getters) => {
        if (getters.isLeaf) {
            return state.currentCourse.currentItem.parent.parent;
        }
        return state.currentCourse.currentItem;
    },
    isLeaf: state => {
        return !state.currentCourse.currentItem.hasOwnProperty('items');
    },
    isLast: (state, getters) => {
        if (getters.isLeaf) {
            let curItem = state.currentCourse.currentItem;
            let curChapter = curItem.parent.parent;
            let lastCard = curChapter.items[curChapter.items.length - 1];

            return lastCard.items[lastCard.items.length - 1] === curItem;
        }
        return false;
    }
};
