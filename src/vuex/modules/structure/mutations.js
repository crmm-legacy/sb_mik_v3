export default {
    GO_NEXT_BY_LAST(state, { getters }) {
        if (getters.isLast) {
            let nextChapter = getters.currentChapter.getAnyThisLevelNextItem();
            if (nextChapter) nextChapter.setCurrent();
        } else {
            getters.currentChapter.setCurrent();
        }
    }
};
