import { vuexStructure } from 'course-structure';
import structure from '@src/structure';
import getters from './getters';
import actions from './actions';
import mutations from './mutations';
/**
 * Добавляем специфичный vuex-модуль
 */
const structureModule = vuexStructure(structure);
Object.assign(structureModule.getters, getters);
structureModule.actions = actions;
structureModule.mutations = mutations;

/**
* Экспортируем структуру, обернутую в плагин-помощник из библиотеки
* (создает vuex-модуль с геттерами)
*/
export default structureModule;
