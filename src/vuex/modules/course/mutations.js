export default {
    TOGGLE_MENU(state) {
        state.ui.isMenuOpen = !state.ui.isMenuOpen;
    },
    TOGGLE_HEADER(state) {
        state.ui.isHeaderVisible = !state.ui.isHeaderVisible;
    },
    SHOW_SCREEN(state, payload) {
        state.currentScreen = payload;
    },
    CLOSE_SCREEN(state) {
        state.currentScreen = null;
    }
};
