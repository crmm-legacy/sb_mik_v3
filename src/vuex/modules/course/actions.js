export default {
    toggleMenu({ commit }) {
        commit('TOGGLE_MENU');
    },
    toggleHeader({ commit }) {
        commit('TOGGLE_HEADER');
    },
    showScreen({ commit, dispatch }, payload) {
        commit('SHOW_SCREEN', payload);
    },
    closeScreen({ commit, dispatch }) {
        commit('CLOSE_SCREEN');
    }
};
