export default {
    ui: {
        isMenuOpen: false,
        isHeaderVisible: true
    },
    currentScreen: null,
    mediaPaused: false
};
