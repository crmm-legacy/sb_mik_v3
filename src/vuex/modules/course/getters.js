export default {
    isMenuOpen: ({ ui }) => ui.isMenuOpen,
    currentScreen: ({ currentScreen }) => currentScreen,
    mediaPaused: ({ ui, currentScreen }) => {
        return !!((ui.isMenuOpen || currentScreen));
    }
};
