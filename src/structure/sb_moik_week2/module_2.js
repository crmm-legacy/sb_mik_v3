import chapter_0 from './module_2/chapter_0';

export default {
    id: '/cnyyy3b1',
    name: 'Способы расчетов (Уровень 2)',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0
    ]
};
