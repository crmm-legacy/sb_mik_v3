import chapter_0 from './module_0/chapter_0';
import chapter_1 from './module_0/chapter_1';
import chapter_2 from './module_0/chapter_2';
import chapter_3 from './module_0/chapter_3';

export default {
    id: '/6ysvxl39',
    name: 'Ипотечные программы (Уровень 2)',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0, // Строительство жилого дома
        chapter_1, // Загородная недвижимость
        chapter_2, // Нецелевой кредит под залог
        chapter_3  // Гараж
    ]
};
