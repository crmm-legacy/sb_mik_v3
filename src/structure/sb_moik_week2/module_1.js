import chapter_0 from './module_1/chapter_0';
import chapter_1 from './module_1/chapter_1';
import chapter_2 from './module_1/chapter_2';

export default {
    id: '/lw6zl6mj',
    name: 'Особые условия и акции (Уровень 2)',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0, // Ипотека с господдержкой
        chapter_1, // Залоговые объекты
        chapter_2 // ГЖС
    ]
};
