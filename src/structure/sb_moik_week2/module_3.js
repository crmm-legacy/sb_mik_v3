import chapter_0 from './module_3/chapter_0';

export default {
    id: '/4x9z7prp',
    name: 'После выдачи (Уровень 2)',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0
    ]
};
