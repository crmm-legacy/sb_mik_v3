import chapter_0 from './module_5/chapter_0';
import chapter_1 from './module_5/chapter_1';
import chapter_2 from './module_5/chapter_2';
import chapter_3 from './module_5/chapter_3';

export default {
    id: '/ayqu6txl',
    name: 'Прием заявлений на изменение условий кредитования',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0,
        chapter_1,
        chapter_2,
        chapter_3
    ]
};
