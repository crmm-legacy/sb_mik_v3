import chapter_0 from './module_6/chapter_0';

export default {
    id: '/47noczbx',
    name: 'Итоговое тестирование',
    type: 'item',
    component: 'chapter',
    config: {
        finalTest: true
    },
    items: [
        chapter_0
    ]
};
