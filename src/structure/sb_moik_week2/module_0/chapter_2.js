import video_i8m8l6jv from '@items/video/video_i8m8l6jv.js';
import test_1u5ontk6 from '@items/test/test_1u5ontk6.js';

export default {
    id: '/agkimpz0',
    name: 'Нецелевой кредит под залог',
    type: 'item',
    component: 'chapter',
    items: [
        video_i8m8l6jv,
        test_1u5ontk6
    ]
};
