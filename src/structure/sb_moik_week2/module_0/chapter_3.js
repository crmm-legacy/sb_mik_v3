import video_mz94d6hk from '@items/video/video_mz94d6hk.js';
import test_zt29248t from '@items/test/test_zt29248t.js';

export default {
    id: '/lh21c0fkc',
    name: 'Гараж',
    type: 'item',
    component: 'chapter',
    items: [
        video_mz94d6hk,
        test_zt29248t
    ]
};
