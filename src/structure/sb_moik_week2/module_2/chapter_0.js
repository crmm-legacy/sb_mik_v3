import video_6rv7x4xv from '@items/video/video_6rv7x4xv.js';
import test_8xqhn3hf from '@items/test/test_8xqhn3hf.js';

export default {
    id: '/qmcrrh97',
    name: 'Прямая и альтернативная сделки',
    type: 'item',
    component: 'chapter',
    items: [
        video_6rv7x4xv,
        test_8xqhn3hf
    ]
};
