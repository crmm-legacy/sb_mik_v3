import final_test_fzhwrn7s from '@items/test/final_test_fzhwrn7s.js';

export default {
    id: '/ia0a0zlo',
    type: 'item',
    component: 'chapter',
    name: 'Итоговый тест',
    config: {},
    items: [
        final_test_fzhwrn7s
    ]
};
