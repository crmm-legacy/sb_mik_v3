import video_rogl8gs7 from '@items/video/video_rogl8gs7.js';
import pdf_prelzr5i from '@items/pdf/pdf_prelzr5i.js';
import test_1dxoqkpq from '@items/test/test_1dxoqkpq.js';

export default {
    id: '/x5r01eq5',
    name: 'ГЖС',
    type: 'item',
    component: 'chapter',
    items: [
        video_rogl8gs7,
        pdf_prelzr5i,
        test_1dxoqkpq
    ]
};
