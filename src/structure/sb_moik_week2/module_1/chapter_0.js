import video_ye29nd8e from '@items/video/video_ye29nd8e.js';
import test_aovlgp46 from '@items/test/test_aovlgp46.js';

export default {
    id: '/3j9dmdop7',
    name: 'Ипотека с господдержкой',
    type: 'item',
    component: 'chapter',
    items: [
        video_ye29nd8e,
        test_aovlgp46
    ]
};
