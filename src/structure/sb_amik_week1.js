import module_0 from './sb_amik_week1/module_0.js';
import module_1 from './sb_amik_week1/module_1.js';
import module_2 from './sb_amik_week1/module_2.js';
import module_3 from './sb_amik_week1/module_3.js';
import module_4 from './sb_amik_week1/module_4.js';
import module_5 from './sb_amik_week1/module_5.js';
import module_6 from './sb_amik_week1/module_6.js';
import module_7 from './sb_amik_week1/module_7.js';
import module_8 from './sb_amik_week1/module_8.js';
import module_9 from './sb_amik_week1/module_9.js';
import module_10 from './sb_amik_week1/module_10.js';
import module_11 from './sb_amik_week1/module_11.js';
import module_12 from './sb_amik_week1/module_12.js';
import module_13 from './sb_amik_week1/module_13.js';
import module_14 from './sb_amik_week1/module_14.js';

export default {
    id: 'https://crmm.ru/xapi/courses/sb_amik_week1',
    name: 'Комплексная программа обучения Менеджера по сделкам с недвижимостью АМИК Неделя 1',
    type: 'course',
    version: 1,
    virtual: 'https://crmm.ru/xapi/courses/sb_mik-3',
    config: {
        color: 'blue'
    },
    items: [
        module_0,
        module_1,
        module_2,
        module_3,
        module_4,
        module_5,
        module_6,
        module_7,
        module_8,
        module_9,
        module_10,
        module_11,
        module_12,
        module_13,
        module_14
    ],
    objectives: [{
        id: 'https://crmm.ru/xapi/courses/sb_amik_week1/ms_0_0',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/h4t6uc38',
        pass_time: '1',
        icon: './static/vshs_icons/amik_week1/0.png',
        color: '#64a7db',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_amik_week1/ms_0_1',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/v4qca4vt',
        pass_time: '1',
        icon: './static/vshs_icons/amik_week1/1.png',
        color: '#64a7db',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_amik_week1/ms_0_2',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/akygnw6f',
        pass_time: '1',
        icon: './static/vshs_icons/amik_week1/2.png',
        color: '#64a7db',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_amik_week1/ms_0_3',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/y7yqen69',
        pass_time: '1',
        icon: './static/vshs_icons/amik_week1/3.png',
        color: '#64a7db',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_amik_week1/ms_0_4',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/xskqofnp',
        pass_time: '1',
        icon: './static/vshs_icons/amik_week1/4.png',
        color: '#64a7db',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_amik_week1/ms_0_5',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/jf5bpdc2',
        pass_time: '1',
        icon: './static/vshs_icons/amik_week1/5.png',
        color: '#64a7db',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_amik_week1/ms_0_6',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/a76gwnwm',
        pass_time: '1',
        icon: './static/vshs_icons/amik_week1/6.png',
        color: '#64a7db',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_amik_week1/ms_0_7',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/tkvmqccm',
        pass_time: '1',
        icon: './static/vshs_icons/amik_week1/7.png',
        color: '#64a7db',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_amik_week1/ms_0_8',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/0me69cnr',
        pass_time: '1',
        icon: './static/vshs_icons/amik_week1/8.png',
        color: '#64a7db',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_amik_week1/ms_0_9',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/rcre18xb',
        pass_time: '1',
        icon: './static/vshs_icons/amik_week1/9.png',
        color: '#64a7db',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_amik_week1/ms_0_10',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/c9iqbhn3',
        pass_time: '1',
        icon: './static/vshs_icons/amik_week1/10.png',
        color: '#64a7db',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_amik_week1/ms_0_11',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/pwgy09do',
        pass_time: '1',
        icon: './static/vshs_icons/amik_week1/11.png',
        color: '#64a7db',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_amik_week1/ms_0_12',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/apjrdpon',
        pass_time: '1',
        icon: './static/vshs_icons/amik_week1/12.png',
        color: '#64a7db',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_amik_week1/ms_0_13',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/tagf96ph',
        pass_time: '1',
        icon: './static/vshs_icons/amik_week1/13.png',
        color: '#64a7db',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_amik_week1/ms_0_14',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/qg6t6vdw',
        pass_time: '1',
        icon: './static/vshs_icons/global/final_test_new.png',
        color: '#64a7db',
        scoped: true
    }]
};
