import CourseStructure from 'course-structure';
import STRUCTURE from './structureContent.js';
import XAPI from 'course-xapi';
import fakeLaunch from '../../.fakeLaunch';
import './register_items';

/**
 * Создаем экземпляр класса структуры и загружаем импортированную структуру
 * в него. Настройка nonreactiveContent делает контент сущностей невидимым для
 * Vue, следовательно - не реактивным (для экономии памяти)
 */

const structure = new CourseStructure({ nonreactiveContent: true, ignoreIncorrectTypes: true });

structure.load(
    STRUCTURE,
    'js',
    new XAPI({ consoleLog: false }),
    IS_DEVELOPMENT,
    fakeLaunch
);

window.structure = structure;

export default structure;
