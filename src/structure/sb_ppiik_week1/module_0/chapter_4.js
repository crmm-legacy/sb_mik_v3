import video_5bn3fizq from '@items/video/video_5bn3fizq.js';
import test_ngxld3rc from '@items/test/test_ngxld3rc.js';

export default {
    id: '/n4yv5v5d',
    type: 'item',
    component: 'chapter',
    name: 'Участники сделки',
    config: {},
    items: [
        video_5bn3fizq,
        test_ngxld3rc
    ]
};
