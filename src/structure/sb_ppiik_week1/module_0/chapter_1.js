import video_4u4cdclq from '@items/video/video_4u4cdclq.js';
import test_88h8v304 from '@items/test/test_88h8v304.js';

export default {
    id: '/rsmxf636',
    type: 'item',
    component: 'chapter',
    name: 'Сервисная модель',
    config: {},
    items: [
        video_4u4cdclq,
        test_88h8v304
    ]
};
