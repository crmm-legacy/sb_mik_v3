import chapter_0 from './module_4/chapter_0';

export default {
    id: '/ld9s56bh',
    name: 'Документы',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0
    ]
};
