import chapter_0 from './module_5/chapter_0';
import chapter_1 from './module_5/chapter_1';
import chapter_2 from './module_5/chapter_2';
import chapter_3 from './module_5/chapter_3';
import chapter_4 from './module_5/chapter_4';

export default {
    id: '/0owu75av',
    name: 'Сервисы',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0,
        chapter_1,
        chapter_2,
        chapter_3,
        chapter_4
    ]
};
