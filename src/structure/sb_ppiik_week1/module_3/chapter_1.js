import video_2pfh6ani from '@items/video/video_2pfh6ani.js';
import test_gjuvrr6c from '@items/test/test_gjuvrr6c.js';

export default {
    id: '/wg8hblw2',
    type: 'item',
    component: 'chapter',
    name: 'Ипотека + материнский капитал',
    config: {},
    items: [
        video_2pfh6ani,
        test_gjuvrr6c
    ]
};
