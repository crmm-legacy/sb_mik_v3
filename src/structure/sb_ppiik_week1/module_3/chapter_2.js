import video_nlijrz3s from '@items/video/video_nlijrz3s.js';
import test_pf1jmr3x from '@items/test/test_pf1jmr3x.js';

export default {
    id: '/k7yvjb11',
    type: 'item',
    component: 'chapter',
    name: 'Жилищный кредит по 2 документам',
    config: {},
    items: [
        video_nlijrz3s,
        test_pf1jmr3x
    ]
};
