import video_xekhgoq8 from '@items/video/video_xekhgoq8.js';
import test_pjwl1hlr from '@items/test/test_pjwl1hlr.js';

export default {
    id: '/fr2n7nyz',
    type: 'item',
    component: 'chapter',
    name: 'Молодая семья',
    config: {},
    items: [
        video_xekhgoq8,
        test_pjwl1hlr
    ]
};
