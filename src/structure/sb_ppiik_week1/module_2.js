import chapter_0 from './module_2/chapter_0';
import chapter_1 from './module_2/chapter_1';

export default {
    id: '/n80ppnpi',
    name: 'Продукты',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0,
        chapter_1
    ]
};
