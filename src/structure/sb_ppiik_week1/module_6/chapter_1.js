import video_d83yj0ie from '@items/video/video_d83yj0ie.js';
import test_nt29t2nk from '@items/test/test_nt29t2nk.js';

export default {
    id: '/5k89f0wy',
    type: 'item',
    component: 'chapter',
    name: 'Сейфовая ячейка',
    config: {},
    items: [
        video_d83yj0ie,
        test_nt29t2nk
    ]
};
