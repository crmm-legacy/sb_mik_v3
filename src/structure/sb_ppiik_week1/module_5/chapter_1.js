import video_62gfzy1u from '@items/video/video_62gfzy1u.js';
import test_jtkl5wsa from '@items/test/test_jtkl5wsa.js';

export default {
    id: '/nxg6vjt3',
    type: 'item',
    component: 'chapter',
    name: 'Страхование залога',
    config: {},
    items: [
        video_62gfzy1u,
        test_jtkl5wsa
    ]
};
