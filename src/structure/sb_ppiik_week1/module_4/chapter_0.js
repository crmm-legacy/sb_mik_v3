import video_o7vqm8mf from '@items/video/video_o7vqm8mf.js';
import test_ugu1vzlp from '@items/test/test_ugu1vzlp.js';

export default {
    id: '/nd7fobyj',
    type: 'item',
    component: 'chapter',
    name: 'Подтверждение первоначального взноса',
    config: {},
    items: [
        video_o7vqm8mf,
        test_ugu1vzlp
    ]
};
