import chapter_0 from './module_7/chapter_0';
import chapter_1 from './module_7/chapter_1';
import chapter_2 from './module_7/chapter_2';

export default {
    id: '/8cbj59y7',
    name: 'Стандарты обслуживания клиентов',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0,
        chapter_1,
        chapter_2
    ]
};
