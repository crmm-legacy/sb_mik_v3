import video_rpshobgp from '@items/video/video_rpshobgp.js';
import test_jhfv81xp from '@items/test/test_jhfv81xp.js';

export default {
    id: '/s0sizwpx',
    type: 'item',
    component: 'chapter',
    name: 'Заполнение анкеты-заявления',
    config: {},
    items: [
        video_rpshobgp,
        test_jhfv81xp
    ]
};
