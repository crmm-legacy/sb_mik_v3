import pdf_2yk58oka from '@items/pdf/pdf_2yk58oka.js';
import test_ruivwq3j from '@items/test/test_ruivwq3j.js';

export default {
    id: '/tnqfw4ln',
    type: 'item',
    component: 'chapter',
    name: 'Подтверждение дохода',
    config: {},
    items: [
        pdf_2yk58oka,
        test_ruivwq3j
    ]
};
