import video_6xviao91 from '@items/video/video_6xviao91.js';
import test_7uzbb0mw from '@items/test/test_7uzbb0mw.js';

export default {
    id: '/m1b7bd86',
    type: 'item',
    component: 'chapter',
    name: 'Подтверждение трудовой занятости',
    config: {},
    items: [
        video_6xviao91,
        test_7uzbb0mw
    ]
};
