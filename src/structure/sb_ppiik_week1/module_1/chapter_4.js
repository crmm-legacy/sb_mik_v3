import video_2qzpgk2q from '@items/video/video_2qzpgk2q.js';
import test_9augcfc7 from '@items/test/test_9augcfc7.js';

export default {
    id: '/bz03mtek',
    type: 'item',
    component: 'chapter',
    name: 'Клиенту одобрена сумма меньше запрошенной',
    config: {},
    items: [
        video_2qzpgk2q,
        test_9augcfc7
    ]
};
