import chapter_0 from './module_6/chapter_0';
import chapter_1 from './module_6/chapter_1';

export default {
    id: '/18rtj69p',
    name: 'Способы расчета',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0,
        chapter_1
    ]
};
