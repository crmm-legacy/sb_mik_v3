import chapter_0 from './module_0/chapter_0';
import chapter_1 from './module_0/chapter_1';
import chapter_2 from './module_0/chapter_2';
import chapter_3 from './module_0/chapter_3';
import chapter_4 from './module_0/chapter_4';
import chapter_5 from './module_0/chapter_5';
import chapter_6 from './module_0/chapter_6';

export default {
    id: '/ekze7ion',
    name: 'Введение',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0,
        chapter_1,
        chapter_2,
        chapter_3,
        chapter_4,
        chapter_5,
        chapter_6
    ]
};
