import final_test_3xctzb7h from '@items/test/final_test_3xctzb7h.js';

export default {
    id: '/qeiro9ss',
    type: 'item',
    component: 'chapter',
    name: 'Итоговый тест',
    config: {},
    items: [
        final_test_3xctzb7h
    ]
};
