import module_0 from './sb_moik_week1/module_0.js';
import module_1 from './sb_moik_week1/module_1.js';
import module_2 from './sb_moik_week1/module_2.js';
import module_3 from './sb_moik_week1/module_3.js';
import module_4 from './sb_moik_week1/module_4.js';
import module_5 from './sb_moik_week1/module_5.js';
import module_6 from './sb_moik_week1/module_6.js';
import module_7 from './sb_moik_week1/module_7.js';
import module_8 from './sb_moik_week1/module_8.js';

export default {
    id: 'https://crmm.ru/xapi/courses/sb_moik_week1',
    name: 'Комплексная программа обучения МОИК. Неделя 1',
    type: 'course',
    version: 1,
    virtual: 'https://crmm.ru/xapi/courses/sb_mik-3',
    config: {
        color: 'blue'
    },
    items: [
        module_0, // Введение
        module_1, // Ипотечные программы
        module_2, // Особые условия и акции
        module_3, // Сервисы
        module_4, // Способы расчетов
        module_5, // После выдачи
        module_6, // Предоставление ответов Клиенту в момент обращения
        module_7, // Стандарты обслуживания клиентов
        module_8 // Итоговое тестирование
    ],
    objectives: [{
        id: 'https://crmm.ru/xapi/courses/sb_moik_week1/ms_0_0',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/tazrov4g',
        pass_time: '1',
        icon: './static/vshs_icons/moik_week1/0.png',
        color: '#64a7db',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_moik_week1/ms_0_1',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/73f32lwb',
        pass_time: '1',
        icon: './static/vshs_icons/moik_week1/1.png',
        color: '#64a7db',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_moik_week1/ms_0_2',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/ncbhmf6s',
        pass_time: '1',
        icon: './static/vshs_icons/moik_week1/2.png',
        color: '#64a7db',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_moik_week1/ms_0_3',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/5r8myeu2',
        pass_time: '1',
        icon: './static/vshs_icons/moik_week1/3.png',
        color: '#64a7db',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_moik_week1/ms_0_4',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/7mb0xyrl',
        pass_time: '1',
        icon: './static/vshs_icons/moik_week1/4.png',
        color: '#64a7db',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_moik_week1/ms_0_5',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/jx4l1kyr',
        pass_time: '1',
        icon: './static/vshs_icons/moik_week1/5.png',
        color: '#64a7db',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_moik_week1/ms_0_6',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/y0aj1om4',
        pass_time: '1',
        icon: './static/vshs_icons/moik_week1/6.png',
        color: '#64a7db',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_moik_week1/ms_0_7',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/qbzqp5h1',
        pass_time: '1',
        icon: './static/vshs_icons/moik_week1/7.png',
        color: '#64a7db',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_moik_week1/ms_0_8',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/cx335jzr',
        pass_time: '1',
        icon: './static/vshs_icons/global/final_test_new.png',
        color: '#64a7db',
        scoped: true
    }]
};
