import video_uiom487c from '@items/video/video_uiom487c.js';
import test_0o40quhw from '@items/test/test_0o40quhw.js';

export default {
    id: '/atrevpgi',
    type: 'item',
    component: 'chapter',
    name: 'Приобретение строящегося жилья',
    config: {},
    items: [
        video_uiom487c,
        test_0o40quhw
    ]
};
