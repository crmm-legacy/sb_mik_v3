import video_awhxizga from '@items/video/video_awhxizga.js';
import test_u7o16god from '@items/test/test_u7o16god.js';

export default {
    id: '/9u0y3rue',
    type: 'item',
    component: 'chapter',
    name: 'Приобретение готового жилья',
    config: {},
    items: [
        video_awhxizga,
        test_u7o16god
    ]
};
