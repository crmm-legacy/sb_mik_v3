import video_a7m7p0kn2 from '@items/video/video_a7m7p0kn2.js';
import test_z19yteg7 from '@items/test/test_z19yteg7.js';
import video_g2hgkz73 from '@items/video/video_g2hgkz73.js';
import pdf_kv0fdmq2 from '@items/pdf/pdf_kv0fdmq2.js';

export default {
    id: '/wxfdaohy',
    type: 'item',
    component: 'chapter',
    name: 'Виды платежей и погашение кредита',
    config: {},
    items: [
        video_a7m7p0kn2,
        video_g2hgkz73,
        pdf_kv0fdmq2,
        test_z19yteg7
    ]
};
