import video_be0d2j246 from '@items/video/video_be0d2j246.js';
import test_g97xkcxo from '@items/test/test_g97xkcxo.js';
import video_4wy5b2d4 from '@items/video/video_4wy5b2d4.js';

export default {
    id: '/5a58o7213',
    type: 'item',
    component: 'chapter',
    name: 'Досрочное погашение кредита и неустойка',
    config: {},
    items: [
        video_be0d2j246,
        video_4wy5b2d4,
        test_g97xkcxo
    ]
};
