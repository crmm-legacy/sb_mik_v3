import video_ql2t5gmt from '@items/video/video_ql2t5gmt.js';
import video_konen2mn from '@items/video/video_konen2mn.js';

export default {
    id: '/yqcpilg1',
    type: 'item',
    component: 'chapter',
    name: 'Работа в АС ЕКП',
    config: {},
    items: [
        video_ql2t5gmt,
        video_konen2mn
    ]
};
