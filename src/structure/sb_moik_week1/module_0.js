import chapter_0 from './module_0/chapter_0';
import chapter_1 from './module_0/chapter_1';
import chapter_2 from './module_0/chapter_2';
import chapter_3 from './module_0/chapter_3';
import chapter_4 from './module_0/chapter_4';
import chapter_5 from './module_0/chapter_5';

export default {
    id: '/tazrov4g',
    name: 'Введение',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0,    // Знакомство с компанией и ценностями Банка
        chapter_1,    // О курсе и ипотечном кредитовании
        chapter_2,    // Путь Клиента
        chapter_3,    // Участники сделки
        chapter_4,    // Требования к участникам
        chapter_5     // Обеспечение ипотечного кредита
    ]
};
