import chapter_0 from './module_5/chapter_0';
import chapter_1 from './module_5/chapter_1';
import chapter_2 from './module_5/chapter_2';

export default {
    id: '/jx4l1kyr',
    name: 'После выдачи',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0, // Виды платежей и погашение кредита
        chapter_1, // Досрочное погашение кредита и неустойка
        chapter_2 // Работа в АС ЕКП (поиск договора и клиента)
    ]
};
