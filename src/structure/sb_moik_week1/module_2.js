import chapter_0 from './module_2/chapter_0';
import chapter_1 from './module_2/chapter_1';
import chapter_2 from './module_2/chapter_2';

export default {
    id: '/ncbhmf6s',
    name: 'Особые условия и акции',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0,    // Молодая семья
        chapter_1,    // Ипотека + материнский капитал
        chapter_2     // Жилищный кредит по 2 документам
    ]
};
