import train_ppi580fkd from '@items/train/train_ppi580fkd.js';
import train_998bcilpn from '@items/train/train_998bcilpn.js';
import train_rn3b93d2v from '@items/train/train_rn3b93d2v.js';

export default {
    id: '/ruczcf7mf',
    type: 'item',
    component: 'chapter',
    name: 'Тренажеры безналичных перечислений в адрес ЮЛ',
    config: {},
    items: [
        train_ppi580fkd,
        train_998bcilpn,
        train_rn3b93d2v
    ]
};
