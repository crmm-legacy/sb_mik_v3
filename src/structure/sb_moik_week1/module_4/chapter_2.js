import video_d83yj0ie from '@items/video/video_d83yj0ie.js';
import test_nt29t2nk from '@items/test/test_nt29t2nk.js';
import train_dgl25gfb6 from '@items/train/train_dgl25gfb6.js';

export default {
    id: '/90w1r83w',
    type: 'item',
    component: 'chapter',
    name: 'Индивидуальный сейф',
    config: {},
    items: [
        video_d83yj0ie,
        train_dgl25gfb6,
        test_nt29t2nk
    ]
};
