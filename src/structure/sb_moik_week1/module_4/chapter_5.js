import train_k81dgmlef from '@items/train/train_k81dgmlef.js';
import train_6666lnmod from '@items/train/train_6666lnmod.js';
import train_57h8j51kl from '@items/train/train_57h8j51kl.js';

export default {
    id: '/w5hwyqr4',
    type: 'item',
    component: 'chapter',
    name: 'Тренажеры безналичных перечислений в адрес ЮЛ',
    config: {},
    items: [
        train_k81dgmlef,
        train_6666lnmod,
        train_57h8j51kl
    ]
};
