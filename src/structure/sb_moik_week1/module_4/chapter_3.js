import video_kke4oko0j from '@items/video/video_kke4oko0j.js';
import test_yfe08trv from '@items/test/test_yfe08trv.js';
import train_d4p4n3idb from '@items/train/train_d4p4n3idb.js';

export default {
    id: '/vldixl6g',
    type: 'item',
    component: 'chapter',
    name: 'Безналичные перечисления',
    config: {},
    items: [
        video_kke4oko0j,
        train_d4p4n3idb,
        test_yfe08trv
    ]
};
