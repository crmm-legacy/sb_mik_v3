// import video_6c8n7sbj from '@items/video/video_6c8n7sbj.js';
import pdf_5hnoinzi from '@items/pdf/pdf_5hnoinzi.js';
import download_07y7kc7h from '@items/download/download_07y7kc7h.js';
import test_e0svhblq from '@items/test/test_e0svhblq.js';

export default {
    id: '/4mgi7l46',
    type: 'item',
    component: 'chapter',
    name: 'Регистрация, аренда, перепланировка',
    config: {},
    items: [
        // video_6c8n7sbj,
        pdf_5hnoinzi,
        download_07y7kc7h,
        test_e0svhblq
    ]
};
