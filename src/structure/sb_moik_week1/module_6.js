import chapter_0 from './module_6/chapter_0';
import chapter_1 from './module_6/chapter_1';

export default {
    id: '/y0aj1om4',
    name: 'Предоставление ответов Клиенту в момент обращения',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0,	// Справки/выписки по договору
        chapter_1	// Регистрация, аренда, перепланировка
    ]
};
