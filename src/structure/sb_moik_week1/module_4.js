import chapter_0 from './module_4/chapter_0';
// import chapter_1 from './module_4/chapter_1';
import chapter_2 from './module_4/chapter_2';
import chapter_3 from './module_4/chapter_3';
// import chapter_4 from './module_4/chapter_4';
import chapter_5 from './module_4/chapter_5';
import chapter_6 from './module_4/chapter_6';

export default {
    id: '/7mb0xyrl',
    name: 'Способы расчетов',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0, // Сервис безопасных расчетов
        // chapter_1,    // Аккредитив
        chapter_2, // Индивидуальный сейф
        chapter_3, // Безналичные перечисления
        // chapter_4,    // Работа в АС «ФСБ»
        chapter_5, // Тренажер безналичных перечислений в адрес ЮЛ
        chapter_6 // Тренажер безналичных перечислений в адрес ЮЛ ч 2
    ]
};
