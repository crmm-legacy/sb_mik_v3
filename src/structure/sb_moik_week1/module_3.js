import chapter_0 from './module_3/chapter_0';
import chapter_1 from './module_3/chapter_1';
import chapter_2 from './module_3/chapter_2';
// import chapter_3 from './module_3/chapter_3';

export default {
    id: '/5r8myeu2',
    name: 'Сервисы',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0,    // Страхование жизни и здоровья (Защищенный заемщик)
        chapter_1,    // Страхование залога
        chapter_2,    // Сервис электронной регистрации
        // chapter_3     // Работа в АС «Банковское страхование»
    ]
};
