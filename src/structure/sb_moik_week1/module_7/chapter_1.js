import video_rflt9g6t from '@items/video/video_rflt9g6t.js';
import test_ibqmnkhk from '@items/test/test_ibqmnkhk.js';

export default {
    id: '/kppoisx7',
    type: 'item',
    component: 'chapter',
    name: 'Стандарты общения с Клиентом при личном взаимодействии',
    config: {},
    items: [
        video_rflt9g6t,
        test_ibqmnkhk
    ]
};
