import video_lc08y04h from '@items/video/video_lc08y04h.js';
import test_9miu85fw from '@items/test/test_9miu85fw.js';

export default {
    id: '/eoj8gtig',
    type: 'item',
    component: 'chapter',
    name: 'Путь Клиента',
    config: {},
    items: [
        video_lc08y04h,
        test_9miu85fw
    ]
};
