import video_2v1j2whb from '@items/video/video_2v1j2whb.js';
import test_f8x5w2c4 from '@items/test/test_f8x5w2c4.js';

export default {
    id: '/ooxqskzl',
    type: 'item',
    component: 'chapter',
    name: 'Требования к участникам',
    config: {},
    items: [
        video_2v1j2whb,
        test_f8x5w2c4
    ]
};
