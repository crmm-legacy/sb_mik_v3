import video_rkqe0wz1 from '@items/video/video_rkqe0wz1.js';

export default {
    id: '/st7r1979',
    type: 'item',
    component: 'chapter',
    name: 'Знакомство с компанией и ценностями Банка',
    config: {},
    items: [
        video_rkqe0wz1
    ]
};
