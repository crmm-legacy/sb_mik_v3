
import video_dwsdy9oh from '@items/video/video_dwsdy9oh.js';
import test_qgjkzxwz from '@items/test/test_qgjkzxwz.js';

export default {
    id: '/38sgv9p2',
    type: 'item',
    component: 'chapter',
    name: 'О курсе и ипотечном кредитовании',
    config: {},
    items: [
        video_dwsdy9oh,
        test_qgjkzxwz
    ]
};
