import chapter_0 from './module_8/chapter_0';

export default {
    id: '/cx335jzr',
    name: 'Итоговое тестирование',
    type: 'item',
    component: 'chapter',
    config: {
        finalTest: true
    },
    items: [
        chapter_0
    ]
};
