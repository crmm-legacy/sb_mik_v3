import chapter_0 from './module_1/chapter_0';
import chapter_1 from './module_1/chapter_1';

export default {
    id: '/73f32lwb',
    name: 'Ипотечные программы',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0,	// Приобретение строящегося жилья
        chapter_1	// Приобретение готового жилья
    ]
};
