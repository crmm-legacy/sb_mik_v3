import video_zz94tep6 from '@items/video/video_zz94tep6.js';
import test_3jab0fsf from '@items/test/test_3jab0fsf.js';
import pdf_gh0epgoe5 from '@items/pdf/pdf_gh0epgoe5.js';

export default {
    id: '/xlsiotyc',
    type: 'item',
    component: 'chapter',
    name: 'Сервис электронной регистрации',
    config: {},
    items: [
        video_zz94tep6,
        pdf_gh0epgoe5,
        test_3jab0fsf
    ]
};
