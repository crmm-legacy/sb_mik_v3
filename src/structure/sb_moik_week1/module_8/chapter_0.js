import final_test_gy2h3dj4 from '@items/test/final_test_gy2h3dj4.js';

export default {
    id: '/9k6cndb3',
    type: 'item',
    component: 'chapter',
    name: 'Итоговый тест',
    config: {},
    items: [
        final_test_gy2h3dj4
    ]
};
