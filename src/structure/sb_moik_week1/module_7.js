import chapter_0 from './module_7/chapter_0';
import chapter_1 from './module_7/chapter_1';
// import chapter_2 from './module_7/chapter_2';

export default {
    id: '/qbzqp5h1',
    name: 'Стандарты обслуживания клиентов',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0,    // Стандарты общения с Клиентом по телефону
        chapter_1,    // Стандарты общения с Клиентом при личном взаимодействии
        // chapter_2     // Работа с внутренним Клиентом
    ]
};
