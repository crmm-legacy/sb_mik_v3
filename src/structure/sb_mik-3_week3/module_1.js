import chapter_0 from './module_1/chapter_0';

export default {
    id: '/d190f32a',
    name: 'Особые условия и акции (уровень 3)',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0
    ]
};
