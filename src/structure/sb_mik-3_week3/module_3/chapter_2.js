import video_ptaw8eta from '@items/video/video_ptaw8eta.js';
// import pdf_ from '@items/pdf/pdf_.js'
import test_jpnr5zve from '@items/test/test_jpnr5zve.js';

export default {
    id: '/xx6rrdkv',
    name: 'ПИФ',
    type: 'item',
    component: 'chapter',
    items: [
        video_ptaw8eta,
        // pdf_,
        test_jpnr5zve
    ]
};
