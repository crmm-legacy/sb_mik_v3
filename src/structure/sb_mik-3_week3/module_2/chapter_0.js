import video_l7jbx8m8 from '@items/video/video_l7jbx8m8.js';
import video_zaqqvdwg from '@items/video/video_zaqqvdwg.js';
// import pdf_ from '@items/pdf/pdf_.js';
import test_74r8fw0r from '@items/test/test_74r8fw0r.js';

export default {
    id: '/bqw2nrsi',
    name: 'Анализ отчета об оценке',
    type: 'item',
    component: 'chapter',
    items: [
        video_l7jbx8m8,
        video_zaqqvdwg,
        // pdf_,
        test_74r8fw0r
    ]
};
