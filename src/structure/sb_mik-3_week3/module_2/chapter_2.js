import video_9opxk7xv from '@items/video/video_9opxk7xv.js';
import pdf_ermx4ds6 from '@items/pdf/pdf_ermx4ds6.js';
import test_i2wq0gc9 from '@items/test/test_i2wq0gc9.js';
import download_s1x04x6r from '@items/download/download_s1x04x6r.js';

export default {
    id: '/47rz21dv',
    name: 'Пилот. Отказ от отчета об оценке',
    type: 'item',
    component: 'chapter',
    items: [
        video_9opxk7xv,
        pdf_ermx4ds6,
        download_s1x04x6r,
        test_i2wq0gc9
    ]
};
