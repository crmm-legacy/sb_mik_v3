import final_test_gn2olhqr from '@items/test/final_test_gn2olhqr.js';

export default {
    id: '/o9jdh31m',
    name: 'Итоговый тест',
    type: 'item',
    component: 'chapter',
    items: [
        final_test_gn2olhqr
    ]
};
