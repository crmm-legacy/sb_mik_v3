import chapter_0 from './module_5/chapter_0';

export default {
    id: '/j3df69s4',
    name: 'Итоговое тестирование',
    type: 'item',
    component: 'chapter',
    config: {
        finalTest: true
    },
    items: [
        chapter_0
    ]
};
