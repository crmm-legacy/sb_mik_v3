import video_yt8byaba from '@items/video/video_yt8byaba.js';
import pdf_re8r5dbc from '@items/pdf/pdf_re8r5dbc.js';
import test_k9cb5xne from '@items/test/test_k9cb5xne.js';

export default {
    id: '/uv50sb6h',
    name: 'Строительство жилого дома',
    type: 'item',
    component: 'chapter',
    items: [
        video_yt8byaba,
        pdf_re8r5dbc,
        test_k9cb5xne
    ]
};
