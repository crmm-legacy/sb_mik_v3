import chapter_0 from './module_4/chapter_0';
import chapter_1 from './module_4/chapter_1';
import chapter_2 from './module_4/chapter_2';

export default {
    id: '/oxb3ngow',
    name: 'Виды сделок (уровень 2)',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0,
        chapter_1,
        chapter_2
    ]
};
