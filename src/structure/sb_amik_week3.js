import module_0 from './sb_amik_week3/module_0.js';
import module_1 from './sb_amik_week3/module_1.js';
import module_2 from './sb_amik_week3/module_2.js';
import module_3 from './sb_amik_week3/module_3.js';
import module_4 from './sb_amik_week3/module_4.js';
import module_5 from './sb_amik_week3/module_5.js';

export default {
    id: 'https://crmm.ru/xapi/courses/sb_amik_week3',
    name: 'Комплексная программа обучения Менеджера по сделкам с недвижимостью АМИК Неделя 3',
    type: 'course',
    version: 1,
    virtual: 'https://crmm.ru/xapi/courses/sb_mik-3',
    config: {
        color: 'red'
    },
    items: [
        module_0,
        module_1,
        module_2,
        module_3,
        module_4,
        module_5
    ],
    objectives: [{
        id: 'https://crmm.ru/xapi/courses/sb_amik_week3/ms_0_0',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/uqstxk17',
        pass_time: '1',
        icon: './static/vshs_icons/sb_amik_week3/0.png',
        color: '#64a7db',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_amik_week3/ms_0_1',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/qi6t0jdz',
        pass_time: '1',
        icon: './static/vshs_icons/sb_amik_week3/1.png',
        color: '#64a7db',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_amik_week3/ms_0_2',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/g1a6927g',
        pass_time: '1',
        icon: './static/vshs_icons/sb_amik_week3/2.png',
        color: '#64a7db',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_amik_week3/ms_0_3',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/bxk2y4ly',
        pass_time: '1',
        icon: './static/vshs_icons/sb_amik_week3/3.png',
        color: '#64a7db',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_amik_week3/ms_0_4',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/eg03i22s',
        pass_time: '1',
        icon: './static/vshs_icons/sb_amik_week3/4.png',
        color: '#64a7db',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_amik_week3/ms_0_5',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/7rrfmw7l',
        pass_time: '1',
        icon: './static/vshs_icons/global/final_test_new.png',
        color: '#64a7db',
        scoped: true
    }]
};
