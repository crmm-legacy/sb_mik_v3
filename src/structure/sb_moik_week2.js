import module_0 from './sb_moik_week2/module_0.js';
import module_1 from './sb_moik_week2/module_1.js';
import module_2 from './sb_moik_week2/module_2.js';
// import module_3 from './sb_moik_week2/module_3.js';
// import module_4 from './sb_moik_week2/module_4.js';
import module_5 from './sb_moik_week2/module_5.js';
import module_6 from './sb_moik_week2/module_6.js';

export default {
    id: 'https://crmm.ru/xapi/courses/sb_moik_week2',
    name: 'Комплексная программа обучения МОИК. Неделя 2',
    type: 'course',
    version: 1,
    virtual: 'https://crmm.ru/xapi/courses/sb_mik-3',
    config: {
        color: 'green'
    },
    items: [
        module_0, // Ипотечные программы (Уровень 2)
        module_1, // Особые условия и акции (Уровень 2)
        module_2, // Способы расчетов (Уровень 2)
        // module_3, // После выдачи (Уровень 2)
        // module_4, // Взаимодействие с РКС (ЦОППиИК)
        module_5, // Прием заявлений на изменение условий кредитования
        module_6 // Итоговое тестирование
    ],
    objectives: [{
        id: 'https://crmm.ru/xapi/courses/sb_moik_week2/ms_0_0',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/6ysvxl39',
        pass_time: '1',
        icon: './static/vshs_icons/moik_week2/0.png',
        color: '#47b27e',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_moik_week2/ms_0_1',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/lw6zl6mj',
        pass_time: '1',
        icon: './static/vshs_icons/moik_week2/1.png',
        color: '#47b27e',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_moik_week2/ms_0_2',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/cnyyy3b1',
        pass_time: '1',
        icon: './static/vshs_icons/moik_week2/2.png',
        color: '#47b27e',
        scoped: true
    }, 
    // {
    //     id: 'https://crmm.ru/xapi/courses/sb_moik_week2/ms_0_3',
    //     target: 'https://crmm.ru/xapi/courses/sb_mik-3/4x9z7prp',
    //     pass_time: '1',
    //     icon: './static/vshs_icons/moik_week2/3.png',
    //     color: '#47b27e',
    //     scoped: true
    // },
    /*{
        id: 'https://crmm.ru/xapi/courses/sb_moik_week2/ms_0_4',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/9htsnxrw',
        pass_time: '1',
        icon: './static/vshs_icons/moik_week2/4.png',
        color: '#47b27e',
        scoped: true
    },*/ {
        id: 'https://crmm.ru/xapi/courses/sb_moik_week2/ms_0_5',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/ayqu6txl',
        pass_time: '1',
        icon: './static/vshs_icons/moik_week2/5.png',
        color: '#47b27e',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_moik_week2/ms_0_6',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/47noczbx',
        pass_time: '1',
        icon: './static/vshs_icons/global/final_test_new.png',
        color: '#47b27e',
        scoped: true
    }]
};
