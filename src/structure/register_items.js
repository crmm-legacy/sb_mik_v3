import { ItemTypes } from 'course-structure';

export class Td extends ItemTypes.Item {
    constructor(obj, parent, root, options) {
        super(obj, parent, root, options);
        this.passOnDemand = true;
    }
};

export class Pract extends ItemTypes.Item {
    constructor(obj, parent, root, options) {
        super(obj, parent, root, options);
        this.passOnDemand = true;
    }
};

export class Pdf extends ItemTypes.Item {
    constructor(obj, parent, root, options) {
        super(obj, parent, root, options);
    }
};

ItemTypes.Td = Td;
ItemTypes.Pract = Pract;
ItemTypes.Pdf = Pdf;
