import final_test_nozob3vx from '@items/test/final_test_nozob3vx.js';

export default {
    id: '/nbsmq5es',
    type: 'item',
    component: 'chapter',
    name: 'Итоговый тест',
    config: {},
    items: [
        final_test_nozob3vx
    ]
};
