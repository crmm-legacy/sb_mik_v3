import chapter_0 from './module_0/chapter_0';
// import chapter_1 from './module_0/chapter_1';
import chapter_2 from './module_0/chapter_2';
// import chapter_3 from './module_0/chapter_3';

export default {
    id: '/wwenpusc',
    name: 'Ипотечные программы (Уровень 3)',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0, // Военная ипотека
        // chapter_1, // Сопровождение военное ипотеки
        chapter_2 // Рефинансирование
        // chapter_3  // Сопровождение рефинансируемого кредита
    ]
};
