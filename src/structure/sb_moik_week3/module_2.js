// import chapter_0 from './module_2/chapter_0';
// import chapter_1 from './module_2/chapter_1';
// import chapter_2 from './module_2/chapter_2';
// import chapter_3 from './module_2/chapter_3';
// import chapter_4 from './module_2/chapter_4';
// import chapter_5 from './module_2/chapter_5';

export default {
    id: '/r0bw4zyq',
    name: 'Взаимодействие с ЦОППиИК (Уровень 2)',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        // chapter_0, // Замена кредитуемого Объекта недвижимости после выдачи кредита
        // chapter_1, // Предоставление отсрочек в погашении основного долга
        // chapter_2, // На регистрацию ЗОН, построенного на залоговом земельном участке / на строительство ОН на залоговом земельном участке за счет собственных средств Заемщика
        // chapter_3, // На изменение договора аренды земельного участка/ответ касательно уведомления Банка о смене арендодателя
        // chapter_4, // На изменение Договора-основания инвестирования строительства
        // chapter_5  // На изменение залогового объекта недвижимости
    ]
};
