import video_xpenhc7 from '@items/video/video_xpenhc7.js';
import pdf_jg195mph from '@items/pdf/pdf_jg195mph.js';
import test_i7a9sip8 from '@items/test/test_i7a9sip8.js';

export default {
    id: '/0fstr3b0',
    type: 'item',
    component: 'chapter',
    name: 'Военная ипотека',
    config: {},
    items: [
        video_xpenhc7,
        pdf_jg195mph,
        test_i7a9sip8
    ]
};
