import video_nz1s897n from '@items/video/video_nz1s897n.js';
import test_n83l3o4h from '@items/test/test_n83l3o4h.js';

export default {
    id: '/zs0xh56z',
    type: 'item',
    component: 'chapter',
    name: 'Рефинансирование',
    config: {},
    items: [
        video_nz1s897n,
        test_n83l3o4h
    ]
};
