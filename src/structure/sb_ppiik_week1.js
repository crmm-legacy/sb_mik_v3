import module_0 from './sb_ppiik_week1/module_0.js';
import module_1 from './sb_ppiik_week1/module_1.js';
import module_2 from './sb_ppiik_week1/module_2.js';
import module_3 from './sb_ppiik_week1/module_3.js';
import module_4 from './sb_ppiik_week1/module_4.js';
import module_5 from './sb_ppiik_week1/module_5.js';
import module_6 from './sb_ppiik_week1/module_6.js';
import module_7 from './sb_ppiik_week1/module_7.js';
import module_8 from './sb_ppiik_week1/module_8.js';

export default {
    id: 'https://crmm.ru/xapi/courses/sb_ppiik_week1',
    name: 'Программа подготовки КМ (ПЦП ППиИК) Неделя 1',
    type: 'course',
    version: 1,
    virtual: 'https://crmm.ru/xapi/courses/sb_mik-3',
    config: {
        color: 'blue'
    },
    items: [
        module_0,
        module_1,
        module_2,
        module_3,
        module_4,
        module_5,
        module_6,
        module_7,
        module_8
    ],
    objectives: [{
        id: 'https://crmm.ru/xapi/courses/sb_ppiik_week1/ms_0_0',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/ekze7ion',
        pass_time: '1',
        icon: './static/vshs_icons/ppiik_week1/0.png',
        color: '#64a7db',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_ppiik_week1/ms_0_1',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/ax9jmq6a',
        pass_time: '1',
        icon: './static/vshs_icons/ppiik_week1/1.png',
        color: '#64a7db',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_ppiik_week1/ms_0_2',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/n80ppnpi',
        pass_time: '1',
        icon: './static/vshs_icons/ppiik_week1/2.png',
        color: '#64a7db',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_ppiik_week1/ms_0_3',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/kntsec61',
        pass_time: '1',
        icon: './static/vshs_icons/ppiik_week1/3.png',
        color: '#64a7db',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_ppiik_week1/ms_0_4',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/ld9s56bh',
        pass_time: '1',
        icon: './static/vshs_icons/ppiik_week1/4.png',
        color: '#64a7db',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_ppiik_week1/ms_0_5',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/0owu75av',
        pass_time: '1',
        icon: './static/vshs_icons/ppiik_week1/5.png',
        color: '#64a7db',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_ppiik_week1/ms_0_6',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/18rtj69p',
        pass_time: '1',
        icon: './static/vshs_icons/ppiik_week1/6.png',
        color: '#64a7db',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_ppiik_week1/ms_0_7',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/8cbj59y7',
        pass_time: '1',
        icon: './static/vshs_icons/ppiik_week1/7.png',
        color: '#64a7db',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_ppiik_week1/ms_0_8',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/ye6017l8',
        pass_time: '1',
        icon: './static/vshs_icons/global/final_test_new.png',
        color: '#64a7db',
        scoped: true
    }]
};
