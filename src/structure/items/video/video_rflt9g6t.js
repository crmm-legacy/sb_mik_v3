export default {
    id: '/rflt9g6t',
    name: 'Стандарты общения с Клиентом при личном взаимодействии',
    type: 'video',
    component: 'videoplayer',
    config: {},
    src: 'week1/chapter8/03_Standarty_lichnoe_obshenie.mp4',
    oldId: '/8/2/0'
};
