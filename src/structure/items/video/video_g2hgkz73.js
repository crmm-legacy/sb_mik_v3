export default {
    id: '/g2hgkz73',
    name: 'Скринкаст',
    type: 'video',
    component: 'videoplayer',
    config: {
        screencast: true
    },
    src: 'week1/chapter6/6_how_to_change.mp4'
};
