export default {
    id: '/d6w5jgn3',
    name: 'Скринкаст',
    type: 'video',
    component: 'videoplayer',
    config: {
        screencast: true
    },
    src: 'week1/chapter6/Screeencast_konstruktor_ip_sdelok.mp4'
};
