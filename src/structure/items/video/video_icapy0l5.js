export default {
    id: '/icapy0l5',
    name: 'Скринкаст',
    type: 'video',
    component: 'videoplayer',
    config: {
        screencast: true
    },
    src: 'week1/chapter6/PE_screencast.mp4',
    oldId: '/5/4/0'
};
