export default {
    id: '/kmpvcfj6',
    name: 'Скринкаст',
    type: 'video',
    component: 'videoplayer',
    config: {
        screencast: true
    },
    src: 'week1/chapter7/Screencast_SBR.mp4',
    oldId: '/6/0/3'
};
