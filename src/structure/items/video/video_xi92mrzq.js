export default {
    id: '/xi92mrzq',
    name: 'Скринкаст',
    type: 'video',
    component: 'videoplayer',
    config: {
        screencast: true
    },
    src: 'week1/chapter6/ocenka_ned.mp4',
    oldId: '/5/2/1'
};
