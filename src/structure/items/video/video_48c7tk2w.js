export default {
    id: '/48c7tk2w',
    name: 'Скринкаст',
    type: 'video',
    component: 'videoplayer',
    config: {
        screencast: true
    },
    src: 'week1/chapter6/Screen_SER.mp4',
    oldId: '/5/3/2'
};
