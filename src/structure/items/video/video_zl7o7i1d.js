export default {
    id: '/zl7o7i1d',
    name: 'Скринкаст',
    type: 'video',
    component: 'videoplayer',
    config: {
        screencast: true
    },
    src: 'week1/chapter6/Screen_Pol.mp4',
    oldId: '/5/5/0'
};
