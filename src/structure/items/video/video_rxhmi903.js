export default {
    id: '/rxhmi903',
    name: 'Скринкаст',
    type: 'video',
    component: 'videoplayer',
    config: {
        screencast: true
    },
    src: 'week1/chapter6/Programs_(Partner,Tranzakt).mp4',
    oldId: '/5/5/1'
};
