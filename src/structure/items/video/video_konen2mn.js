export default {
    id: '/konen2mn',
    name: 'Как изменить данные клиента',
    type: 'video',
    component: 'videoplayer',
    config: {
        screencast: true
    },
    src: 'week1/chapter6/screen_kak_izmenit_dannye.mp4'
};
