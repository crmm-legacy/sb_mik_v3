export default {
    id: '/xfoo1xcd',
    name: 'Скринкаст',
    type: 'video',
    component: 'videoplayer',
    config: {
        screencast: true
    },
    src: 'week1/chapter7/screen_kak_oformit_spravki.mp4'
};
