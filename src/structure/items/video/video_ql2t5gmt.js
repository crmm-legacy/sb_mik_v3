export default {
    id: '/ql2t5gmt',
    name: 'Как найти кредитный договор',
    type: 'video',
    component: 'videoplayer',
    config: {
        screencast: true
    },
    src: 'week1/chapter6/screen_kak_naiti_kred_dogovor.mp4'
};
