export default {
    id: '/rkqe0wz1',
    name: 'Знакомство с компанией и ценностями Банка',
    type: 'video',
    component: 'videoplayer',
    src: 'week1/chapter1/01_znakomstvo.mp4'
};
