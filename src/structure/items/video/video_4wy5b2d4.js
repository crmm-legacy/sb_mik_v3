export default {
    id: '/4wy5b2d4',
    name: 'Скринкаст',
    type: 'video',
    component: 'videoplayer',
    config: {
        screencast: true
    },
    src: 'week1/chapter6/screen_dosrochnoe_pogashenie.mp4'
};
