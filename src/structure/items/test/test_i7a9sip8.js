export default {
    id: '/i7a9sip8',
    name: 'Военная ипотека. Тест',
    type: 'test',
    component: 'test_compo',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'checkbox',
            id: 'vcbu0zcr',
            text: 'Кредит в рамках военной ипотеки может предоставляться на приобретение:',
            description: '',
            answers: [{
                id: 'o7r7qhr2',
                text: 'Таунхауса',
                correct: true
            }, {
                id: '0ooteirm',
                text: 'Комнаты',
                correct: true
            }, {
                id: 'ssm773ea',
                text: 'Квартиры',
                correct: true
            }, {
                id: '9yl4tl54',
                text: 'Земельного участка',
                correct: false
            }, {
                id: 'tsvzqztd',
                text: 'Доли в праве собственности на квартиру',
                correct: false
            }]
        },
        {
            type: 'radio',
            id: 'nvqoebtzp',
            text: 'В рамках военной ипотеки при приобретении готового жилья в качестве обеспечения оформляется:',
            description: '',
            answers: [{
                id: '8wxi5lg5',
                text: 'Залог кредитуемого объекта недвижимости',
                correct: true
            }, {
                id: 'kz4a0hn7',
                text: 'Залог иного объекта недвижимости',
                correct: false
            }, {
                id: '6v2brzwp',
                text: 'Поручительство платежеспособных физических лиц',
                correct: false
            }, {
                id: '034aqb38',
                text: 'Всё перечисленное',
                correct: false
            }]
        },
        {
            type: 'checkbox',
            id: '8sw159nn',
            text: 'В рамках военной ипотеки на объект недвижимости оформляется обременение в пользу:',
            description: '',
            answers: [{
                id: 'aypssd38',
                text: 'Банка',
                correct: true
            }, {
                id: 'hj8e3in2',
                text: 'Российской Федерации',
                correct: true
            }, {
                id: 'f9i1nqps',
                text: 'Министерства обороны РФ',
                correct: false
            }, {
                id: '3593dgwm',
                text: 'ФГКУ «Росвоенипотека» ',
                correct: false
            }]
        },
        {
            type: 'radio',
            id: 'dbe4vxpn',
            text: 'Учитывается ли платежеспособность заемщика при рассмотрении заявления на кредит?',
            description: '',
            answers: [{
                id: 'm69kq7xb',
                text: 'Да, учитывается, подтверждение доходов требуется',
                correct: false
            }, {
                id: 'ok5pt2zz',
                text: 'Нет, не учитывается, подтверждение доходов не требуется',
                correct: true
            }]
        },
        {
            type: 'radio',
            id: '5s48ojto',
            text: 'Предусмотрено ли наличие созаемщика при заключении договора на получение военной ипотеки?',
            description: '',
            answers: [{
                id: '12xfglfq',
                text: 'Нет, не предусмотрено',
                correct: true
            }, {
                id: 'e3qhi4mi',
                text: 'Да, предусмотрено',
                correct: false
            }, {
                id: 'he79olv9',
                text: 'Да, но не более одного созаемщика',
                correct: false
            }]
        }]
    }
};
