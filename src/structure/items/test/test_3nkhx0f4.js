export default {
    id: '/3nkhx0f4',
    name: 'Несовершеннолетний в сделке. Тест',
    type: 'test',
    component: 'test_compo',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            id: '58619g2r',
            type: 'radio',
            text: 'Укажите, верно ли утверждение «Родители могут самостоятельно распоряжаться квартирой, если одним из собственников является ребенок»',
            description: '',
            answers: [{
                id: '29yw9z1l',
                text: 'да, верно',
                correct: false
            }, {
                id: 'ep7vyuiz',
                text: 'нет, неверно, требуется получить согласие банка',
                correct: false
            }, {
                id: 'hgktazp8',
                text: 'нет, неверно, требуется получить согласие органов опеки',
                correct: true
            }]
        }, {
            id: 'w3vekc73',
            type: 'radio',
            text: 'Нужно обращаться в органы опеки, чтобы получить разрешение, если:',
            description: '',
            answers: [{
                id: '3vmayyjh',
                text: 'один из продавцов несовершеннолетний',
                correct: false
            }, {
                id: 'bretsjlk',
                text: 'один из залогодателей несовершеннолетний',
                correct: false
            }, {
                id: '6d46lcym',
                text: 'требуется отказ от преимущественного права покупки, когда соседней комнатой в коммунальной квартире владеет несовершеннолетний',
                correct: false
            }, {
                id: '18ks3vkx',
                text: 'все перечисленное верно',
                correct: true
            }]
        }, {
            id: 'daim1owq',
            type: 'checkbox',
            text: 'Укажите, что должно содержать разрешение органов опеки и попечительства',
            description: '',
            answers: [{
                id: 'd5j5itjm',
                text: 'указание лица, которому дается разрешение',
                correct: true
            }, {
                id: '9swphfds',
                text: 'наименование сделки или сделок, на совершение которых дается разрешение',
                correct: true
            }, {
                id: '5g4rsmp6',
                text: 'описание объектов недвижимости, сделки с которыми будут  совершены',
                correct: true
            }, {
                id: 'iggek8et',
                text: 'паспортные данные покупателя и продавца',
                correct: false
            }]
        }, {
            id: '6w7zo1x2',
            type: 'checkbox',
            text: 'Требуется ли разрешение органов опеки, если ребенок не является собственником части недвижимости, а только прописан в квартире?',
            description: '',
            answers: [{
                id: 'ffo6788p',
                text: 'да',
                correct: false
            }, {
                id: 'yuzon236',
                text: 'нет',
                correct: true
            }]
        }]
    }
};
