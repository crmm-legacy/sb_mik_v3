export default {
    id: '/g8ge59ou',
    name: 'Тест',
    type: 'test',
    component: 'test_compo',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'checkbox',
            id: '',
            text: '',
            description: '',
            answers: [{
                id: '',
                text: '',
                correct: true
            }, {
                id: '',
                text: '',
                correct: true
            }, {
                id: '',
                text: '',
                correct: false
            }]
        }, {
            type: 'radio',
            id: '',
            text: '',
            description: '',
            answers: [{
                id: '',
                text: '',
                correct: true
            }, {
                id: '',
                text: '',
                correct: false
            }]
        }]
    }
};
