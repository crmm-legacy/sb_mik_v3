export default {
    id: '/zt29248t',
    name: 'Гараж. Тест',
    type: 'test',
    component: 'test_compo',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            id: '9a5cjxa8',
            type: 'radio',
            text: 'Должен ли гараж/машиноместо быть оформленным как объект недвижимости для получения кредита на его покупку?',
            description: '',
            answers: [{
                id: '5rvx4fuq',
                text: 'Только гараж',
                correct: false
            }, {
                id: '3ywo77qp',
                text: 'Только машиноместо',
                correct: false
            }, {
                id: 'aps5zy9p',
                text: 'И гараж, и машиноместо',
                correct: true
            }, {
                id: 'ocxxcejj',
                text: 'Не должно ни то, ни другое',
                correct: false
            }]
        }, {
            id: '4mv4pl7a',
            type: 'radio',
            text: 'Какой первоначальный взнос нужен при покупке в ипотеку гаража или машиноместа?',
            description: '',
            answers: [{
                id: 'hsqj8sta',
                text: '15%',
                correct: false
            }, {
                id: 'zoj5togs',
                text: '20%',
                correct: false
            }, {
                id: 'zrun52eh',
                text: '25%',
                correct: true
            }, {
                id: 'xwv7leaq',
                text: '10%',
                correct: false
            }]
        }, {
            id: 'dqerwjh2',
            type: 'checkbox',
            text: 'Какой документ НЕ нужно предоставлять при приобретении гаража или машиноместа, если в залог передается кредитуемый объект?',
            description: '',
            answers: [{
                id: 'oe1f0ejj',
                text: 'Отчет об оценке',
                correct: true
            }, {
                id: 'v10smy55',
                text: 'Договор-основание приобретения',
                correct: false
            }, {
                id: 'wteyxr0b',
                text: 'Выписка из ЕГРН',
                correct: false
            }, {
                id: 'h7d3no4b',
                text: 'Технический паспорт',
                correct: true
            }]
        }, {
            id: '9vnolxf4',
            type: 'checkbox',
            text: 'Укажите верные утверждения об обеспечении кредита по программе «Гараж»',
            description: '',
            answers: [{
                id: 'pza1ma4a',
                text: 'Если кредит до 1,5 млн и договор-основание – ДДУ, то единственным обеспечением может быть залог прав требования',
                correct: true
            }, {
                id: '0im04id9',
                text: 'Если кредит до 1,5 млн и договор-основание – ДКП, то единственным обеспечением может быть поручительство',
                correct: true
            }, {
                id: 'j95z9yx1',
                text: 'Если кредит более 1,5 млн и договор-основание – ДКП, то единственным обеспечением может быть поручительство',
                correct: false
            }, {
                id: 'naj4q339',
                text: 'Обеспечением может быть залог кредитуемого или иного объекта недвижимости, в том числе апартаменты',
                correct: false
            }]
        }]
    }
};
