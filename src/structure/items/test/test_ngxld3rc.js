export default {
    id: '/ngxld3rc',
    name: 'Участники сделки. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/0/4/1',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'radio',
            id: 'hqh1j5jf',
            text: 'В каком качестве участвует в сделке супруг(-а) титульного созаемщика, чей доход не учитывается?',
            description: '',
            answers: [{
                id: '3qj27705',
                text: 'Созаемщик с учетом платежеспособности',
                correct: false
            }, {
                id: '9awtkylh',
                text: 'Созаемщик без учета платежеспособности',
                correct: true
            }, {
                id: 'q0a39un0',
                text: 'Поручитель с учетом платежеспособности',
                correct: false
            }, {
                id: 'y7yjlxa0',
                text: 'Поручитель без учета платежеспособности',
                correct: false
            }, {
                id: 'b5eecotx',
                text: 'Не может участвовать в сделке',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'q7ebtk3b',
            text: 'В каком качестве участвуют в сделке родственники 1 степени титульного созаемщика (дети, родители)?',
            description: '',
            answers: [{
                id: 'c86lkcv7',
                text: 'Созаемщик с учетом платежеспособности',
                correct: false
            }, {
                id: '8scw5qbd',
                text: 'Поручитель с учетом платежеспособности',
                correct: false
            }, {
                id: 'qjcmzv7h',
                text: 'Поручитель без учета платежеспособности',
                correct: true
            }, {
                id: 'il63c6ax',
                text: 'Не могут участвовать в сделке',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'enz0mkuv',
            text: 'В каком качестве участвует в сделке супруг(-а) титульного созаемщика без гражданства РФ?',
            description: '',
            answers: [{
                id: 'cee10na0',
                text: 'Созаемщик с учетом платежеспособности',
                correct: false
            }, {
                id: 'i3bfd4fw',
                text: 'Созаемщик без учета платежеспособности',
                correct: false
            }, {
                id: 'bqeor3tg',
                text: 'Поручитель с учетом платежеспособности',
                correct: false
            }, {
                id: '2b23mhyf',
                text: 'Поручитель без учета платежеспособности',
                correct: false
            }, {
                id: '0zickgn7',
                text: 'Не участвует в сделке',
                correct: true
            }]
        }, {
            type: 'radio',
            id: 'tpn8b3l7',
            text: 'Созаемщик с учетом платежеспособности влияет на размер одобренной суммы кредита?',
            description: '',
            answers: [{
                id: 'fr9e0fog',
                text: 'Да',
                correct: true
            }, {
                id: 'kz4al4ni',
                text: 'Нет',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'afhc1jhp',
            text: 'Поручитель с учетом платежеспособности влияет на размер одобренной суммы кредита?',
            description: '',
            answers: [{
                id: 'trc42sax',
                text: 'Да',
                correct: false
            }, {
                id: 'ilp7bb9j',
                text: 'Нет',
                correct: true
            }]
        }, {
            type: 'radio',
            id: '6ju5tu1i',
            text: 'Залогодателем может быть человек, который предоставил свой ОН в залог Банку и не является созаемщиком по кредиту:',
            description: '',
            answers: [{
                id: 'bn3rk1yv',
                text: 'Верно',
                correct: true
            }, {
                id: 'c8t8zfn0',
                text: 'Неверно',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'frzxbqta',
            text: 'Кто может быть залогодателем при залоге КОН?',
            description: '',
            answers: [{
                id: '57wimorh',
                text: 'Только титульный созаемщик',
                correct: false
            }, {
                id: 'qiyo7hdx',
                text: 'Титульный созаемщик и его супруг(-а)',
                correct: false
            }, {
                id: 'mixa6ai1',
                text: 'Любое лицо',
                correct: true
            }]
        }, {
            type: 'checkbox',
            id: 'q4yf2xuv',
            text: 'В каком качестве может участвовать в сделке гражданский супруг (сожитель) (при наличии гражданства РФ)?',
            description: '',
            answers: [{
                id: 'h6k5b1z1',
                text: 'Созаемщик с учетом платежеспособности',
                correct: true
            }, {
                id: 'qi8vudc4',
                text: 'Созаемщик без учета платежеспособности',
                correct: false
            }, {
                id: 'nrna8c9n',
                text: 'Поручитель с учетом платежеспособности',
                correct: true
            }, {
                id: '06csp5ss',
                text: 'Поручитель без учета платежеспособности',
                correct: false
            }, {
                id: 'l0ncyx2q',
                text: 'Не может участвовать в сделке',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'q9smn2nx',
            text: 'Сколько может быть созаемщиков по одному жилищному кредиту?',
            description: '',
            answers: [{
                id: 'nb68xwu7',
                text: 'Один',
                correct: false
            }, {
                id: '2au21wsf',
                text: 'Два',
                correct: false
            }, {
                id: 'cze3l534',
                text: 'Три',
                correct: true
            }]
        }, {
            type: 'radio',
            id: 'au0cvz83',
            text: 'Сколько может быть продавцов в сделке?',
            description: '',
            answers: [{
                id: 'y8a6gky9',
                text: 'Три',
                correct: false
            }, {
                id: '2c2juy10',
                text: 'Пять',
                correct: true
            }, {
                id: 't5lmbjio',
                text: 'Восемь',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'laqy7mdq',
            text: 'Кто из участников сделки может осуществлять частично досрочное погашение ипотеки, заказывать справки по ипотечному кредиту?',
            description: '',
            answers: [{
                id: 'g5ymlqvk',
                text: 'Титульный созаемщик',
                correct: true
            }, {
                id: 'i71mshy5',
                text: 'Любой из созаемщиков',
                correct: false
            }, {
                id: 'k6xggxn9',
                text: 'Поручитель',
                correct: false
            }, {
                id: '9wjfhwnx',
                text: 'Совершеннолетние дети титульного созаемщика',
                correct: false
            }]
        }]
    }
};
