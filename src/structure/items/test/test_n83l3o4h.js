export default {
    id: '/n83l3o4h',
    name: 'Рефинансирование. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/20/1/2',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'checkbox',
            id: '6ju30s0i',
            text: 'Укажите, какие кредиты можно рефинансировать в Сбербанке:',
            description: '',
            answers: [{
                id: 'jccn2q8s',
                text: 'Кредитную карту другого банка',
                correct: true
            }, {
                id: 'oanygi75',
                text: 'Автокредит Сбербанка',
                correct: true
            }, {
                id: 'tv8kcbc1',
                text: 'Дебетовую карту с овердрафтом другого банка',
                correct: true
            }, {
                id: 'm63xvttt',
                text: 'Ипотечный кредит другого банка',
                correct: true
            }, {
                id: 'm6m3u3ux',
                text: 'Автокредит другого банка',
                correct: true
            }, {
                id: 'x0tny854',
                text: 'Потребительский кредит Сбербанка',
                correct: true
            }, {
                id: 'wv4xatpk',
                text: 'Дебетовую карту с овердрафтом Сбербанка',
                correct: false
            }, {
                id: 'vacsg81s',
                text: 'Ипотечный кредит Сбербанка',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 'lwaf6nfn',
            text: 'Укажите требования к рефинансируемым кредитам:',
            description: '',
            answers: [{
                id: 'vroe2rsz',
                text: 'Рефинансируемых кредитов может быть не более 5',
                correct: true
            }, {
                id: 'a8lszw81',
                text: 'Кредит должен быть выдан не менее 6 месяцев назад',
                correct: true
            }, {
                id: 'h3plt5yb',
                text: 'До окончания срока каждого кредита должно быть не менее 6 месяцев',
                correct: false
            }, {
                id: 'zwxmye2v',
                text: 'Рефинансируемых кредитов может быть не более 3',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'zegqfik8',
            text: 'В какие сроки должна быть предоставлена справка о погашении ипотечного кредита?',
            description: '',
            answers: [{
                id: 'wlpjv5d8',
                text: '2 месяца с даты выдачи первого транша',
                correct: true
            }, {
                id: 'yztrqhfu',
                text: '45 дней с даты выдачи второго транша',
                correct: false
            }, {
                id: '6669qfix',
                text: '3 месяца с даты выдачи первого транша',
                correct: false
            }, {
                id: '9evdcnq8',
                text: '30 дней с даты выдачи первого транша',
                correct: false
            }]
        }, {
            type: 'radio',
            id: '036juwix',
            text: 'Можно ли рефинансировать кредит, сменив титульного созаемщика?',
            description: '',
            answers: [{
                id: 'ykxk42nk',
                text: 'Нет',
                correct: true
            }, {
                id: 'ryu5s2kh',
                text: 'Да',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'c9yzxw4q',
            text: 'Возможно ли рефинансировать кредит, выданный на строящееся жилье, на которое еще не оформлено право собственности?',
            description: '',
            answers: [{
                id: '168f136g',
                text: 'Да, в залог отдается иной (некредитуемый) объект',
                correct: true
            }, {
                id: 'rf2itplk',
                text: 'Да, в залог отдается рефинансируемый объект',
                correct: false
            }, {
                id: 'ipmt11h0',
                text: 'Нет, это невозможно',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 't95o0q8f',
            text: 'В какой срок должен быть оформлен залог объекта недвижимости в пользу Банка?',
            description: '',
            answers: [{
                id: 'w8lwm838',
                text: '3 месяца',
                correct: true
            }, {
                id: 'abyruozs',
                text: '2 месяца',
                correct: false
            }, {
                id: 'j03363u6',
                text: '4 месяца',
                correct: false
            }, {
                id: 'l4wyrtno',
                text: '5 месяца',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'qo1z6it2',
            text: 'В какие сроки должна быть предоставлена справка о погашении потребительского кредита?',
            description: '',
            answers: [{
                id: '9dm9ay9k',
                text: '45 дней с даты выдачи второго транша',
                correct: true
            }, {
                id: 'pr8s8rja',
                text: '2 месяца с даты выдачи первого транша',
                correct: false
            }, {
                id: 'eo4ld5du',
                text: '45 дней с даты выдачи первого транша',
                correct: false
            }, {
                id: '942rnb9i',
                text: '2 месяца с даты выдачи второго транша',
                correct: false
            }]
        }]
    }
};
