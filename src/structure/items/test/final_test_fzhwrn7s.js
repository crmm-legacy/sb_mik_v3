export default {
    id: '/fzhwrn7s',
    name: 'Финальное тестирование',
    type: 'test',
    component: 'test_compo',
    content: {
        name: 'Итоговое тестирование по курсу',
        type: 'test'
    },
    shuffle: true,
    config: {
        isFinalTest: true,
        basedOn: [
            // module_0
            {
                id: '/k9cb5xne',
                numOfQuestions: 1
            },
            {
                id: '/o6uzp13h',
                numOfQuestions: 1
            },
            {
                id: '/1u5ontk6',
                numOfQuestions: 1
            },
            {
                id: '/zt29248t',
                numOfQuestions: 1
            },
            // module_1
            {
                id: '/aovlgp46',
                numOfQuestions: 1
            },
            {
                id: '/smamddlo',
                numOfQuestions: 1
            },
            {
                id: '/1dxoqkpq',
                numOfQuestions: 1
            },
            // module_2
            {
                id: '/8xqhn3hf',
                numOfQuestions: 1
            },
            // module_5
            {
                id: '/5dl67yc1',
                numOfQuestions: 1
            },
            {
                id: '/jhfv81xp',
                numOfQuestions: 1
            },
            {
                id: '/ruivwq3j',
                numOfQuestions: 1
            },
            {
                id: '/7uzbb0mw',
                numOfQuestions: 1
            }
        ]
    }
};
