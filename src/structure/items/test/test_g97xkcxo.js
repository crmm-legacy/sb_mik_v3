export default {
    id: '/g97xkcxo',
    name: 'Досрочное погашение кредита и неустойка. Тест',
    type: 'test',
    component: 'test_compo',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'radio',
            id: 'p1evb5g9',
            text: 'Как клиент может провести досрочное погашение с уменьшением срока кредита?',
            description: '',
            answers: [{
                id: '2jfycjc3',
                text: 'В офисе банка',
                correct: true
            }, {
                id: '5akyuqlm',
                text: 'В Сбербанк Онлайн',
                correct: false
            }, {
                id: 'o90j1rbw',
                text: 'Позвонив в контактный центр банка',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: '99342xnw',
            text: 'По каким причинам могла возникнуть просроченная задолженность?',
            description: '',
            answers: [{
                id: 'rj8gbmb3',
                text: 'По каким причинам могла возникнуть просроченная задолженность?',
                correct: true
            }, {
                id: 'yaexlp0o',
                text: 'Заемщик внес деньги на счет, который не указан в поручении на списание',
                correct: true
            }, {
                id: 'yyk8dj52',
                text: 'Уменьшение процентной ставки по кредиту',
                correct: false
            }, {
                id: 'oofxakg4',
                text: 'Изменение состава обеспечения по кредиту',
                correct: false
            }]
        }, {
            type: 'radio',
            id: '2xsg3qc0',
            text: 'При погашении кредита средствами материнского капитала',
            description: '',
            answers: [{
                id: 'mun2m8qt',
                text: 'Уменьшается сумма ежемесячных платежей',
                correct: true
            }, {
                id: 'qxhvwnns',
                text: 'Уменьшается срок кредита',
                correct: false
            }, {
                id: 'hqx9ykbd',
                text: 'Можно выбрать любой из вариантов',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'j7509sxc',
            text: 'Может ли созаемщик осуществить досрочное погашение кредита?',
            description: '',
            answers: [{
                id: 'nb0oik1q',
                text: 'Да, при предъявлении нотариальной доверенности, в которой указаны полномочия на подачу заявления на досрочное погашение и списание средств со счета титульного созаемщика, и паспорта',
                correct: true
            }, {
                id: 'g4izheai',
                text: 'Нет',
                correct: false
            }, {
                id: 'mts8ang1',
                text: 'Может при предъявлении паспорта',
                correct: false
            }]
        }, {
            type: 'radio',
            id: '1w513765',
            text: 'Возникла просроченная задолженность по кредиту. На следующий день клиент внес денежные средства на счет, который привязан к кредитному договору. Пройдет ли списание в погашение просроченной задолженности?',
            description: '',
            answers: [{
                id: 'poidwzkz',
                text: 'Нет',
                correct: false
            }, {
                id: 'seps7878',
                text: 'Да',
                correct: true
            }]
        }]
    }
};
