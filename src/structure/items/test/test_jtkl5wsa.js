export default {
    id: '/jtkl5wsa',
    name: 'Страхование залога. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/5/1/1',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'radio',
            id: '1tea6a6m',
            text: 'При покупке строящегося жилья Клиент:',
            description: '',
            answers: [{
                id: 'f7tn00sx',
                text: 'Не обязан оформлять полис страхования залога',
                correct: false
            }, {
                id: 'kxreqla5',
                text: 'Обязан сразу оформить полис страхования после регистрации ДДУ',
                correct: false
            }, {
                id: 't7xqrnjp',
                text: 'Должен оформить полис страхования после того, как будет зарегистрировано право собственности и залог',
                correct: true
            }]
        }, {
            type: 'checkbox',
            id: 'dno1mj6o',
            text: 'Можно застраховать следующие типы недвижимости:',
            description: '',
            answers: [{
                id: 'hv5vjlqj',
                text: 'Комната',
                correct: true
            }, {
                id: 'qmzs2m9f',
                text: 'Доля в квартире',
                correct: false
            }, {
                id: '5y9zg99l',
                text: 'Квартира',
                correct: true
            }, {
                id: 'k6x6zkj7',
                text: 'Жилой дом',
                correct: true
            }]
        }, {
            type: 'checkbox',
            id: 'rrbenktg',
            text: 'Укажите, какие части недвижимого имущества могут быть застрахованы:',
            description: '',
            answers: [{
                id: 'xapikwh4',
                text: 'Пол и потолок',
                correct: true
            }, {
                id: 'u72rjx1f',
                text: 'Стены',
                correct: true
            }, {
                id: 'irmyrzbu',
                text: 'Ремонт',
                correct: false
            }, {
                id: 'amix80xh',
                text: 'Окна и входные двери',
                correct: true
            }]
        }, {
            type: 'radio',
            id: '8e0m1nfk',
            text: 'В СК «Сбербанк страхование» на страхование принимаются объекты не ранее… года постройки:',
            description: '',
            answers: [{
                id: 'eidool78',
                text: 1955,
                correct: true
            }, {
                id: '447iu0dk',
                text: 1965,
                correct: false
            }, {
                id: 'vg44gj0v',
                text: 1945,
                correct: false
            }, {
                id: 'kqfsh24o',
                text: 1950,
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 'v5mh5bg2',
            text: 'Укажите, какие ситуации относятся к страховым случаям:',
            description: '',
            answers: [{
                id: '5evona8f',
                text: 'Пожар',
                correct: true
            }, {
                id: 'n1v17fxl',
                text: 'Террористический акт',
                correct: false
            }, {
                id: 'jwlqo25h',
                text: 'Стихийные бедствия',
                correct: true
            }, {
                id: 'u69fuhxp',
                text: 'Падение на имущество опор линий электропередачи, средств наружной рекламы',
                correct: true
            }]
        }]
    }
};
