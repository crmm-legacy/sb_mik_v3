export default {
    id: '/1u5ontk6',
    name: 'Нецелевой кредит под залог. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/20/2/2',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'checkbox',
            id: 'x6xwdnvt',
            text: 'По продукту «Нецелевой кредит под залог недвижимости» в качестве обеспечения можно оформить в залог:',
            description: '',
            answers: [{
                id: 'mzi08e66',
                text: 'Квартиру',
                correct: true
            }, {
                id: 'zrlct50o',
                text: 'Гараж',
                correct: true
            }, {
                id: '9p4w5u04',
                text: 'Долю на квартиру',
                correct: false
            }, {
                id: '5hei4l8o',
                text: '2 земельных участка',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'h2ywzom6',
            text: 'Минимальный размер кредита по продукту «Нецелевой кредит под залог недвижимости»:',
            description: '',
            answers: [{
                id: 'i9e9wb97',
                text: '500 тыс. рублей',
                correct: true
            }, {
                id: 'hhhvuw5m',
                text: '300 тыс. рублей',
                correct: false
            }, {
                id: 'pfhjun3x',
                text: '1 млн рублей',
                correct: false
            }, {
                id: 'ppmewqa9',
                text: '600 тыс. рублей',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'k63ifxlp',
            text: 'Максимальный размер кредита по продукту «Нецелевой кредит под залог недвижимости»:',
            description: '',
            answers: [{
                id: 'r3h7og18',
                text: '10 млн рублей',
                correct: true
            }, {
                id: 'nc8nicdv',
                text: '30 млн рублей',
                correct: false
            }, {
                id: 'fy9c4avv',
                text: '20 млн рублей',
                correct: false
            }, {
                id: 'k6xk0tzh',
                text: '15 млн рублей',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'fzhkc2qi',
            text: 'Максимальный срок кредита по продукту «Нецелевой кредит под залог недвижимости»:',
            description: '',
            answers: [{
                id: '70mzg7by',
                text: '20 лет',
                correct: true
            }, {
                id: 'nwguxqvr',
                text: '30 лет',
                correct: false
            }, {
                id: 'biawk9f1',
                text: '25 лет',
                correct: false
            }, {
                id: 'li9h59np',
                text: '15 лет',
                correct: false
            }]
        }, {
            type: 'radio',
            id: '1zt44d01',
            text: 'Максимальное количество залогодателей по продукту «Нецелевой кредит под залог недвижимости»:',
            description: '',
            answers: [{
                id: 'bn50wds5',
                text: '4',
                correct: true
            }, {
                id: 'rewtp3db',
                text: '3',
                correct: false
            }, {
                id: 'f57rgu23',
                text: '2',
                correct: false
            }, {
                id: 'cc2l843b',
                text: '6',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 'x5q9b34h',
            text: 'Нецелевой кредит под залог недвижимости не предоставляется, если заемщик или хотя бы один из созаемщиков является:',
            description: '',
            answers: [{
                id: 'kx78ksit',
                text: 'Индивидуальным предпринимателем',
                correct: true
            }, {
                id: '4mvg711z',
                text: 'Владельцем малого предприятия в доле более 5 %',
                correct: true
            }, {
                id: 's4gqln4j',
                text: 'Участником фермерского хозяйства',
                correct: true
            }, {
                id: '0wj7a0s4',
                text: 'Работником предприятия, прошедшего аккредитацию в Сбербанке',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'l1wjslgw',
            text: 'В рамках продукта «Нецелевой кредит под залог недвижимости» созаемщиком может быть только платежеспособный супруг или супруга, чей доход учитывается при расчете максимального размера кредита:',
            description: '',
            answers: [{
                id: 'zpdcck6d',
                text: 'Да',
                correct: true
            }, {
                id: 'benwp8l6',
                text: 'Нет',
                correct: false
            }]
        }]
    }
};
