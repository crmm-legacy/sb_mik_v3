export default {
    id: '/s1lrqukk',
    name: 'Финальное тестирование',
    type: 'test',
    component: 'test_compo',
    content: {
        name: 'Итоговое тестирование по курсу',
        type: 'test'
    },
    shuffle: true,
    config: {
        isFinalTest: true,
        basedOn: [
            // module_0
            {
                id: '/k9cb5xne',
                numOfQuestions: 1
            },
            {
                id: '/i7a9sip8',
                numOfQuestions: 1
            },
            {
                id: '/zt29248t',
                numOfQuestions: 1
            },
            // module_1
            {
                id: '/1dxoqkpq',
                numOfQuestions: 1
            },
            // module_2
            {
                id: '/3nkhx0f4',
                numOfQuestions: 1
            },
            {
                id: '/smamddlo',
                numOfQuestions: 1
            },
            // module_3
            {
                id: '/fikg7w7q',
                numOfQuestions: 1
            },
            {
                id: '/t6ickhq7',
                numOfQuestions: 1
            },
            {
                id: '/7nglymzx',
                numOfQuestions: 1
            }
        ]
    }
};
