export default {
    id: '/r3r7t5oy',
    name: 'Выявление причин выставленной Клиентом оценки. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/8/1/1',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'radio',
            id: 'yvos4yaj',
            text: 'Что делать, если Клиент не готов сейчас разговаривать?',
            description: '',
            answers: [{
                id: 'umdm1vki',
                text: 'Договориться созвониться в удобное для него время',
                correct: true
            }, {
                id: 'j591ml4j',
                text: 'Сказать Клиенту, что у Вас всего несколько коротких вопросов',
                correct: false
            }, {
                id: 'xrhdgh0c',
                text: 'Попросить Клиента, чтобы он перезвонил Вам в удобное ему время',
                correct: false
            }, {
                id: '8yg3dyck',
                text: 'Предупредить Клиента о времени следующего звонка',
                correct: false
            }]
        }, {
            type: 'radio',
            id: '0wvttlng',
            text: 'Какую рекомендованную фразу необходимо использовать для уточнения причин выставленной оценки?',
            description: '',
            answers: [{
                id: 'zyta7iun',
                text: '«<Имя-отчество Клиента>, спасибо Вам, что оценили наш сервис. Какие у Вас остались впечатления от работы с Вашим менеджером <имя>?»',
                correct: true
            }, {
                id: 'ahkb87h6',
                text: '«<Имя-отчество Клиента>, позвольте уточнить причины оценки, которую Вы поставили Вашему менеджеру <имя>»',
                correct: false
            }, {
                id: 'wetp33yw',
                text: '«<Имя-отчество Клиента>, спасибо Вам, что оценили наш сервис. Хочу поинтересоваться, что именно Вам не понравилось в работе нашего сотрудника»',
                correct: false
            }, {
                id: 'w3r59yov',
                text: '«<Имя-отчество Клиента>, пожалуйста, поделитесь своими впечатлениями от работы с Вашим менеджером <имя>»',
                correct: false
            }]
        }]
    }
};
