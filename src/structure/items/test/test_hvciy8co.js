export default {
    id: '/hvciy8co',
    name: 'Сервис безопасных расчетов. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/6/0/2',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'checkbox',
            id: 'kug8ps5e',
            text: 'Какие документы необходимы от продавца для оформления услуги?',
            description: '',
            answers: [{
                id: '9712eh2q',
                text: 'Паспорт',
                correct: true
            }, {
                id: 'luh0f2ut',
                text: 'Банковские реквизиты',
                correct: true
            }, {
                id: 'h5m3t6w3',
                text: 'ИНН',
                correct: false
            }, {
                id: '5cuauar5',
                text: 'СНИЛС',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: '1xkwdq9y',
            text: 'Какие документы необходимы от покупателя для оформления услуги?',
            description: '',
            answers: [{
                id: 'c7bo9dud',
                text: 'Паспорт',
                correct: true
            }, {
                id: 'jm5g5rep',
                text: 'ИНН',
                correct: true
            }, {
                id: 'qh3xwjf7',
                text: 'СНИЛС',
                correct: false
            }, {
                id: 'fed8s8dh',
                text: 'Банковские реквизиты',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 'qqbrvgfm',
            text: 'Оформление услуги невозможно, если:',
            description: '',
            answers: [{
                id: 'f22dj7fk',
                text: 'Продавец не является гражданином РФ',
                correct: true
            }, {
                id: 'l950ghqu',
                text: 'Собственность на объект оформлена до 1998 года',
                correct: true
            }, {
                id: '6wjpmp1v',
                text: '2 получателя средств',
                correct: false
            }, {
                id: 'm89hd8u0',
                text: 'У продавца отсутствует СНИЛС',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: '0bn5k3mm',
            text: 'Оформление услуги невозможно, если:',
            description: '',
            answers: [{
                id: '6na90dcp',
                text: 'Собственность на объект оформлена после 1998 года',
                correct: false
            }, {
                id: 'foi175nd',
                text: 'ДДУ / ДУПТ не имеют номера',
                correct: true
            }, {
                id: '8co4wog9',
                text: 'Сделка альтернативная',
                correct: true
            }, {
                id: 'sf9ixqje',
                text: 'Сделка по предварительному ДКП («мертвый период»)',
                correct: true
            }]
        }, {
            type: 'checkbox',
            id: 'ihaxofb2',
            text: 'Укажите верные утверждения о сервисе безопасных расчетов:',
            description: '',
            answers: [{
                id: 'h2jojnuf',
                text: 'Участники сделки получают платежные поручения, подтверждающие перевод средств по почте России',
                correct: false
            }, {
                id: '9qui05z6',
                text: 'Срок перевода денежных средств составляет от 1 до 5 рабочих дней',
                correct: true
            }, {
                id: '2a37fxrd',
                text: 'Для перевода рекомендуется использовать счета, открытые в Сбербанке',
                correct: true
            }, {
                id: 'nnyir9s0',
                text: 'Если в документах допущена ошибка, нужно заново оформлять договор СБР',
                correct: false
            }]
        }]
    }
};
