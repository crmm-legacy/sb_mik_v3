export default {
    id: '/o6uzp13h',
    name: 'Загородная недвижимость. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/20/0/2',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'radio',
            id: 'fmitnq0a',
            text: 'Минимальный размер первоначального взноса по продукту «Загородная недвижимость»:',
            description: '',
            answers: [{
                id: 'vyzjuxo7',
                text: '15 %',
                correct: false
            }, {
                id: 'ujcs0zrp',
                text: '20 %',
                correct: false
            }, {
                id: 'bvoctlgq',
                text: '25 %',
                correct: true
            }, {
                id: 'hdj85tou',
                text: '30 %',
                correct: false
            }]
        }, {
            type: 'radio',
            id: '370y5fb0',
            text: 'Максимальное количество объектов недвижимости, оформляемых в залог в рамках продукта «Загородная недвижимость»:',
            description: '',
            answers: [{
                id: 'iuj1n0eu',
                text: 6,
                correct: true
            }, {
                id: 'q0h44me4',
                text: 3,
                correct: false
            }, {
                id: 'ac3oz7hf',
                text: 4,
                correct: false
            }, {
                id: '4uc1zpvt',
                text: 8,
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'si6u6axw',
            text: 'Максимальное количество кредитуемых объектов недвижимости в рамках продукта «Загородная недвижимость»:',
            description: '',
            answers: [{
                id: '666a8o91',
                text: 3,
                correct: true
            }, {
                id: 'rajtp79l',
                text: 4,
                correct: false
            }, {
                id: 'ct90zg6o',
                text: 6,
                correct: false
            }, {
                id: 't8d9u00p',
                text: 8,
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'dkw4m1tg',
            text: 'В рамках продукта «Загородная недвижимость» возможно приобрести два соседних земельных участка?',
            description: '',
            answers: [{
                id: 'iiugyeeo',
                text: 'Да',
                correct: true
            }, {
                id: 'j7uctagg',
                text: 'Нет',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'sz4q8amd',
            text: 'В рамках продукта «Загородная недвижимость» по кредиту в сумме до 1,5 млн рублей возможно оформление в качестве обязательного обеспечения:',
            description: '',
            answers: [{
                id: '4dg19hhc',
                text: 'Поручительство платежеспособных физических лиц',
                correct: true
            }, {
                id: 'bg0wnv9b',
                text: 'Залог иного объекта недвижимости',
                correct: false
            }]
        }]
    }
};
