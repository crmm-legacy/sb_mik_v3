export default {
    id: '/0z2x3v4t',
    name: 'Анализ отчета об оценке. Тест',
    type: 'test',
    component: 'test_compo',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'checkbox',
            id: '74n6cqq8',
            text: 'В отчете об оценке обязательно должны быть',
            description: '',
            answers: [{
                id: 'o7n04dtn',
                text: 'сведения об оценщике',
                correct: true
            }, {
                id: 'fmc5pt51',
                text: 'правоустанавливающие документы',
                correct: true
            }, {
                id: '61b576ho',
                text: 'фотографии общего вида объекта',
                correct: true
            }, {
                id: '1embp25v',
                text: 'фотографии таблички с адресом объекта',
                correct: true
            }, {
                id: 'j2lq5lbt',
                text: 'всё перечисленное не является обязательным',
                correct: false
            }]
        },
        {
            type: 'checkbox',
            id: 're6vllwv',
            text: 'Укажите критичные перепланировки, квартиры с которыми банк не кредитует',
            description: '',
            answers: [{
                id: 'gx1z7otz',
                text: 'снос несущих стен',
                correct: true
            }, {
                id: '1i1v7cv7',
                text: 'снос подоконного блока',
                correct: false
            }, {
                id: 'gif9cb3a',
                text: 'перенос газового оборудования за пределы кухни',
                correct: true
            }, {
                id: 'qevglvfy',
                text: 'перенос радиаторов отопления на балконы или лоджии',
                correct: true
            }, {
                id: 'gnjhciqt',
                text: 'пристройка балконов или лоджий',
                correct: true
            }]
        }]
    }
};
