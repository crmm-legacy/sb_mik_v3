export default {
    id: '/wqgmt1l4',
    name: 'Анализ отчета об оценке. Тест',
    type: 'test',
    component: 'test_compo',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'checkbox',
            id: 'lh5bcn6a',
            text: 'Банк принимает отчеты, которые сделаны с применением',
            description: '',
            answers: [{
                id: 'znb1e9cv',
                text: 'доходного подхода',
                correct: false
            }, {
                id: '1irl2zhj',
                text: 'затратного подхода',
                correct: true
            }, {
                id: 'n3sz06hy',
                text: 'сравнительного подхода',
                correct: true
            }]
        },
        {
            type: 'checkbox',
            id: 'jodm7dta',
            text: 'Укажите требования к объектам-аналогам при применении сравнительного подхода для оценки недвижимости',
            description: '',
            answers: [{
                id: '587xqrpd',
                text: 'Аналоги имеют схожие характеристики с объектом оценки',
                correct: true
            }, {
                id: 'fpdve1ix',
                text: 'Аналогов не менее 4 для городов с населением более 500 тысяч человек',
                correct: true
            }, {
                id: '9ytstdad',
                text: 'Аналогов не менее 3 для городов с населением менее 500 тысяч человек',
                correct: false
            }, {
                id: 'cbf0hf6i',
                text: 'При оценке квартиры аналогом может быть как квартира, так и земельный участок',
                correct: false
            }]
        },
        {
            type: 'radio',
            id: 'yxtq8w19',
            text: 'Укажите пороговое значение коэффициентов Т и Т1',
            description: '',
            answers: [{
                id: '270kc4fy',
                text: '10',
                correct: false
            }, {
                id: '9x6ye9em',
                text: '15',
                correct: false
            }, {
                id: 'y7qba73e',
                text: '20',
                correct: true
            }, {
                id: '8kj9rv2p',
                text: '25',
                correct: false
            }]
        },
        {
            type: 'radio',
            id: '2emsmk8o',
            text: 'Если по итогам ручной верификации отчета об оценке коэффициент Т больше 20%',
            description: '',
            answers: [{
                id: 'txdwql6r',
                text: 'отправляем отчет на дополнительную проверку в уполномоченное подразделение',
                correct: true
            }, {
                id: 's354xa1x',
                text: 'рассчитываем коэффициент Т1 и сравниваем с пороговым значением',
                correct: false
            }]
        },
        {
            type: 'radio',
            id: '3ffvv144',
            text: 'Если по итогам ручной верификации отчета об оценке коэффициент Т1 меньше 20%',
            description: '',
            answers: [{
                id: 'yclg2tcm',
                text: 'принимаем отчет, никуда его не отправляем и одобряем объект в стандартном режиме',
                correct: true
            }, {
                id: 'byohl132',
                text: 'отправляем в уполномоченное подразделение для детальной проверки',
                correct: false
            }, {
                id: 'qqx5dcm9',
                text: 'отчет не принимается. Просим клиента заменить отчет или объект',
                correct: false
            }]
        }]
    }
};
