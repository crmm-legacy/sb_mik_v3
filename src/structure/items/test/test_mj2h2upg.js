export default {
    id: '/mj2h2upg',
    name: 'Доверенность от продавца и покупателя. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/22/1/2',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'checkbox',
            id: '434xq972',
            text: 'Какая информация обязательно указывается в нотариальной доверенности?',
            description: '',
            answers: [{
                id: 'g6pbki9f',
                text: 'Дата и место составления',
                correct: true
            }, {
                id: 'zbw3ghgt',
                text: 'Паспортные данные доверителя',
                correct: true
            }, {
                id: 'dzkrjcoi',
                text: 'Разрешенные действия',
                correct: true
            }, {
                id: '0nqq1a7r',
                text: 'Паспортные данные нотариуса',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'lq7bryn1',
            text: 'Нужно ли загружать скан-копию нотариальной доверенности в АС Transact?',
            description: '',
            answers: [{
                id: '8p950y3r',
                text: 'Да, нужно загрузить цветную скан-копию',
                correct: true
            }, {
                id: 'u0lo7kac',
                text: 'Да, нужно загрузить черно-белую скан-копию',
                correct: false
            }, {
                id: 'ssqkvbbb',
                text: 'Нет, загружать не нужно, достаточно ее проверить',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 'n4cdizne',
            text: 'В каком случае нотариальная доверенность является недействительной?',
            description: '',
            answers: [{
                id: 'wzeygw9d',
                text: 'Отсутствует дата оформления',
                correct: true
            }, {
                id: 'acgb87vh',
                text: 'До окончания срока действия доверенности остался 1 месяц',
                correct: false
            }, {
                id: 'tizouduk',
                text: 'Доверитель отозвал свою доверенность',
                correct: true
            }, {
                id: 'kq5ywu4x',
                text: 'В случае смерти доверителя',
                correct: true
            }]
        }, {
            type: 'radio',
            id: 'c933fjdq',
            text: 'Если заемщик является инвалидом по зрению, то текст документа должен быть зачитан:',
            description: '',
            answers: [{
                id: 'ry12qtbt',
                text: 'Работником Банка',
                correct: false
            }, {
                id: 'ob2q9pdx',
                text: 'Доверенным лицом, которое подписывает документ',
                correct: true
            }, {
                id: '5nhoyh2o',
                text: 'Риелтором заемщика',
                correct: false
            }]
        }]
    }
};
