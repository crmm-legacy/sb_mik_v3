export default {
    id: '/bdujs276',
    name: 'Страхование жизни и здоровья. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/5/0/1',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'radio',
            id: '4jha3vwm',
            text: 'Кого из участников сделки нужно застраховать, чтобы процентная ставка по кредиту была снижена?',
            description: '',
            answers: [{
                id: 'dwjpcpdq',
                text: 'Все созаемщики',
                correct: false
            }, {
                id: 'tmk3ph13',
                text: 'Любой из созаемщиков',
                correct: false
            }, {
                id: 'k661qiwz',
                text: 'Титульный созаемщик',
                correct: true
            }, {
                id: 'fsdf3i38',
                text: 'Титульный созаемщик и поручитель',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 'a20s5d30',
            text: 'От чего зависит стоимость полиса страхования?',
            description: '',
            answers: [{
                id: 'yjudn0bv',
                text: 'Пол',
                correct: true
            }, {
                id: 'mvkkood4',
                text: 'Состояние здоровья',
                correct: false
            }, {
                id: 'p5op2tcp',
                text: 'Возраст',
                correct: true
            }, {
                id: 'htb23w5i',
                text: 'Размер основного долга перед банком',
                correct: true
            }]
        }, {
            type: 'checkbox',
            id: 'a84j02fu',
            text: 'По каким причинам может повыситься процентная ставка?',
            description: '',
            answers: [{
                id: 'nc5z2m0w',
                text: 'Расторжение договора страхования',
                correct: true
            }, {
                id: 'h2sb0we2',
                text: 'Замена выгодоприобретателя',
                correct: true
            }, {
                id: '1y01h416',
                text: 'Невозобновление действия полиса страхования',
                correct: true
            }, {
                id: 'zyon5ygt',
                text: 'Несвоевременная оплата ежемесячного платежа по кредиту',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'gd3st4gb',
            text: 'Укажите верные утверждения',
            description: '',
            answers: [{
                id: '3oxh6wmp',
                text: 'При использовании особого условия «Защищенный кредит» процентная ставка снижается на 1%',
                correct: true
            }, {
                id: 'nf8jsbv1',
                text: 'Клиент, желающий оформить полис страхования жизни и здоровья не должен болеть онкологическими заболевания. При этом полис можно приобрести при таких заболеваниях как цирроз печени и стенокардия',
                correct: false
            }, {
                id: 'xgjr761y',
                text: 'Деньги, уплаченные в пользу страховой компании, при досрочном погашении можно вернуть в любом случае вне зависимости от условий договора',
                correct: false
            }]
        }]
    }
};
