export default {
    id: '/i2wq0gc9',
    name: 'Пилот. Отказ от отчета об оценке. Тест',
    type: 'test',
    component: 'test_compo',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'checkbox',
            id: 'lscaxpok',
            text: 'Укажите условия, при которых не нужно заказывать отчет об оценке в рамках пилотного проекта',
            description: '',
            answers: [{
                id: '12zjpc76',
                text: 'объявление о продаже опубликовано на ДомКлик',
                correct: true
            }, {
                id: '2azuifi5',
                text: 'объект оценки — квартира, продукт — «Готовое жилье»',
                correct: true
            }, {
                id: 'slpf5dab',
                text: 'объект оценки — апартаменты, продукт — «Строящееся жилье»',
                correct: false
            }, {
                id: 'q59250wg',
                text: 'объект расположен в многоквартирном доме',
                correct: true
            }, {
                id: 'y4fzdj4v',
                text: 'стены объекта построены с использованием деревянных материалов',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'cm10sljq',
            text: 'Для определения допустимой стоимости объекта в рамках пилота об отказе от отчета об оценке надо смотреть на',
            description: '',
            answers: [{
                id: 'c9oxhkt8',
                text: 'город нахождения объекта',
                correct: true
            }, {
                id: 'f0q24nvw',
                text: 'место подачи заявки',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'yqfjcvxs',
            text: 'Для отказа от отчета об оценке стоимость объекта для городов миллионников, кроме Москвы и Санкт-Петербурга, должна быть не более',
            description: '',
            answers: [{
                id: '4695bb9i',
                text: '20 млн',
                correct: false
            }, {
                id: 'iqcy783w',
                text: '10 млн',
                correct: true
            }, {
                id: 'oqanfwwl',
                text: '5 млн',
                correct: false
            }]
        },
        {
            type: 'radio',
            id: 'uhv1lkfr',
            text: 'Для отказа от отчета об оценке стоимость объекта для Московской области должна быть не более',
            description: '',
            answers: [{
                id: 'gukg58m4',
                text: '20 млн',
                correct: false
            }, {
                id: '191y935f',
                text: '10 млн',
                correct: false
            }, {
                id: 'act9arku',
                text: '5 млн',
                correct: true
            }]
        },
        {
            type: 'radio',
            id: 'p3nglzs5',
            text: 'Верно ли утверждение: «Сервис безопасных расчетов невозможно использовать в рамках пилота по отказу от отчета об оценке»',
            description: '',
            answers: [{
                id: 'zmxw3jr3',
                text: 'верно',
                correct: true
            }, {
                id: 'wdt9k0e4',
                text: 'неверно',
                correct: false
            }]
        },
        {
            type: 'checkbox',
            id: 'slbt3709',
            text: 'Верно ли утверждение: «Сервис безопасных расчетов невозможно использовать в рамках пилота по отказу от отчета об оценке»',
            description: '',
            answers: [{
                id: 'f0niethx',
                text: 'фотографии всех помещений, кроме балкона',
                correct: true
            }, {
                id: 'w18ke6pg',
                text: 'фотографии всех помещений, кроме санузла',
                correct: false
            },
            {
                id: 'fbs2k1jf',
                text: 'фотографии фасада дома и его окружения',
                correct: false
            },
            {
                id: '7snhom9a',
                text: 'фотографии таблички с адресом объекта',
                correct: true
            }]
        }]
    }
};
