export default {
    id: '/jpnr5zve',
    name: 'ПИФ. Тест',
    type: 'test',
    component: 'test_compo',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'radio',
            id: 'iwei1wjn',
            text: 'Взносы в ПИФ могут делать',
            description: '',
            answers: [{
                id: 'mvh18iln',
                text: 'Физические лица',
                correct: false
            }, {
                id: 'l3xv6g5h',
                text: 'Юридические лица',
                correct: false
            }, {
                id: '3j6i3cxu',
                text: 'И те и другие',
                correct: true
            }]
        }, {
            type: 'radio',
            id: 'cdmer77t',
            text: 'Кто управляет ПИФом',
            description: '',
            answers: [{
                id: '4dz5ytpg',
                text: 'Управляющая компания',
                correct: true
            }, {
                id: '7pgx88wz',
                text: 'Специализированный депозитарий',
                correct: false
            }, {
                id: 'rcnjhqz3',
                text: 'Пайщики',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 'w696hkjf',
            text: 'Какие нужны документы для покупки недвижимости, если инвестор – ПИФ',
            description: '',
            answers: [{
                id: 'i3nqxyof',
                text: 'Согласие специализированного депозитария на сделку',
                correct: true
            }, {
                id: 'wy35l2fw',
                text: 'Документы по управляющей компании паевого фонда',
                correct: true
            }, {
                id: '9o1c44ot',
                text: 'Копия выписки из реестра ПИФ, выданная Банком России',
                correct: true
            }, {
                id: '6be0smnl',
                text: 'Зарегистрированные правила доверительного управления ПИФ',
                correct: true
            }]
        }, {
            type: 'radio',
            id: 's6998wd4',
            text: 'Какие сервисы применяются в сделках с ПИФ',
            description: '',
            answers: [{
                id: 'l9py2ie8',
                text: 'Сервис электронной регистрации',
                correct: false
            }, {
                id: 'b7dyvpwy',
                text: 'Сервис безопасных расчетов',
                correct: false
            }, {
                id: 'no7hogte',
                text: 'Ни тот ни другой',
                correct: true
            }]
        }]
    }
};
