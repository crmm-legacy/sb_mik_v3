export default {
    id: '/4dh7yqoc',
    name: 'Покупка доли и комнаты. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/23/0/2',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'checkbox',
            id: 'hqxh8ouj',
            text: 'Приобрести комнату можно в рамках продукта:',
            description: '',
            answers: [{
                id: 'h9wol5n2',
                text: '«Приобретение строящегося жилья»',
                correct: false
            }, {
                id: 'k4mrj29k',
                text: '«Приобретение готового жилья»',
                correct: true
            }, {
                id: 'bvz6j7cv',
                text: '«Военная ипотека - Приобретение готового жилья»',
                correct: true
            }, {
                id: 'uvyfdax6',
                text: '«Загородная недвижимость»',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'oqykd0q8',
            text: 'Приобрести долю можно в рамках продукта:',
            description: '',
            answers: [{
                id: 'fsvagis5',
                text: '«Приобретение строящегося жилья»',
                correct: false
            }, {
                id: 'yg0st1ax',
                text: '«Приобретение готового жилья»',
                correct: true
            }, {
                id: 'bgb18ew8',
                text: '«Военная ипотека - Приобретение готового жилья»',
                correct: false
            }, {
                id: 't7shv75c',
                text: '«Загородная недвижимость»',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: '0w6hhd0f',
            text: 'При приобретении комнаты или доли возникает преимущественное право покупки. Какие дополнительные документы при этом могут потребоваться?',
            description: '',
            answers: [{
                id: 't0tqle5h',
                text: 'Разрешение органов опеки и попечительства',
                correct: false
            }, {
                id: 'p34v6ku4',
                text: 'Отказ от преимущественного права покупки',
                correct: true
            }, {
                id: 'uxly7bt7',
                text: 'Свидетельство нотариуса о передаче заявления продавца о намерении продать комнату или долю',
                correct: true
            }, {
                id: 'hgqtlj57',
                text: 'Нотариальное согласие супруги продавца на продажу объекта недвижимости',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'y8m0aaua',
            text: 'Страхование имущества при передаче в залог комнаты оформляется на:',
            description: '',
            answers: [{
                id: 'jd4xfqyw',
                text: 'Квартиру, в которой находится комната',
                correct: false
            }, {
                id: '7do2lno7',
                text: 'Комнату',
                correct: true
            }, {
                id: 'zqjmn6gl',
                text: 'Страхование не требуется',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'q9uw7bm0',
            text: 'Верно ли утверждение: «По кредитам на приобретение доли в качестве основного обеспечения достаточно оформить залог кредитуемого объекта недвижимости – приобретаемой доли»?',
            description: '',
            answers: [{
                id: 'fr2lo0eb',
                text: 'Верно',
                correct: false
            }, {
                id: '4x8o0rse',
                text: 'Неверно, также требуется оформление ипотеки остальных долей',
                correct: true
            }]
        }]
    }
};
