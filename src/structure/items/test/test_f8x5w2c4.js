export default {
    id: '/f8x5w2c4',
    name: 'Требования к участникам сделки. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/0/5/1',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'checkbox',
            id: 'm4y1hrab',
            text: 'Что влияет на размер процентной ставки?',
            description: '',
            answers: [{
                id: 'esnvgqnz',
                text: 'Категория заемщика',
                correct: true
            }, {
                id: 'oaai5mdu',
                text: 'Тип продукта',
                correct: true
            }, {
                id: 's8fnnudx',
                text: 'Специальные условия (например, «Молодая семья», «Защищенный кредит»)',
                correct: true
            }, {
                id: 'qfrf6ysg',
                text: 'Размер первоначального взноса',
                correct: true
            }, {
                id: 'akghnpro',
                text: 'Стоимость кредитуемого объекта недвижимости',
                correct: false
            }, {
                id: 'g7n7miq6',
                text: 'Все перечисленное верно',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 'thmmw4pw',
            text: 'Какие требования предъявляют к заемщику?',
            description: '',
            answers: [{
                id: '1wknrv9c',
                text: 'Гражданин РФ',
                correct: true
            }, {
                id: 'cegj6h6i',
                text: 'Постоянная регистрация на территории РФ',
                correct: false
            }, {
                id: 'w2zclak3',
                text: 'Постоянная или временная регистрация на территории РФ',
                correct: true
            }, {
                id: 'go0xuuiz',
                text: 'Возраст заемщика 21–75 лет (для ипотеки по 2 документам до 65 лет)',
                correct: true
            }, {
                id: 'k1xiv5h4',
                text: 'Возраст заемщика 21–70 лет (для ипотеки по 2 документам до 60 лет)',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 't0vj7esv',
            text: 'Какие требования к гражданству установлены для заемщика по ипотечному кредиту?',
            description: '',
            answers: [{
                id: 'em1w448b',
                text: 'Гражданин РФ, который имеет постоянную/временную регистрацию по месту жительства/пребывания на территории РФ',
                correct: true
            }, {
                id: 'nobdfvi7',
                text: 'Гражданин РФ независимо от регистрации',
                correct: false
            }, {
                id: 'k32ovna7',
                text: 'Любое дееспособное лицо, которое имеет постоянную/временную регистрацию по месту жительства/пребывания на территории РФ',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: '4hj41scl',
            text: 'Укажите требования, предъявляемые к трудовому стажу:',
            description: '',
            answers: [{
                id: '7mefmb89',
                text: 'Не менее 1 года за последние 5 лет',
                correct: true
            }, {
                id: 'wc6fvkrd',
                text: 'Не менее 6 месяцев за последний год',
                correct: false
            }, {
                id: 'gu4ly6sw',
                text: 'Не менее 6 месяцев за последние 5 лет',
                correct: false
            }, {
                id: 'zsvtvtgt',
                text: 'Срок на текущем рабочем месте не менее 6 месяцев',
                correct: true
            }, {
                id: '2zizv27m',
                text: 'Срок на текущем рабочем месте не менее 1 года',
                correct: false
            }]
        }, {
            type: 'radio',
            id: '3jbvbppy',
            text: 'Если титульный заемщик относится к категории 4 (физическое лицо с постоянным доходом), а созаемщик — к категории 2 (зарплатный Клиент), то условия кредитования определяются:',
            description: '',
            answers: [{
                id: 'x01gt7de',
                text: 'Как зарплатному Клиенту (по 2 категории)',
                correct: true
            }, {
                id: 'byd6lgs6',
                text: 'Как физическому лицу с постоянным доходом (по 4 категории)',
                correct: false
            }, {
                id: 'cc54mgvr',
                text: 'Ипотека по 2 документам',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'u9leuvd6',
            text: 'Если титульный заемщик относится к категории 4 (физическое лицо с постоянным доходом), а созаемщик вне категорий (физическое лицо, не предоставившее документы, подтверждающие финансовое состояние и трудовую занятость), то условия кредитования определяются:',
            description: '',
            answers: [{
                id: 'h5frpws1',
                text: 'По созаемщику вне категорий',
                correct: true
            }, {
                id: 'pq8n3l4s',
                text: 'По созаемщику 4 категории',
                correct: false
            }, {
                id: 'q31mbuwi',
                text: 'Созаемщика вне категорий необходимо исключить из заявки',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'rticopx6',
            text: 'Физические лица, не предоставившие документы, подтверждающие финансовое состояние и трудовую занятость не могут взять кредит:',
            description: '',
            answers: [{
                id: 'z4nuizr9',
                text: 'По месту постоянной регистрации',
                correct: false
            }, {
                id: 'uyggkavb',
                text: 'По месту временной регистрации',
                correct: true
            }, {
                id: 'uwn7xbdo',
                text: 'По месту нахождения или строительства объекта недвижимости',
                correct: false
            }]
        }]
    }
};
