export default {
    id: '/lb0tqfvm',
    name: 'Одобрение объекта недвижимости. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/22/3/2',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'radio',
            id: 'mrduzxlm',
            text: 'Сколько времени заявка будет доступна в Transact после принятия положительного решения о предоставлении кредита и одобрения объекта недвижимости?',
            description: '',
            answers: [{
                id: 'winecgk4',
                text: '30 рабочих дней',
                correct: false
            }, {
                id: '30rtl0ug',
                text: '30 календарных дней',
                correct: true
            }, {
                id: 'qmpsw8bg',
                text: '90 дней до конца срока действия отлагательного условия',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'q3ff2eid',
            text: 'Сколько раз в Transact можно скорректировать параметры по Клиенту и объекту в заявке?',
            description: '',
            answers: [{
                id: '2w9g30aa',
                text: 'только один раз',
                correct: false
            }, {
                id: 'ndh0gwau',
                text: 'не более трёх раз',
                correct: false
            }, {
                id: 'nwlfdnln',
                text: 'не более пяти раз',
                correct: true
            }, {
                id: 'vuibgc1h',
                text: 'неограниченное количество',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'jv3187bl',
            text: 'Сколько корректировок можно сделать по оценочному альбому в Transact?',
            description: '',
            answers: [{
                id: 'np70566c',
                text: 'только один раз',
                correct: false
            }, {
                id: 't9wqddzt',
                text: 'не более трёх раз',
                correct: true
            }, {
                id: 'rzz9bdq0',
                text: 'не более пяти раз',
                correct: false
            }, {
                id: 'pbemdknt',
                text: 'неограниченное количество',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'pe1s7pru',
            text: 'Что делать МИК при статусе «Ожидание оповещения об одобрении» с комментарием «Возврат документов»?',
            description: '',
            answers: [{
                id: '4w38kgtv',
                text: 'назначить сделку, так как заявка в статусе «Ожидание оповещения об одобрении»',
                correct: false
            }, {
                id: 'ax8frz99',
                text: 'назначить сделку, если дополнительные документы будут предоставлены на сделке',
                correct: false
            }, {
                id: 'shqdmco2',
                text: 'не назначать сделку, проанализировать замечания и запросить дополнительные документы',
                correct: true
            }]
        }]
    }
};
