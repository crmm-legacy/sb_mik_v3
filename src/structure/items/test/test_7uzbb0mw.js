export default {
    id: '/7uzbb0mw',
    name: 'Подтверждение трудовой занятости. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/1/3/1',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'radio',
            id: 'xt40ezhz',
            text: 'Как можно подтвердить трудовую занятость?',
            description: '',
            answers: [{
                id: 'tnga3giq',
                text: 'Копия из трудовой книжки',
                correct: false
            }, {
                id: 'xb8t6ayn',
                text: 'Выписка из трудовой книжки',
                correct: false
            }, {
                id: 'sw4ynojy',
                text: 'Справка от работодателя',
                correct: false
            }, {
                id: 'y8o7b8dm',
                text: 'Все перечисленное верно',
                correct: true
            }]
        }, {
            type: 'checkbox',
            id: 'rtz8w71m',
            text: 'Укажите верные утверждения о трудовой книжке:',
            description: '',
            answers: [{
                id: 'epn949rj',
                text: 'Трудовая книжка должна быть предоставлена в оригинале',
                correct: false
            }, {
                id: 'as5e7ec3',
                text: 'Копия трудовой книжки должна быть постранично заверена',
                correct: true
            }, {
                id: 'ohg8em76',
                text: 'Копия трудовой книжки действует 20 дней с даты выдачи',
                correct: false
            }, {
                id: 'sclmj8ys',
                text: 'Копия может быть прошита и заверена на последней странице с указанием количества листов',
                correct: true
            }]
        }, {
            type: 'checkbox',
            id: 'inv6ruz6',
            text: 'Укажите верные утверждения о выписке из трудовой книжки:',
            description: '',
            answers: [{
                id: 'we0ktuvj',
                text: 'Выписка должна быть заверена',
                correct: true
            }, {
                id: '06081yn0',
                text: 'Содержит сведения обо всех местах работы с даты начала трудовой деятельности',
                correct: false
            }, {
                id: '234ntpqx',
                text: 'Содержит сведения обо всех местах работы за последние 5 лет',
                correct: true
            }, {
                id: '8uduj1xv',
                text: 'Выписка действует 30 дней с даты выдачи',
                correct: true
            }]
        }, {
            type: 'radio',
            id: 'uxl7iwic',
            text: 'Каким документом подтверждает свою трудовую занятость ИП?',
            description: '',
            answers: [{
                id: '738w8dgk',
                text: 'Лицензия',
                correct: false
            }, {
                id: 'gunxgcvz',
                text: 'Удостоверение',
                correct: false
            }, {
                id: '4zqcjjr8',
                text: 'Свидетельство',
                correct: true
            }, {
                id: 'gra3tf1p',
                text: 'Приказ',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'k3doxbi3',
            text: 'Заверенная копия трудового договора предоставляется при подтверждении:',
            description: '',
            answers: [{
                id: '0e40zv4d',
                text: 'Основного места работы',
                correct: false
            }, {
                id: 'ixb92qvj',
                text: 'Место работы по совместительству',
                correct: true
            }, {
                id: 'qoi958zw',
                text: 'Дохода от сдачи в аренду жилья',
                correct: false
            }]
        }]
    }
};
