export default {
    id: '/gjuvrr6c',
    name: 'Материнский капитал. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/3/1/1',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'checkbox',
            id: '0csn0bsa',
            text: 'Использовать материнский капитал можно при оформлении ипотечного кредита по продуктам:',
            description: '',
            answers: [{
                id: 'ag3wf0tw',
                text: '«Готовое жилье»',
                correct: true
            }, {
                id: '9pa5y9by',
                text: '«Строящееся жилье»',
                correct: true
            }, {
                id: 'psnckkka',
                text: '«Загородная недвижимость»',
                correct: false
            }, {
                id: 'vrxam9ml',
                text: '«Нецелевой кредит под залог объекта недвижимости»',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'hyv7izci',
            text: 'Минимальный ПВ составляет:',
            description: '',
            answers: [{
                id: 'jbxbobpz',
                text: '10 %',
                correct: false
            }, {
                id: 'ln41wsya',
                text: '15 %',
                correct: true
            }, {
                id: 'mahy4a6e',
                text: '20 %',
                correct: false
            }, {
                id: '33b8apcx',
                text: 'Необязателен',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 'j06p9cmc',
            text: 'Материнский капитал можно использовать как:',
            description: '',
            answers: [{
                id: 'nd28dfzh',
                text: 'Частично досрочное погашение действующего ипотечного кредита',
                correct: true
            }, {
                id: 'krs2itpk',
                text: 'Первоначальный взнос с увеличением суммы кредита',
                correct: true
            }, {
                id: 'azj2a5ax',
                text: 'Первоначальный взнос путем перечисления денежных средств продавцу',
                correct: true
            }, {
                id: '9w5mwvbz',
                text: 'Средства на ремонт приобретаемой недвижимости',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 'emvdkyi3',
            text: 'Укажите верные утверждения:',
            description: '',
            answers: [{
                id: 'ewaarih6',
                text: 'ПФР сразу переводит средства материнского капитала в Банк, после отправки Клиентом письменного запроса о переводе',
                correct: false
            }, {
                id: 'ljs8i8gi',
                text: 'Клиент должен предоставить в Банк государственный сертификат на материнский капитал',
                correct: true
            }, {
                id: '4po02l9t',
                text: 'Клиент должен предоставить в Банк справку об остатке средств материнского капитала',
                correct: true
            }, {
                id: 'yptrgxbk',
                text: 'Дети обязательно должны быть наделены собственностью сразу (в момент регистрации сделки)',
                correct: false
            }]
        }]
    }
};
