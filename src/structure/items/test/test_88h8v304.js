export default {
    id: '/88h8v304',
    name: 'Сервисная модель. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/0/1/1',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'radio',
            id: 'g4l2urc0',
            text: 'К какому принципу относится выражение: «Назначай встречи, только если это необходимо самому Клиенту. Не звони, если это не нужно»?',
            description: '',
            answers: [{
                id: 'b3tz0szo',
                text: '«Откажись от лишнего»',
                correct: true
            }, {
                id: 'ptkzppu5',
                text: '«Делай то, что обещал. Если не смог — признавай ошибки и сразу исправляй»',
                correct: false
            }, {
                id: 'wsnboto4',
                text: '«Помни, что сказал Клиент, фиксируй все договоренности»',
                correct: false
            }, {
                id: 'vcdzhgq1',
                text: '«Все, что нужно сделать заранее, должно быть сделано заранее»',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'r3nqn76k',
            text: 'К какому принципу относится выражение: «К приходу Клиента все должно быть готово»?',
            description: '',
            answers: [{
                id: 'z15or51q',
                text: '«Все, что может быть сделано за Клиента, должно быть сделано за Клиента»',
                correct: false
            }, {
                id: 'vex1z2ex',
                text: '«Все, что нужно сделать заранее, должно быть сделано заранее»',
                correct: true
            }, {
                id: 'r64amj0q',
                text: '«Информируй Клиента о каждом шаге»',
                correct: false
            }, {
                id: 'rj6p2jwr',
                text: '«Делай то, что обещал. Если не смог — признавай ошибки и сразу исправляй»',
                correct: false
            }]
        }]
    }
};
