export default {
    id: '/1dxoqkpq',
    name: 'ГЖС. Тест',
    type: 'test',
    component: 'test_compo',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'radio',
            id: '2957q5dw',
            text: 'В каком регионе можно купить объект по государственному жилищному сертификату',
            description: '',
            answers: [{
                id: '9mcgxgzf',
                text: 'по месту регистрации владельца сертификата',
                correct: false
            }, {
                id: 'rj47acad',
                text: 'по месту работы владельца сертификата',
                correct: false
            }, {
                id: 'hs0bggsb',
                text: 'по месту нахождения выбранного объекта',
                correct: false
            }, {
                id: 'pyiqx56m',
                text: 'по месту, указанному в сертификате',
                correct: true
            }]
        }, {
            type: 'checkbox',
            id: 'y4tx045t',
            text: 'Выберите верные утверждения о жилищных сертификатах',
            description: '',
            answers: [{
                id: '6er1nxbm',
                text: 'недвижимость должна быть оформлена на всех членов семьи, указанных в сертификате',
                correct: true
            }, {
                id: 'rwcyn5nm',
                text: 'сумма сертификата перечисляется продавцу до регистрации сделки в Росреестре',
                correct: false
            }, {
                id: 's9m6bgbg',
                text: 'сертификат нельзя использовать как первоначальный взнос',
                correct: false
            }, {
                id: 'gxd09n5b',
                text: 'срок предоставления сертификата в банк ограничен',
                correct: true
            }]
        },
        {
            type: 'checkbox',
            id: 'fw43ybrx',
            text: 'Выберите верные утверждения о жилищных сертификатах',
            description: '',
            answers: [{
                id: '5tcmpz86',
                text: 'после открытия ИБЦ - счета сертификат возвращается клиенту',
                correct: false
            }, {
                id: '0r4ceqwr',
                text: 'при покупке 2 и более объектов документы по всем объектам предоставляются в банк одновременно',
                correct: true
            }, {
                id: 's3tr9tkd',
                text: 'в договоре-основании надо указать реквизиты сертификата',
                correct: true
            }]
        }]
    }
};
