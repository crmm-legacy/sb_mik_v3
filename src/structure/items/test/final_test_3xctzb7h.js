export default {
    id: '/3xctzb7h',
    name: 'Финальное тестирование',
    type: 'test',
    component: 'test_compo',
    content: {
        name: 'Итоговое тестирование по курсу',
        type: 'test'
    },
    shuffle: true,
    config: {
        isFinalTest: true,
        basedOn: [
            // module_0
            {
                id: '/qgjkzxwz',
                numOfQuestions: 1
            },
            {
                id: '/88h8v304',
                numOfQuestions: 1
            },
            {
                id: '/9miu85fw',
                numOfQuestions: 1
            },
            {
                id: '/gvop93j1',
                numOfQuestions: 1
            },
            {
                id: '/ngxld3rc',
                numOfQuestions: 1
            },
            {
                id: '/f8x5w2c4',
                numOfQuestions: 1
            },
            {
                id: '/x53c1hfb',
                numOfQuestions: 1
            },
            // module_1
            {
                id: '/5dl67yc1',
                numOfQuestions: 1
            },
            {
                id: '/jhfv81xp',
                numOfQuestions: 1
            },
            {
                id: '/ruivwq3j',
                numOfQuestions: 1
            },
            {
                id: '/7uzbb0mw',
                numOfQuestions: 1
            },
            {
                id: '/9augcfc7',
                numOfQuestions: 1
            },
            // module_2
            {
                id: '/0o40quhw',
                numOfQuestions: 1
            },
            {
                id: '/u7o16god',
                numOfQuestions: 1
            },
            // module_3
            {
                id: '/pjwl1hlr',
                numOfQuestions: 1
            },
            {
                id: '/gjuvrr6c',
                numOfQuestions: 1
            },
            {
                id: '/pf1jmr3x',
                numOfQuestions: 1
            },
            // module_4
            {
                id: '/ugu1vzlp',
                numOfQuestions: 1
            },
            // module_5
            {
                id: '/bdujs276',
                numOfQuestions: 1
            },
            {
                id: '/jtkl5wsa',
                numOfQuestions: 1
            },
            {
                id: '/829y1dd9',
                numOfQuestions: 1
            },
            {
                id: '/3jab0fsf',
                numOfQuestions: 1
            },
            {
                id: '/0hyhwnu1',
                numOfQuestions: 1
            },
            // module_6
            {
                id: '/hvciy8co',
                numOfQuestions: 1
            },
            {
                id: '/nt29t2nk',
                numOfQuestions: 1
            },
            // module_7
            {
                id: '/ixxp3uaa',
                numOfQuestions: 1
            },
            {
                id: '/r3r7t5oy',
                numOfQuestions: 1
            },
            {
                id: '/ibqmnkhk',
                numOfQuestions: 1
            }
        ]
    }
};
