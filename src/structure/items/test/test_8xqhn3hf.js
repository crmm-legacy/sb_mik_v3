export default {
    id: '/8xqhn3hf',
    name: 'Прямая и альтернативная сделки. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/24/0/2',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'radio',
            id: '6dd5mdky',
            text: 'Какой способ расчета применяется при альтернативной сделке?',
            description: '',
            answers: [{
                id: '5l2mw6jl',
                text: 'Сервис безопасных расчетов',
                correct: false
            }, {
                id: 'bzssn507',
                text: 'Сейфовая ячейка',
                correct: true
            }, {
                id: 'l3ed4izj',
                text: 'Безналичный перевод',
                correct: false
            }]
        }, {
            type: 'radio',
            id: '003y2z0m',
            text: 'Прямая продажа — это',
            description: '',
            answers: [{
                id: '5rs564yl',
                text: 'Когда продавец просто получает за недвижимость деньги и не покупает ничего взамен',
                correct: true
            }, {
                id: 'qe44fchd',
                text: 'Когда собственник продает свою недвижимость и покупает другую на деньги от продажи',
                correct: false
            }, {
                id: 'y3wwi699',
                text: 'Сделка, в которой продается не один, а два или несколько объектов',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 'kxgshki7',
            text: 'Укажите утверждения, верные для альтернативной сделки',
            description: '',
            answers: [{
                id: 'blapzkn4',
                text: 'Сделка по всем объектам недвижимости проходит в один день',
                correct: true
            }, {
                id: '5rawiaro',
                text: 'Подписание и передача документов на регистрацию ДКП происходит единовременно',
                correct: true
            }, {
                id: '2x28f4wn',
                text: 'Сделки проходят последовательно, в разные дни одна за другой',
                correct: false
            }]
        }, {
            type: 'radio',
            id: '0jcsb6in',
            text: 'Верно ли утверждение: «Продавать свою и одновременно покупать новую недвижимость в рамках альтернативной сделки может только сам собственник»?',
            description: '',
            answers: [{
                id: 'pttbznyx',
                text: 'верно только для ипотечных сделок',
                correct: true
            }, {
                id: '3e8d55lp',
                text: 'утверждение неверно',
                correct: false
            }, {
                id: 'mi2pp5it',
                text: 'верно для всех видов сделок',
                correct: false
            }]
        }]
    }
};
