export default {
    id: '/pf1jmr3x',
    name: 'ЖК по 2 документам. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/3/2/1',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'checkbox',
            id: '1niv1z34',
            text: 'Ипотеку по двум документам можно получить в рамках продуктов:',
            description: '',
            answers: [{
                id: 'p4b7rtu9',
                text: '«Приобретение готового жилья»',
                correct: true
            }, {
                id: 'cfijj32v',
                text: '«Приобретение строящегося жилья»',
                correct: true
            }, {
                id: '0stnwk5b',
                text: '«Строительство жилого дома»',
                correct: false
            }, {
                id: 'w79cv2wb',
                text: '«Загородная недвижимость»',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: '7jw0w8c9',
            text: 'Выберите второй документ, который подходит для подачи заявки на ипотеку по двум документам:',
            description: '',
            answers: [{
                id: 'b0t080jo',
                text: 'Водительское удостоверение',
                correct: true
            }, {
                id: '0rea4vk1',
                text: 'Военный билет',
                correct: true
            }, {
                id: 'k0dees0t',
                text: 'Загранпаспорт',
                correct: true
            }, {
                id: 'spd5g8ua',
                text: 'ИНН',
                correct: false
            }, {
                id: 'qokym0k0',
                text: 'Свидетельство о рождении',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'j17tkp38',
            text: 'Максимальный размер кредита по ипотеке по двум документам:',
            description: '',
            answers: [{
                id: 'j3etyih2',
                text: '15 000 000 рублей для объектов Москвы и Санкт-Петербурга, для остальных городов — 8 000 000 рублей',
                correct: true
            }, {
                id: 'lgeck2vo',
                text: '15 000 000 рублей для объектов Москвы, Московской области и Санкт-Петербурга, для остальных городов — 8 000 000 рублей',
                correct: false
            }, {
                id: '92xnjsxt',
                text: '8 000 000 рублей для объектов Москвы и Санкт-Петербурга, для остальных городов — 15 000 000 рублей',
                correct: false
            }, {
                id: 'dofhjw30',
                text: '8 000 000 рублей для объектов Москвы, Московской области и Санкт-Петербурга, для остальных городов — 15 000 000 рублей',
                correct: false
            }]
        },
        // {
        //     type: 'radio',
        //     id: 'zu96z379',
        //     text: 'Минимальный размер первоначального взноса по ипотеке по двум документам:',
        //     description: '',
        //     answers: [{
        //         id: 'hazx5ztl',
        //         text: 'От 40 %',
        //         correct: false
        //     }, {
        //         id: 'yafmysfx',
        //         text: 'От 30 %',
        //         correct: false
        //     }, {
        //         id: '0mg0p32b',
        //         text: 'От 50%',
        //         correct: true
        //     }, {
        //         id: '0qv129fx',
        //         text: 'От 15%',
        //         correct: false
        //     }]
        // },
        {
            type: 'radio',
            id: '36dukh85',
            text: 'Требование к возрасту заемщика при подаче заявки на ипотеку по двум документам:',
            description: '',
            answers: [{
                id: '7p4edq77',
                text: 'От 21 года при условии, что срок возврата кредита по договору наступает до исполнения заемщику 65 лет',
                correct: true
            }, {
                id: 'rkj8ve5w',
                text: 'От 21 года при условии, что срок возврата кредита по договору наступает до исполнения заемщику 75 лет',
                correct: false
            }, {
                id: '5shqfpt5',
                text: 'От 20 года при условии, что срок возврата кредита по договору наступает до исполнения заемщику 65 лет',
                correct: false
            }, {
                id: 'y0yko6bl',
                text: 'От 20 года при условии, что срок возврата кредита по договору наступает до исполнения заемщику 75 лет',
                correct: false
            }]
        }]
    }
};
