export default {
    id: '/qgjkzxwz',
    name: 'О курсе и ипотечном кредитовании. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/0/0/1',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'radio',
            id: 'x8wh2eqh',
            text: 'Каковы допустимые сроки кредитования?',
            description: '',
            answers: [{
                id: 'n0lsbzye',
                text: 'От 12 месяцев до 30 лет',
                correct: true
            }, {
                id: '5xcgkb9p',
                text: 'От 12 месяцев до 35 лет',
                correct: false
            }, {
                id: 'ru9fdoyt',
                text: 'От 6 месяцев до 30 лет',
                correct: false
            }, {
                id: 't25icpt8',
                text: 'От 6 месяцев до 35 лет',
                correct: false
            }]
        }, {
            type: 'radio',
            id: '0ee27qnd',
            text: 'Какова минимальная сумма ипотечного кредита?',
            description: '',
            answers: [{
                id: 'v6llztrm',
                text: 'От 300 000 рублей',
                correct: true
            }, {
                id: '91c64nfh',
                text: 'От 500 000 рублей',
                correct: false
            }, {
                id: '59lqhz6d',
                text: 'От 1 млн рублей',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 'zuniwssb',
            text: 'Каковы свойства максимальной суммы кредита?',
            description: '',
            answers: [{
                id: '3r6mcor1',
                text: 'Рассчитывается в зависимости от платежеспособности заемщика/созаемщиков',
                correct: true
            }, {
                id: '42u08auu',
                text: 'Рассчитывается в зависимости от предоставленного обеспечения',
                correct: true
            }, {
                id: 'dy85le4v',
                text: 'Не зависит от выбранного ипотечного продукта',
                correct: false
            }, {
                id: '9ug4wjgx',
                text: 'Не может превышать стоимость кредитуемого объекта недвижимости',
                correct: true
            }]
        }]
    }
};
