export default {
    id: '/fikg7w7q',
    name: 'Межрегиональная сделка. Тест',
    type: 'test',
    component: 'test_compo',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'radio',
            id: 'ekl13a0q',
            text: 'Где может находиться продавец по время межрегиональный сделки',
            description: '',
            answers: [{
                id: 'taetdyoq',
                text: 'По месту нахождения объекта недвижимости',
                correct: false
            }, {
                id: 'acf0g6zr',
                text: 'По адресу регистрации покупателя',
                correct: false
            }, {
                id: 'xni6ikck',
                text: 'В любом центре ипотечного кредитования',
                correct: true
            }]
        }, {
            type: 'checkbox',
            id: 'zp8qdpz8',
            text: 'Где может находиться покупатель (участник зарплатного проекта Сбербанка) по время межрегиональный сделки',
            description: '',
            answers: [{
                id: 'zokd9ti5',
                text: 'По месту нахождения объекта недвижимости',
                correct: true
            }, {
                id: 'lsf4scc1',
                text: 'По месту своей регистрации',
                correct: true
            }, {
                id: 'x0e14gjc',
                text: 'По месту нахождения работодателя',
                correct: true
            },
            {
                id: 'c9o4b36k',
                text: 'В любом центре ипотечного кредитования',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 'srisfzfa',
            text: 'Выберите верные утверждения о межрегиональных сделках',
            description: '',
            answers: [{
                id: 'abwnj6wv',
                text: 'можно использовать сервис электронной регистрации',
                correct: true
            }, {
                id: 'iawkznk7',
                text: 'нельзя использовать сервис безопасных расчетов',
                correct: false
            }, {
                id: 'cw25cidh',
                text: 'расчет через ячейку возможен, если продавец и покупатель встретятся в одном месте',
                correct: true
            },
            {
                id: 'coq4tvxr',
                text: 'расчет через ячейку возможен, если продавец и покупатель встретятся в одном месте',
                correct: false
            }]
        }]
    }
};
