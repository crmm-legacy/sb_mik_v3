export default {
    id: '/nt29t2nk',
    name: 'Сейфовая ячейка. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/6/1/1',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'radio',
            id: 'o1flk4tt',
            text: 'При проведении расчетов по сделке индивидуальный сейф арендует:',
            description: '',
            answers: [{
                id: '7iuhnbnv',
                text: 'Покупатель',
                correct: true
            }, {
                id: 'c72jqgk2',
                text: 'Продавец',
                correct: false
            }, {
                id: '0r0kk8xu',
                text: 'Риелтор покупателя',
                correct: false
            }, {
                id: 'kb6lem40',
                text: 'Риелтор продавца',
                correct: false
            }]
        }, {
            type: 'radio',
            id: '5deljdy6',
            text: 'Доступ к сейфовой ячейке предоставляется:',
            description: '',
            answers: [{
                id: '44wgrb5n',
                text: 'Сотруднику Банка',
                correct: false
            }, {
                id: 'b6zeiu0v',
                text: 'Клиентам, указанным в договоре аренды сейфовой ячейки',
                correct: true
            }, {
                id: 'w69dsul8',
                text: 'Риелтору продавца',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'mrum714b',
            text: 'В договоре аренды ячейки всегда прописываются:',
            description: '',
            answers: [{
                id: 'w6dk9d2m',
                text: 'Условия доступа',
                correct: true
            }, {
                id: 'nip54c0x',
                text: 'Сумма, которая будет заложена',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'en6bb2ym',
            text: 'Могут ли клиенты предоставить право допуска к сейфу другим лицам в соответствии с полномочиями, указанными в доверенности?',
            description: '',
            answers: [{
                id: '16dqedwh',
                text: 'Да',
                correct: true
            }, {
                id: 'sx2t5i7u',
                text: 'Нет',
                correct: false
            }]
        }]
    }
};
