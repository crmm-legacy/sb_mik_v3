export default {
    id: '/jhfv81xp',
    name: 'Заполнение анкеты-заявления. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/1/1/1',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'checkbox',
            id: 'n3nth6tf',
            text: 'Раздел «Сведения об основной работе» Клиент не заполняет, если он:',
            description: '',
            answers: [{
                id: 'jyad00vl',
                text: 'Заемщик, который подает заявку на продукт «Военная ипотека»',
                correct: true
            }, {
                id: '8daewv2h',
                text: 'Созаемщик без учета платежеспособности',
                correct: true
            }, {
                id: 'ftbf4xtb',
                text: 'Поручитель без учета платежеспособности',
                correct: true
            }, {
                id: 'df4ge1c5',
                text: 'Созаемщик с учетом платежеспособности',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: '5r38ytvt',
            text: 'Если Клиент является зарплатным клиентом Сбербанка, в разделе «Дополнительная информация» он указывает:',
            description: '',
            answers: [{
                id: 'luvkgpyk',
                text: 'Номер банковской карты, на которую поступает зарплата',
                correct: true
            }, {
                id: '9jfs1r1b',
                text: 'Номер счета, на который поступает зарплата',
                correct: true
            }, {
                id: '9ylu24e3',
                text: 'ИНН организации',
                correct: false
            }, {
                id: 'fs9rro6g',
                text: 'СНИЛС',
                correct: false
            }]
        }]
    }
};
