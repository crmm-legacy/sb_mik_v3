export default {
    id: '/qnw9t877',
    name: 'Субсидирование. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/21/2/2',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'radio',
            id: 'fs2skfwb',
            text: 'Какой дисконт на ставку по программе субсидирования предоставят Клиенту, если он купит готовую квартиру у физического лица?',
            description: '',
            answers: [{
                id: 'ja6tso63',
                text: '1,5 %',
                correct: false
            }, {
                id: 'avj92ykb',
                text: '2 %',
                correct: false
            }, {
                id: 'ipogc54k',
                text: '1 %',
                correct: false
            }, {
                id: 'bixxww9l',
                text: 'Дисконт не предоставляется',
                correct: true
            }]
        }, {
            type: 'radio',
            id: 'ii6c4n2r',
            text: 'Какой дисконт на ставку по программе субсидирования предоставят Клиенту, если он купит готовую квартиру у застройщика. Срок кредита — 6 лет. Застройщик участвует в программе субсидирования.',
            description: '',
            answers: [{
                id: 'y7tamjks',
                text: '1,5 %',
                correct: false
            }, {
                id: 'm2h7o7js',
                text: '2 %',
                correct: true
            }, {
                id: '5fq2zzai',
                text: '1 %',
                correct: false
            }, {
                id: 'vtriv7d6',
                text: 'Дисконт не предоставляется',
                correct: false
            }]
        }, {
            type: 'radio',
            id: '4gcpntnj',
            text: 'Какой дисконт на ставку по программе субсидирования предоставят Клиенту, если он купит строящуюся квартиру у застройщика. Срок кредита — 15 лет. Застройщик участвует в программе субсидирования.',
            description: '',
            answers: [{
                id: 'd5nr6ua6',
                text: '1,5 %',
                correct: false
            }, {
                id: 'u0vlzdue',
                text: '2 %',
                correct: false
            }, {
                id: 'ek2776sw',
                text: '1 %',
                correct: false
            }, {
                id: 'nlg2s3d0',
                text: 'Дисконт не предоставляется',
                correct: true
            }]
        }, {
            type: 'radio',
            id: 'w1uojo9w',
            text: 'Допускается ли выдача кредита по программе субсидирования, если нет гарантийного письма об оплате субсидии от застройщика?',
            description: '',
            answers: [{
                id: 'iowjk2i1',
                text: 'Нет, ни при каких обстоятельствах',
                correct: true
            }, {
                id: 'muowykz1',
                text: 'Да, в редких случаях',
                correct: false
            }, {
                id: 'j06hkg06',
                text: 'Да, возможна',
                correct: false
            }]
        }]
    }
};
