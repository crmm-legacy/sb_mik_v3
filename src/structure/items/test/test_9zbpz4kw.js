export default {
    id: '/9zbpz4kw',
    name: 'Визуальная оценка. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/22/2/1',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'radio',
            id: '3hjdidkn',
            text: 'При проведении визуальной оценки Клиента необходимо:',
            description: '',
            answers: [{
                id: 'b8oa7xne',
                text: 'Сравнить внешность Клиента с фотографиями в документах',
                correct: false
            }, {
                id: 'eiet9iho',
                text: 'Все варианты верны',
                correct: true
            }, {
                id: '27dofhla',
                text: 'Оценить вменяемость Клиента',
                correct: false
            }, {
                id: '3u6zozx3',
                text: 'Обратить внимание на соответствие внешнего вида и поведения Клиента заявленному статусу',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: '2uajxujh',
            text: 'Укажите, что из перечисленного будет считаться нарушением при проведении визуальной оценки паспорта',
            description: '',
            answers: [{
                id: 'rf1zrku6',
                text: 'Наличие отметок и записей, не предусмотренных Положением о паспорте РФ',
                correct: true
            }, {
                id: 'eq04ak1p',
                text: 'Код подразделения, выдавшего паспорт, не соответствует региону выдачи',
                correct: true
            }, {
                id: 'afudwuqp',
                text: 'Количество листов паспорта 10 штук',
                correct: false
            }, {
                id: 'upfx6534',
                text: 'Серия и номер паспорта одинаковы на каждом листе',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'edl0oja4',
            text: 'Укажите срок действия справки о доходах в календарных днях',
            description: '',
            answers: [{
                id: 'qabrhi9b',
                text: 30,
                correct: true
            }, {
                id: '0pjz7sok',
                text: 28,
                correct: false
            }, {
                id: 'zsvetq6d',
                text: 14,
                correct: false
            }, {
                id: 'dghahzt2',
                text: 7,
                correct: false
            }]
        }]
    }
};
