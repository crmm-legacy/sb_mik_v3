export default {
    id: '/0hyhwnu1',
    name: 'Правовая экспертиза. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/5/4/3',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'checkbox',
            id: 'qd465bah',
            text: 'Отметьте необходимые условия для предоставления услуги «Правовая экспертиза»?',
            description: '',
            answers: [{
                id: 'lgv34skd',
                text: 'Не оформляется для объектов на этапе строительства (новостройки)',
                correct: true
            }, {
                id: 'whzwyj0c',
                text: 'Объект недвижимости находится на территории РФ',
                correct: true
            }, {
                id: 'extf48gz',
                text: 'Собственник объекта недвижимости — только гражданин РФ',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 'noamjzin',
            text: 'Как Клиент может воспользоваться услугой «Правовая экспертиза»?',
            description: '',
            answers: [{
                id: '59ec0zdk',
                text: 'Оформляет заявку в личном кабинете «ДомКлик»',
                correct: true
            }, {
                id: 'otwkooi1',
                text: 'Менеджер по ипотечному кредитованию оформляет через заявку в CRM',
                correct: true
            }, {
                id: 'gx9ezkpc',
                text: 'Клиент лично обращается к менеджеру по продажам в ВСП',
                correct: false
            }]
        }, {
            type: 'radio',
            id: '0aku0oua',
            text: 'Как Клиент получит результат экспертизы?',
            description: '',
            answers: [{
                id: 'o9w9r9zk',
                text: 'В личном кабинете на сайте «ДомКлик»',
                correct: true
            }, {
                id: 'nwlt68ap',
                text: 'Обращение в ЦИК и получение заключения на бумаге',
                correct: false
            }]
        }]
    }
};
