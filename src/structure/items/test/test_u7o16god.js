export default {
    id: '/u7o16god',
    name: 'Приобретение готового жилья. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/2/1/2',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'radio',
            id: '3q8v9b3v',
            text: 'Какой объект недвижимости нельзя приобрести в рамках продукта «Приобретение готового жилья»?',
            description: '',
            answers: [{
                id: 'ud9mkx61',
                text: 'Квартира',
                correct: false
            }, {
                id: 'rk64wz13',
                text: 'Комната',
                correct: false
            }, {
                id: 'bbzvv2yb',
                text: 'Доля в праве собственности на квартиру',
                correct: false
            }, {
                id: 'g07enxwu',
                text: 'Апартаменты',
                correct: true
            }]
        }, {
            type: 'radio',
            id: 't9hhz5gm',
            text: 'Минимальный первоначальный взнос по продукту составляет:',
            description: '',
            answers: [{
                id: 'xu94agak',
                text: '10 %',
                correct: false
            }, {
                id: '7ey32n3q',
                text: '15 %',
                correct: true
            }, {
                id: 'rvds9g0p',
                text: '20 %',
                correct: false
            }, {
                id: 'q1fr3vh5',
                text: '25 %',
                correct: false
            }]
        }, {
            type: 'radio',
            id: '5um6wp99',
            text: 'Минимальная сумма кредита по продукту «Приобретение готового жилья» составляет:',
            description: '',
            answers: [{
                id: 'bu4dga0h',
                text: '300 000 рублей',
                correct: true
            }, {
                id: 'vmj7dtzk',
                text: '200 000 рублей',
                correct: false
            }, {
                id: 'v2qpbzhp',
                text: '350 000 рублей',
                correct: false
            }, {
                id: 'oda8nud9',
                text: '400 000 рублей',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'iadsm42g',
            text: 'Что подписывают покупатель и продавец при приобретении готового жилья?',
            description: '',
            answers: [{
                id: '778svdbb',
                text: 'Договор передачи',
                correct: false
            }, {
                id: '0phui309',
                text: 'Договор дарения',
                correct: false
            }, {
                id: '9cccxlq1',
                text: 'Договор купли-продажи',
                correct: true
            }, {
                id: 'wz6phfd9',
                text: 'Договор переуступки прав',
                correct: false
            }]
        }]
    }
};
