export default {
    id: '/829y1dd9',
    name: 'Оценка объекта недвижимости. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/5/2/3',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'checkbox',
            id: 'jq09yx4v',
            text: 'Для определения стоимости отчета об оценке необходимо ввести в CRM:',
            description: '',
            answers: [{
                id: 'tf7i7itl',
                text: 'Адрес объекта',
                correct: true
            }, {
                id: 'ibo8b6m6',
                text: 'Площадь объекта',
                correct: true
            }, {
                id: 'xrn6hj4j',
                text: 'Кадастровый номер объекта',
                correct: false
            }, {
                id: 'zh66robu',
                text: 'Количество комнат',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'yjq1kb9h',
            text: 'Заказ отчета об оценке является обязательным:',
            description: '',
            answers: [{
                id: 'u31vxixu',
                text: 'Для любой программы ипотечного кредитования',
                correct: false
            }, {
                id: 'nbrdg803',
                text: 'При покупке объекта на вторичном рынке',
                correct: true
            }, {
                id: '0siwnsfs',
                text: 'Только при покупке квартиры на первичном рынке',
                correct: false
            }, {
                id: 'aa7o66oj',
                text: 'Не является обязательным, заказывается по желанию Клиента',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'pbukow5z',
            text: 'Срок подготовки отчета составляет:',
            description: '',
            answers: [{
                id: 'gafwioxv',
                text: '3 дня с момента осмотра и предоставления документов и 3 дня на доставку в ЦИК',
                correct: false
            }, {
                id: 'yn6oqo7p',
                text: '5 дней с момента осмотра и предоставления документов и 2 дня на доставку в ЦИК',
                correct: false
            }, {
                id: '31gxh2uo',
                text: '3 дня с момента полного осмотра и предоставления документов и 2 дня на доставку в ЦИК',
                correct: true
            }, {
                id: 'w5t6r0q5',
                text: '3 дня с момента полного осмотра и предоставления документов и 3 дня на доставку в ЦИК',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 'vrkpbu9n',
            text: 'Заказать отчет об оценке через менеджера по ипотечному кредитованию Клиент может по:',
            description: '',
            answers: [{
                id: 'i77kmwh4',
                text: 'Квартире',
                correct: true
            }, {
                id: 'qz3to597',
                text: 'Комнате',
                correct: true
            }, {
                id: 'z3j0ah7o',
                text: 'Дому',
                correct: false
            }, {
                id: 'jowm5t64',
                text: 'Земельному участку',
                correct: false
            }]
        }, {
            type: 'radio',
            id: '12vawsxg',
            text: 'Оценочная компания приступает к работе с заявкой:',
            description: '',
            answers: [{
                id: 'm8c5uptd',
                text: 'После оплаты Клиентом стоимости оценочного альбома',
                correct: true
            }, {
                id: '3g8khzst',
                text: 'После нажатия кнопки «Заказать оценку»',
                correct: false
            }, {
                id: '6z47b4wp',
                text: 'После ввода адреса и площади в заявку',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 's8v0421q',
            text: 'Если отчет подготовлен в электронном виде с подписью УКЭП, то в бумажном виде поступит:',
            description: '',
            answers: [{
                id: 'n828ay7f',
                text: '1 экземпляр для Банка',
                correct: false
            }, {
                id: 'zzaj8leh',
                text: '1 экземпляр для Клиента',
                correct: true
            }, {
                id: 'v07mas06',
                text: '2 экземпляра для Банка и Клиента',
                correct: false
            }, {
                id: '3qd3uu7n',
                text: 'Не будет отчета в бумажном виде',
                correct: false
            }]
        }]
    }
};
