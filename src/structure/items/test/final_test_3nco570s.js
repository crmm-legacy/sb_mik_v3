export default {
    id: '/3nco570s',
    name: 'Финальное тестирование',
    type: 'test',
    component: 'test_compo',
    content: {
        name: 'Итоговое тестирование по курсу',
        type: 'test'
    },
    oldId: '/25/0',
    shuffle: true,
    config: {
        isFinalTest: true,
        basedOn: [
            // module_0
            {
                id: '/o6uzp13h',
                numOfQuestions: 1
            },
            {
                id: '/n83l3o4h',
                numOfQuestions: 1
            },
            {
                id: '/1u5ontk6',
                numOfQuestions: 1
            },
            // module_1
            {
                id: '/lg8y9fch',
                numOfQuestions: 1
            },
            {
                id: '/aovlgp46',
                numOfQuestions: 1
            },
            {
                id: '/qnw9t877',
                numOfQuestions: 1
            },
            // module_2
            {
                id: '/lb0tqfvm',
                numOfQuestions: 1
            },
            {
                id: '/mj2h2upg',
                numOfQuestions: 1
            },
            {
                id: '/9zbpz4kw',
                numOfQuestions: 1
            },
            // module_3
            {
                id: '/4dh7yqoc',
                numOfQuestions: 1
            },
            // module_4
            {
                id: '/8xqhn3hf',
                numOfQuestions: 1
            }
        ]
    }
};
