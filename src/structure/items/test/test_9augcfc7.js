export default {
    id: '/9augcfc7',
    name: 'Клиенту одобрена сумма меньше запрошенной. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/1/4/1',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'checkbox',
            id: 'z46zws1p',
            text: 'Какую информацию менеджер должен проверить до первого звонка Клиенту?',
            description: '',
            answers: [{
                id: '39tg54vn',
                text: 'Категорию Клиента',
                correct: true
            }, {
                id: 'fmajd07s',
                text: 'Признак «Молодая семья»',
                correct: true
            }, {
                id: '9wfedwfx',
                text: 'Номер зарплатной карты',
                correct: false
            }, {
                id: 'f7l8il7s',
                text: 'Менял ли Клиент карту в последние 6 месяцев',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: '5kjpr7sk',
            text: 'Какую информацию менеджер должен проверить во время звонка Клиенту?',
            description: '',
            answers: [{
                id: 'l01ntv4y',
                text: 'Номер зарплатной карты',
                correct: true
            }, {
                id: '3pwtobkf',
                text: 'Категорию Клиента',
                correct: false
            }, {
                id: 'nd0ed3hn',
                text: 'Менял ли Клиент карту в последние 6 месяцев',
                correct: true
            }, {
                id: 'tx2l58rz',
                text: 'Признак «Молодая семья»',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 'yye2l06b',
            text: 'Какими способами можно увеличить сумму кредита?',
            description: '',
            answers: [{
                id: '20pwl06p',
                text: 'Привлечь созаемщика с учетом платежеспособности',
                correct: true
            }, {
                id: 'txlzmcat',
                text: 'Изменить срок кредита',
                correct: true
            }, {
                id: 'fc9t79rx',
                text: 'Погасить действующие кредиты',
                correct: true
            }, {
                id: 'upgq9a5r',
                text: 'Учесть дополнительный доход, который Клиент может подтвердить официально',
                correct: true
            }, {
                id: 'eap3r7gb',
                text: 'Выйти из состава поручителей',
                correct: true
            }, {
                id: 'ifdc9ikz',
                text: 'Указать любую сумму дополнительного дохода независимо от его наличия',
                correct: false
            }, {
                id: 'v91e55uu',
                text: 'Привлечь поручителя с учетом платежеспособности',
                correct: false
            }]
        }]
    }
};
