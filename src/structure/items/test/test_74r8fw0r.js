export default {
    id: '/74r8fw0r',
    name: 'Анализ отчета об оценке. Тест',
    type: 'test',
    component: 'test_compo',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'checkbox',
            id: 'neg1pkxn',
            text: 'В отчете об оценке обязательно должны быть',
            description: '',
            answers: [{
                id: 'og94377v',
                text: 'сведения об оценщике',
                correct: true
            }, {
                id: 'ohvvismo',
                text: 'правоустанавливающие документы',
                correct: true
            }, {
                id: 'x5779fkv',
                text: 'фотографии общего вида объекта',
                correct: true
            }, {
                id: 'nsva0wfd',
                text: 'фотографии таблички с адресом объекта',
                correct: false
            }, {
                id: 'odze6m2o',
                text: 'всё перечисленное не является обязательным',
                correct: false
            }]
        },
        {
            type: 'checkbox',
            id: 'wl7kpglg',
            text: 'Укажите критичные перепланировки, квартиры с которыми банк не кредитует',
            description: '',
            answers: [{
                id: 'z40afvic',
                text: 'снос несущих стен',
                correct: true
            }, {
                id: 'oa7dmx1t',
                text: 'снос подоконного блока',
                correct: false
            }, {
                id: 'at3cm18x',
                text: 'перенос газового оборудования за пределы кухни',
                correct: true
            }, {
                id: 'c9gvktp0',
                text: 'перенос радиаторов отопления на балконы или лоджии',
                correct: true
            }, {
                id: 'zvdhvin0',
                text: 'пристройка балконов или лоджий',
                correct: true
            }]
        },
        {
            type: 'checkbox',
            id: '7skyqkra',
            text: 'Банк принимает отчеты, которые сделаны с применением',
            description: '',
            answers: [{
                id: 'f3jk6xfd',
                text: 'доходного подхода',
                correct: false
            }, {
                id: 'u2by13tl',
                text: 'затратного подхода',
                correct: true
            }, {
                id: 'oas71fq4',
                text: 'сравнительного подхода',
                correct: true
            }]
        },
        {
            type: 'checkbox',
            id: 'ea7v4cbt',
            text: 'Укажите требования к объектам-аналогам при применении сравнительного подхода для оценки недвижимости',
            description: '',
            answers: [{
                id: 'r69gxk3k',
                text: 'Аналоги имеют схожие характеристики с объектом оценки',
                correct: true
            }, {
                id: 'vv9w4kb8',
                text: 'Аналогов не менее 4 для городов с населением более 500 тысяч человек',
                correct: true
            }, {
                id: 'k4dr2yh1',
                text: 'Аналогов не менее 3 для городов с населением менее 500 тысяч человек',
                correct: false
            }, {
                id: 'fyhsvy2s',
                text: 'При оценке квартиры аналогом может быть как квартира, так и земельный участок',
                correct: false
            }]
        },
        {
            type: 'radio',
            id: 'em96d7xm',
            text: 'Укажите пороговое значение коэффициентов Т и Т1',
            description: '',
            answers: [{
                id: 'wqel4d2z',
                text: '10',
                correct: false
            }, {
                id: 'tbsueb3w',
                text: '15',
                correct: false
            }, {
                id: 't4d0m427',
                text: '20',
                correct: true
            }, {
                id: 'sf9yrvle',
                text: '25',
                correct: false
            }]
        },
        {
            type: 'radio',
            id: 'fjqy8w2h',
            text: 'Если по итогам ручной верификации отчета об оценке коэффициент Т больше 20%',
            description: '',
            answers: [{
                id: 'hny4c75s',
                text: 'отправляем отчет на дополнительную проверку в уполномоченное подразделение',
                correct: true
            }, {
                id: 'yhh948r9',
                text: 'рассчитываем коэффициент Т1 и сравниваем с пороговым значением',
                correct: false
            }]
        },
        {
            type: 'radio',
            id: 'eg2dzzyg',
            text: 'Если по итогам ручной верификации отчета об оценке коэффициент Т1 меньше 20%',
            description: '',
            answers: [{
                id: 'oey30oi7',
                text: 'принимаем отчет, никуда его не отправляем и одобряем объект в стандартном режиме',
                correct: true
            }, {
                id: 'fjbm42yi',
                text: 'отправляем в уполномоченное подразделение для детальной проверки',
                correct: false
            },
            {
                id: '17yxyh66',
                text: 'отчет не принимается. Просим клиента заменить отчет или объект',
                correct: false
            }]
        }
        ]
    }
};
