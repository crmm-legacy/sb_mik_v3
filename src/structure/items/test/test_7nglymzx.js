export default {
    id: '/7nglymzx',
    name: 'УВС. Тест',
    type: 'test',
    component: 'test_compo',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'checkbox',
            id: '4iy6p0gx',
            text: 'Выберите действия менеджера по ипотеке при проведении удаленной сделки',
            description: '',
            answers: [{
                id: 'n8kv1myf',
                text: 'консультация клиента на этапе поиска объекта',
                correct: true
            }, {
                id: '8b370zvt',
                text: 'подготовка кредитной документации',
                correct: true
            }, {
                id: '6btj1ydz',
                text: 'подготовка кредитной документации',
                correct: false
            }, {
                id: '6btj1ydz',
                text: 'отправка заявки в финмониторинг',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 'p8dftag2',
            text: 'Выберите действия менеджера по продажам при проведении удаленной сделки',
            description: '',
            answers: [{
                id: '2qooi3o8',
                text: 'отправка документов о целевом использовании кредита',
                correct: false
            }, {
                id: 'yhvzl2q2',
                text: 'проверка договора купли-продажи на наличие отметки Росреестра',
                correct: true
            }, {
                id: '87zjtxiw',
                text: 'подготовка кредитной документации',
                correct: false
            }, {
                id: 'c6te9jmn',
                text: 'фотографирование клиента, если заявка подавалась через ДомКлик или партнера',
                correct: true
            }]
        }]
    }
};
