export default {
    id: '/nozob3vx',
    name: 'Финальное тестирование',
    type: 'test',
    component: 'test_compo',
    content: {
        name: 'Итоговое тестирование по курсу',
        type: 'test'
    },
    shuffle: true,
    config: {
        isFinalTest: true,
        basedOn: [
            // module_0
            {
                id: '/i7a9sip8',
                numOfQuestions: 1
            },
            {
                id: '/n83l3o4h',
                numOfQuestions: 1
            }
        ]
    }
};
