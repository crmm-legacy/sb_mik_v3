export default {
    id: '/3jab0fsf',
    name: 'Сервис электронной регистрации. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/5/3/1',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [
        //     {
        //     type: 'radio',
        //     id: 'rscmvvqt',
        //     text: 'Оформляется ли закладная на объект недвижимости при электронной регистрации?',
        //     description: '',
        //     answers: [{
        //         id: '1ezso3x4',
        //         text: 'Да',
        //         correct: false
        //     }, {
        //         id: 'w606kcnb',
        //         text: 'Нет',
        //         correct: true
        //     }]
        // },
            {
                type: 'checkbox',
                id: 'sqtrg36t',
                text: 'Какие документы получит покупатель после электронной регистрации права собственности на квартиру в рамках продукта «Приобретение готового жилья»?',
                description: '',
                answers: [{
                    id: 'j6d5ybmw',
                    text: 'ДКП с электронной подписью Росреестра о регистрации права собственности',
                    correct: true
                }, {
                    id: 'ivmxlycp',
                    text: 'Выписку из ЕГРН с УКЭП регистратора',
                    correct: true
                }, {
                    id: 'kovk4hxi',
                    text: 'Свидетельство о регистрации права собственности с УКЭП регистратора',
                    correct: false
                }, {
                    id: 'e18r6z7a',
                    text: 'ДДУ с электронной подписью Росреестра о регистрации права требования',
                    correct: false
                }]
            }, {
                type: 'checkbox',
                id: 'p0ep2120',
                text: 'С помощь сервиса электронной регистрации можно зарегистрировать сделки с покупкой:',
                description: '',
                answers: [{
                    id: 'fja9yo8x',
                    text: 'Квартиры',
                    correct: true
                }, {
                    id: 'zbvv3pt0',
                    text: 'Комнаты',
                    correct: true
                }, {
                    id: 'mb18eo96',
                    text: 'Земельного участка',
                    correct: true
                }, {
                    id: '9uzlmawa',
                    text: 'Жилого дома с земельным участком',
                    correct: false
                }]
            }, {
                type: 'radio',
                id: 'shyixh9z',
                text: 'Можно ли использовать сервис электронной регистрации, если продавцы владеют объектом недвижимости в долях?',
                description: '',
                answers: [{
                    id: 'aapfmc2b',
                    text: 'Нельзя',
                    correct: true
                }, {
                    id: 'wfi608iq',
                    text: 'Можно',
                    correct: false
                }]
            }, {
                type: 'checkbox',
                id: 'wg68e803',
                text: 'Сервис электронной регистрации нельзя применять:',
                description: '',
                answers: [{
                    id: 'at6gbady',
                    text: 'В рамках продукта «Военная ипотека»',
                    correct: true
                }, {
                    id: 'vzs9vy9s',
                    text: 'В рамках продукта «Приобретение строящего жилья» в мертвый период',
                    correct: true
                }, {
                    id: 'kbo4id0j',
                    text: 'В сделках с одним продавцом',
                    correct: false
                }, {
                    id: '9iryryqj',
                    text: 'В сделках с двумя покупателями, состоящими в браке',
                    correct: false
                }]
            }]
    }
};
