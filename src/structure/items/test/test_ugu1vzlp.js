export default {
    id: '/ugu1vzlp',
    name: 'Подтверждение первоначального взноса. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/4/0/1',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'radio',
            id: '3fu87uh4',
            text: 'Чему равняется минимальный первоначальный взнос в Банке?',
            description: '',
            answers: [{
                id: '0cdkco3x',
                text: '15 %',
                correct: true
            }, {
                id: 'qg1cno58',
                text: '20 %',
                correct: false
            }, {
                id: 'zgp5tt7i',
                text: '25 %',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'tza83k7l',
            text: 'Где можно получить выписку об остатке денежных средств на счете?',
            description: '',
            answers: [{
                id: 'mmxbgcjw',
                text: 'В любом отделении СБ по всей России',
                correct: false
            }, {
                id: 'qtv1fo6h',
                text: 'В любом отделении СБ в рамках ТБ',
                correct: true
            }, {
                id: '7v9v5j9m',
                text: 'В любом отделении СБ в рамках ГОСБ',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 'lgzo6gq5',
            text: 'С помощью какого документа можно подтвердить первоначальный взнос?',
            description: '',
            answers: [{
                id: 'fx9m65de',
                text: 'Выписка об остатке средств на счете',
                correct: true
            }, {
                id: '2gt6594d',
                text: 'Документы о факте оплаты (аванс или задаток)',
                correct: true
            }, {
                id: 'v2f3ksyz',
                text: 'Сертификат на получении субсидии',
                correct: true
            }, {
                id: 'hxlk999u',
                text: 'Расписка от юридического лица',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: '9g70y5yq',
            text: 'Укажите верные утверждения:',
            description: '',
            answers: [{
                id: 'olryh8bg',
                text: 'Первоначальный взнос можно подтвердить только одним документом, например, выпиской об остатке денег на счете',
                correct: false
            }, {
                id: 'r44mozpt',
                text: 'Первоначальный взнос можно подтвердить одновременно несколькими документами. Например, распиской продавца и выпиской об остатке денег на счете',
                correct: true
            }, {
                id: 'jjf5i96t',
                text: 'ДКП о продаже уже имеющегося в собственности объекта недвижимости может быть подтверждением первоначального взноса (в рамках альтернативных сделок)',
                correct: true
            }]
        }]
    }
};
