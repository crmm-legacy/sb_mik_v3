export default {
    id: '/gvop93j1',
    name: 'Работа МИК. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/0/3/1',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'radio',
            id: '456mar3d',
            text: 'Что такое сервис электронной регистрации?',
            description: '',
            answers: [{
                id: 'cy6rliz4',
                text: 'Онлайн-регистрация права собственности на недвижимость',
                correct: true
            }, {
                id: 'sxvyztn3',
                text: 'Безналичный перевод денежных средств продавцу',
                correct: false
            }, {
                id: 'me8z1zyo',
                text: 'Проверка участников сделки и самого объекта',
                correct: false
            }, {
                id: 'q7dkbzuk',
                text: 'Страхование жизни и здоровья заемщика',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'qmt3ncgn',
            text: 'В какое время МИК совершает первый звонок Клиенту?',
            description: '',
            answers: [{
                id: '2ku7hoj3',
                text: 'Не позднее 4 часов с момента поступления заявки',
                correct: true
            }, {
                id: '6y86ly3m',
                text: 'Не позднее 2 часов с момента поступления заявки',
                correct: false
            }, {
                id: 'o976hve0',
                text: 'До конца рабочего дня',
                correct: false
            }, {
                id: '0vviojnf',
                text: 'До конца следующего рабочего дня',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 'i5ictu5k',
            text: 'МИК может самостоятельно:',
            description: '',
            answers: [{
                id: '8bjxpr03',
                text: 'Заказать выписку ЕГРН',
                correct: true
            }, {
                id: 'e05yx0oz',
                text: 'Заказать отчет об оценке объекта недвижимости',
                correct: true
            }, {
                id: 'zj8hk9sk',
                text: 'Заказать выписку из домовой книги',
                correct: false
            }, {
                id: 'q356y69a',
                text: 'Составить договор купли-продажи',
                correct: true
            }]
        }]
    }
};
