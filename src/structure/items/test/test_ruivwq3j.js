export default {
    id: '/ruivwq3j',
    name: 'Подтверждение дохода. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/1/2/1',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'checkbox',
            id: '81jtos1t',
            text: 'В каких случаях заемщики 1 и 2 категорий предоставляют документы, подтверждающие доход?',
            description: '',
            answers: [{
                id: '9dyxylm9',
                text: 'Клиент хочет учесть несколько источников дохода (не только зарплатную карту)',
                correct: true
            }, {
                id: 'sajcz3wv',
                text: 'Если у Клиента 2-й категории не перечисляются средства в течение 4 месяцев и более за последние полгода',
                correct: true
            }, {
                id: 'aambp7lj',
                text: 'Если привлекают созаемщиков для увеличения суммы кредита',
                correct: false
            }, {
                id: 'gipf0nxr',
                text: 'Если предоставлен брачный контракт',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 'gpsh3wei',
            text: 'Укажите верные утверждения о справке 2-НДФЛ:',
            description: '',
            answers: [{
                id: 'xnnrpar8',
                text: 'Справка должна содержать доход за последние 6 месяцев',
                correct: true
            }, {
                id: 'ix4x1no4',
                text: 'Справка должна содержать сведения о доходе за последний год',
                correct: false
            }, {
                id: '5bjhouls',
                text: 'Справка действует 20 дней с даты выдачи',
                correct: false
            }, {
                id: '4od477hp',
                text: 'Если справка выдана с 1 по 15 число месяца включительно, то она может не содержать сведений о доходе за предыдущий месяц',
                correct: true
            }]
        }, {
            type: 'checkbox',
            id: 'yrdlrtyq',
            text: 'Какие утверждения относятся к справке по форме Банка?',
            description: '',
            answers: [{
                id: 'yoo9srez',
                text: 'В справке необязательна информация о месте нахождения и почтовый адрес организации',
                correct: false
            }, {
                id: 'wtcn8szo',
                text: 'В справке необязательно указывать телефон бухгалтерии или отдела кадров',
                correct: false
            }, {
                id: 'e886unvx',
                text: 'Вместо телефона бухгалтерии или отдела кадров можно указать личный телефон заемщика',
                correct: false
            }, {
                id: 'yh19desm',
                text: 'Справка должна быть по форме Сбербанка',
                correct: true
            }, {
                id: 'wxk44wav',
                text: 'В справке содержатся данные о заемщике и его доходах',
                correct: true
            }, {
                id: 'zpf8y1z8',
                text: 'Справка может быть по форме иного банка',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 'dmef379a',
            text: 'Кто всегда подтверждает свой доход?',
            description: '',
            answers: [{
                id: 'bgpju3qw',
                text: 'Созаемщики с учетом платежеспособности',
                correct: true
            }, {
                id: 'ybueo483',
                text: 'Поручители без учета платежеспособности',
                correct: false
            }, {
                id: 'ic9eqybb',
                text: 'Заемщики 2-й категории',
                correct: false
            }, {
                id: '1c6aq3fv',
                text: 'Заемщики 4-й категории',
                correct: true
            }, {
                id: 'tvll634e',
                text: 'Залогодатель',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 'p0y4bnzz',
            text: 'Укажите верные утверждения для предоставления справки о доходах пенсионного характера:',
            description: '',
            answers: [{
                id: 'jgpm6eul',
                text: 'Если Клиент получает пенсию на счет Сбербанка, то предоставлять справку не нужно',
                correct: true
            }, {
                id: 'z9i5skqp',
                text: 'Справка предоставляется за последние 6 месяцев',
                correct: false
            }, {
                id: 'dco2tixg',
                text: 'Справка предоставляется за последний месяц',
                correct: true
            }, {
                id: 'kzuf7eju',
                text: 'Справку можно получить только в отделении Пенсионного фонда РФ',
                correct: false
            }]
        }, {
            type: 'radio',
            id: '5czoagpw',
            text: 'Если индивидуальный предприниматель лично предоставляет декларацию о доходах, какие документы нужно предоставить, кроме самой декларации?',
            description: '',
            answers: [{
                id: '3xs0dmzz',
                text: 'Почтовое извещение об отправке в налоговые органы',
                correct: false
            }, {
                id: '15tdd2ie',
                text: 'Дополнительные документы не требуются',
                correct: true
            }, {
                id: 'hojwzf4x',
                text: 'Протокол входного контроля',
                correct: false
            }, {
                id: 'kuaw44l3',
                text: 'Справка о доходах из Федеральной налоговой службы',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'xjhhnaj6',
            text: 'Нужно ли предоставлять дополнительные документы, если ИП подает налоговую декларацию по почте?',
            description: '',
            answers: [{
                id: '263w3v2p',
                text: 'Да',
                correct: true
            }, {
                id: 'a95grli3',
                text: 'Нет',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'hzs1wrhh',
            text: 'Каким способом ИП подает налоговую декларацию, если в пакет документов, помимо сведений о доходах, входит протокол входного контроля?',
            description: '',
            answers: [{
                id: 'j2relgzu',
                text: 'По почте',
                correct: false
            }, {
                id: 's46mscaf',
                text: 'Лично',
                correct: false
            }, {
                id: 'sr5dm3in',
                text: 'Через сайт Федеральной налоговой службы или через сайт «Госуслуги.ру»',
                correct: true
            }]
        }, {
            type: 'checkbox',
            id: '6qeqmlku',
            text: 'В каком случае потребуется справка 3-НДФЛ?',
            description: '',
            answers: [{
                id: '1dby1x3a',
                text: 'Клиент выполнял работу по договору ГПХ и перечислял налоги самостоятельно',
                correct: true
            }, {
                id: 'k9v4jqrb',
                text: 'Клиент выполнял работу по договору ГПХ, налоги за него перечисляла организация — заказчик работ',
                correct: false
            }, {
                id: 'xufr5wy1',
                text: 'Прибыль от аренды недвижимости при условии, что до окончания срока осталось не менее 12 месяцев',
                correct: true
            }, {
                id: '6m7kgog9',
                text: 'Прибыль от аренды недвижимости при условии, что до окончания срока осталось не менее 6 месяцев',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 'ccqzpirc',
            text: 'Какие источники дохода не учитываются Банком?',
            description: '',
            answers: [{
                id: '4xfpcyox',
                text: 'Алименты',
                correct: true
            }, {
                id: 'nonle65i',
                text: 'Зарплата на работе по совместительству',
                correct: false
            }, {
                id: '6rzod2cm',
                text: 'Страховые выплаты',
                correct: true
            }, {
                id: '6gex1oxj',
                text: 'Декретные выплаты',
                correct: true
            }]
        }, {
            type: 'checkbox',
            id: 's0b3psgq',
            text: 'Как Клиент может подтвердить доходы пенсионного характера, если получает их не через Сбербанк?',
            description: '',
            answers: [{
                id: 'qo4bzo9f',
                text: 'Выпиской из Банка, куда поступают отчисления',
                correct: false
            }, {
                id: '83f2mlu3',
                text: 'Справкой о размере пенсии из ПФР',
                correct: true
            }, {
                id: 'ubjcusbe',
                text: 'Справкой о размере пенсии из МФЦ',
                correct: true
            }, {
                id: 'iukjc9va',
                text: 'Такой доход не учитывается',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'bt5v3p78',
            text: 'За какой период ИП предоставляет налоговую декларацию, если он ведет учет налогов по ЕНВД?',
            description: '',
            answers: [{
                id: 'klhdz5tx',
                text: 'За последние 2 квартала',
                correct: true
            }, {
                id: 'qqsmn8se',
                text: 'За последний год',
                correct: false
            }, {
                id: '9hgb389y',
                text: 'За последний квартал',
                correct: false
            }]
        }]
    }
};
