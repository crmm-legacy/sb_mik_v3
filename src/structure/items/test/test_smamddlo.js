export default {
    id: '/smamddlo',
    name: 'Залоговые объекты. Тест',
    type: 'test',
    component: 'test_compo',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            id: '6pyhsbb4',
            type: 'checkbox',
            text: 'Укажите важные условия для проведения сделок с залоговыми объектами',
            description: '',
            answers: [{
                id: '4vimq87r',
                text: 'Первоначальный кредит продавца должен быть выдан в Сбербанке',
                correct: true
            }, {
                id: 'jwgzgeps',
                text: 'Сделки проходят только с использованием сервиса электронной регистрации',
                correct: false
            }, {
                id: '938k3pab',
                text: 'Использовать сервис безопасных расчетов по таким сделкам нельзя',
                correct: true
            }, {
                id: '2bowisj0',
                text: 'Для оформления сделки нужно обращаться в ЦИК в том ГОСБ, где был выдан первоначальный кредит',
                correct: true
            }]
        }, {
            id: 'ma14dweq',
            type: 'radio',
            text: 'Обращаться в Управление по работе с просроченной задолженностью при сделках с залоговыми объектами надо, только когда есть признаки проблем по кредиту',
            description: '',
            answers: [{
                id: 'nasb86pf',
                text: 'Верно',
                correct: false
            }, {
                id: '3f730o9y',
                text: 'Не верно. Если сделка по залоговому объекту — обращаться в Управление надо всегда',
                correct: true
            }]
        }, {
            id: 'yryleicg',
            type: 'checkbox',
            text: 'Купить залоговый объект в кредит можно, если это',
            description: '',
            answers: [{
                id: 'lgwj55mj',
                text: 'Комната',
                correct: true
            }, {
                id: 'k3p5gc5g',
                text: 'Квартира',
                correct: true
            }, {
                id: 'm9i636wn',
                text: 'Дом с земельным участком',
                correct: true
            }, {
                id: '2k70jd3d',
                text: 'Доля в праве собственности',
                correct: false
            }]
        }]
    }
};
