export default {
    id: '/9miu85fw',
    name: 'Путь Клиента. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/0/2/1',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'checkbox',
            id: 'xdfhsc7a',
            text: 'Кто может предоставить документы на объект недвижимости?',
            description: '',
            answers: [{
                id: '4zsdp0jo',
                text: 'Клиент через личный кабинет на сайте «ДомКлик»',
                correct: true
            }, {
                id: 'kkpjav3n',
                text: 'Продавец по ссылке через сайт «ДомКлик»',
                correct: true
            }, {
                id: '896jvend',
                text: 'Риелтор — партнер Банка через личный кабинет на сайте «ДомКлик Про»',
                correct: true
            }, {
                id: 'jhlnvonz',
                text: 'Сотрудник Отдела прямых продаж (DSA), через которого передали документы',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'ldupmh3g',
            text: 'В течение какого срока после одобрения кредита необходимо предоставить документы по объекту недвижимости?',
            description: '',
            answers: [{
                id: '2u4jgpkv',
                text: '90 дней',
                correct: true
            }, {
                id: 'iukdnxpc',
                text: '120 дней',
                correct: false
            }, {
                id: 'zrojwmra',
                text: '60 дней',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 'l416qkyf',
            text: 'Укажите задачи, которые верны для сотрудника МИК:',
            description: '',
            answers: [{
                id: 'ye8v1nwa',
                text: 'Подготовка кредитной документации',
                correct: true
            }, {
                id: '7jrous7j',
                text: 'Сбор документов по объекту недвижимости',
                correct: true
            }, {
                id: 'dec30ozp',
                text: 'Сопровождение кредита',
                correct: false
            }, {
                id: 'ngevly40',
                text: 'Оформление досрочного погашения',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 'er71oqlt',
            text: 'Укажите задачи, которые верны для сотрудника МОИК:',
            description: '',
            answers: [{
                id: '3tfnyor8',
                text: 'Подготовка кредитной документации',
                correct: false
            }, {
                id: 'bgvkfzrt',
                text: 'Предоставление справок',
                correct: true
            }, {
                id: 'lnyusncu',
                text: 'Оформление досрочного погашения',
                correct: true
            }, {
                id: 'nee5q570',
                text: 'Выдача кредита',
                correct: false
            }]
        }]
    }
};
