export default {
    id: '/k9cb5xne',
    name: 'Строительство жилого дома. Тест',
    type: 'test',
    component: 'test_compo',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            id: 'wvf7129a',
            type: 'radio',
            text: 'Кредит по программе «Строительство жилого дома» предоставляется на строительство:',
            description: '',
            answers: [{
                id: 'knaix7px',
                text: 'Одного жилого дома и гаража',
                correct: false
            }, {
                id: 'ikgc9q6t',
                text: 'Одного жилого дома и бани',
                correct: false
            }, {
                id: '0xbouy4v',
                text: 'Двух жилых домов',
                correct: false
            }, {
                id: 'xe3pvcnx',
                text: 'Одного жилого дома',
                correct: true
            }]
        }, {
            id: 'o4vdwllx',
            type: 'radio',
            text: 'Укажите размер первоначального взноса по программе «Строительство жилого дома»',
            description: '',
            answers: [{
                id: 'rshnbf55',
                text: '10',
                correct: false
            }, {
                id: '2k0bjsma',
                text: '15',
                correct: false
            }, {
                id: 'i8ym5wjm',
                text: '20',
                correct: false
            }, {
                id: 'rdlebsyg',
                text: '25',
                correct: true
            }]
        }, {
            id: 'qtiwhmyi',
            type: 'checkbox',
            text: 'Укажите верные утверждения по программе «Строительство жилого дома»',
            description: '',
            answers: [{
                id: 'h7z5wusy',
                text: 'Клиент может взять  кредит только на отделку жилого дома',
                correct: false
            }, {
                id: '9158ay1m',
                text: 'Кредит всегда выдается траншами',
                correct: true
            }, {
                id: '6nji7026',
                text: 'В банк нужно предоставить смету строительства ',
                correct: true
            }, {
                id: 'am3tte6v',
                text: 'Можно увеличить срок кредита, но не более 3 лет после выдачи первой части кредита',
                correct: false
            }]
        }, {
            id: 'zrwloeol',
            type: 'radio',
            text: 'Если обеспечением кредита по программе «Строительство жилого дома» будет кредитуемый объект, и земля, на который строится дом, у Клиента в собственности, то земля должна быть оформлена в залог до выдачи кредита',
            description: '',
            answers: [{
                id: 'glnqi7ns',
                text: 'Да',
                correct: true
            }, {
                id: 'x5hpc7x7',
                text: 'Нет',
                correct: false
            }]
        }]
    }
};
