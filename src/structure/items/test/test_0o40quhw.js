export default {
    id: '/0o40quhw',
    name: 'Приобретение строящегося жилья. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/2/0/1',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'checkbox',
            id: 'naxrx7q4',
            text: 'На основании какого договора можно приобрести квартиру на этапе строительства?',
            description: '',
            answers: [{
                id: '2usab6io',
                text: 'Договор долевого участия (ДДУ)',
                correct: true
            }, {
                id: '8tqegdbb',
                text: 'Договор уступки прав требования (ДУПТ)',
                correct: true
            }, {
                id: 'h1r2rywf',
                text: 'Предварительный договор купли-продажи (ПДКП)',
                correct: true
            }, {
                id: '816xpwlr',
                text: 'Договор купли-продажи (ДКП)',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'okto6lyp',
            text: 'Кто может быть продавцом при покупке квартиры на этапе строительства?',
            description: '',
            answers: [{
                id: '26tcaf8n',
                text: 'Только физическое лицо',
                correct: false
            }, {
                id: 'alt3alyu',
                text: 'Только застройщик',
                correct: false
            }, {
                id: 'u6vc84sb',
                text: 'Застройщик и физическое лицо',
                correct: true
            }]
        }, {
            type: 'radio',
            id: '7jnt8jmc',
            text: 'Если Клиент приобретает квартиру по допсоглашению к ДДУ, укажите верные утверждения по первоначальному взносу:',
            description: '',
            answers: [{
                id: 'god6j9q6',
                text: 'Ранее внесенные платежи зачтутся как первоначальный взнос, а остаток задолженности станет суммой кредита',
                correct: true
            }, {
                id: 'gunchvf0',
                text: 'Ранее внесенные платежи не учитываются как первоначальный взнос. Необходимо внести первоначальный взнос не 15 % от стоимости объекта',
                correct: false
            }, {
                id: 'b0f9zmk3',
                text: 'Первоначальный взнос не потребуется',
                correct: false
            }]
        }, {
            type: 'radio',
            id: '6lijjvv4',
            text: 'Укажите, какой срок есть у Клиента для подписания ДКП с застройщиком при покупке квартиры по предварительному договору купли продажи (ПДКП)?',
            description: '',
            answers: [{
                id: 'dw5y05i3',
                text: 'До 6 мес.',
                correct: true
            }, {
                id: 'aoae55eh',
                text: 'До 3 мес.',
                correct: false
            }, {
                id: 'g7wh61sx',
                text: 'До 9 мес.',
                correct: false
            }, {
                id: '8858a4kh',
                text: 'До 12 мес.',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: '2kmatpam',
            text: 'Что будет обеспечением по кредиту, если у дома нет аккредитации Банка и размер кредита менее 3 млн рублей?',
            description: '',
            answers: [{
                id: 'lkp8rz94',
                text: 'Залог прав требования',
                correct: false
            }, {
                id: 'xwqfbh9q',
                text: 'Залог иного объекта недвижимости',
                correct: true
            }, {
                id: 'hmkfs477',
                text: 'Залог кредитуемой недвижимости',
                correct: false
            }, {
                id: '5x60h1zt',
                text: 'Поручительство',
                correct: true
            }]
        }, {
            type: 'radio',
            id: '24w99fak',
            text: 'Что будет обеспечением по кредиту, если у дома нет аккредитации Банка и размер кредита более 3 млн рублей?',
            description: '',
            answers: [{
                id: 'ebvjda7m',
                text: 'Залог прав требования',
                correct: false
            }, {
                id: '1672tiga',
                text: 'Залог иного объекта недвижимости',
                correct: true
            }, {
                id: 'ld62pezd',
                text: 'Залог кредитуемой недвижимости',
                correct: false
            }, {
                id: 'za57vcyy',
                text: 'Поручительство',
                correct: false
            }]
        }]
    }
};
