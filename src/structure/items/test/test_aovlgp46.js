export default {
    id: '/aovlgp46',
    name: 'Ипотека с господдержкой. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/21/3/2',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'checkbox',
            id: 'euzzglkq',
            text: 'Кредитование по ипотеке с господдержкой возможно по продуктам',
            description: '',
            answers: [{
                id: 'gn8cll2c',
                text: 'Приобретение готового жилья',
                correct: true
            }, {
                id: 'nl1s5noi',
                text: 'Приобретение строящегося жилья',
                correct: true
            }, {
                id: 'k1m0mljb',
                text: 'Нецелевой кредит под залог объекта недвижимости',
                correct: false
            }, {
                id: '3gpb7uuu',
                text: 'Загородная недвижимость',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'neyi77ps',
            text: 'Укажите, кто может быть продавцом квартиры при оформлении ипотеки с господдержкой',
            description: '',
            answers: [{
                id: '0zlj5lcv',
                text: 'Юридическое лицо',
                correct: true
            }, {
                id: 'vycseqkp',
                text: 'Физическое лицо',
                correct: false
            }, {
                id: 'rzyznawq',
                text: 'Инвестиционные фонды',
                correct: false
            }, {
                id: 'ttv8hq9k',
                text: 'Все перечисленные',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'le9hsv8j',
            text: 'Укажите размер первоначального взноса при ипотеке с господдержкой',
            description: '',
            answers: [{
                id: '63674kbo',
                text: 10,
                correct: false
            }, {
                id: 'mnz5y104',
                text: 15,
                correct: false
            }, {
                id: 'w72jt7d7',
                text: 20,
                correct: true
            }, {
                id: 'nvobo3xv',
                text: 25,
                correct: false
            }]
        },
        // {
        //     type: 'radio',
        //     id: '9i27wbp5',
        //     text: 'Укажите все верные утверждения о созаемщиках по программе господдержки, когда родители не состоят в официальном браке',
        //     description: '',
        //     answers: [{
        //         id: 's1oroezf',
        //         text: 'Один из родителей будет титульным созаемщиком, а второй может стать созаемщиком с учетом платежеспособности',
        //         correct: true
        //     }, {
        //         id: 'mwrlk6re',
        //         text: 'Один из родителей будет титульным созаемщиком, а второй может стать созаемщиком без учета платежеспособности',
        //         correct: false
        //     }, {
        //         id: 'hmbr1d5y',
        //         text: 'Никто, кроме второго родителя ребенка, в рамках программы не может стать созаемщиком',
        //         correct: false
        //     }]
        // },
        {
            type: 'radio',
            id: 'v7ijh6x8',
            text: 'При ипотеке с господдержкой использовать средства материнского капитала можно',
            description: '',
            answers: [{
                id: 'ro3sm5eh',
                text: 'Для увеличения суммы кредита',
                correct: false
            }, {
                id: 'jn6rhr2w',
                text: 'Для частично досрочного погашения',
                correct: true
            }, {
                id: 'yd2rujlz',
                text: 'Оба ответа верны',
                correct: false
            }]
        }, {
            type: 'radio',
            id: '1ojb0a3t',
            text: 'При ипотеке с господдержкой использовать «Сервис безопасных расчетов»:',
            description: '',
            answers: [{
                id: 'bq3bx9lk',
                text: 'нельзя',
                correct: true
            }, {
                id: 'gpbl7ar0',
                text: 'можно',
                correct: false
            }]
        }, {
            type: 'radio',
            id: '0k8esrfm',
            text: 'При ипотеке с господдержкой использовать «Сервис электронной регистрации»:',
            description: '',
            answers: [{
                id: 'toy2evib',
                text: 'нельзя',
                correct: false
            }, {
                id: 'frowj8vv',
                text: 'можно, но скидка к ставке 0,1% не применяется',
                correct: true
            }, {
                id: '8qbnxjyz',
                text: 'можно, скидка к ставке 0,1% применяется',
                correct: false
            }]
        }]
    }
};
