export default {
    id: '/yfe08trv',
    name: 'Безналичные перечисления. Тест',
    type: 'test',
    component: 'test_compo',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            id: 'r0jhph6t',
            type: 'radio',
            text: 'При перечислении кредитных средств продавцу-юридическому лицу комиссия за перевод',
            description: '',
            answers: [{
                id: 'k7wdr6pm',
                text: 'Взимается согласно тарифам банка',
                correct: false
            }, {
                id: 'x0wy41wo',
                text: 'Не взимается',
                correct: true
            }, {
                id: 'wdpbcxwz',
                text: 'Не взимается, если кредитные средства переводятся продавцу одновременно с собственными средствами заемщика',
                correct: false
            }]
        }, {
            id: '64fl7kom',
            type: 'radio',
            text: 'Какой документ является основанием выдачи кредита на инвестирование строительства объекта недвижимости на этапе «мертвого периода»?',
            description: '',
            answers: [{
                id: '9yz9x6en',
                text: 'Договор долевого участия/договор уступки прав требования',
                correct: false
            }, {
                id: 'e08ncywf',
                text: 'Предварительный договор купли-продажи',
                correct: true
            }, {
                id: '9trsp7ak',
                text: 'Предварительный договор купли-продажи с отметками Росреестра о регистрации',
                correct: false
            }]
        }, {
            id: 'ahvr8wv9',
            type: 'radio',
            text: 'В каких случаях используется безналичное перечисление?',
            description: '',
            answers: [{
                id: 'v9y5yrbz',
                text: 'Если продавцом является физическое лицо',
                correct: false
            }, {
                id: 'eoynvaix',
                text: 'Если продавцом является юридическое лицо',
                correct: true
            }]
        }]
    }
};
