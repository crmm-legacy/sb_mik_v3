export default {
    id: '/t6ickhq7',
    name: 'Нотариальная сделка. Тест',
    type: 'test',
    component: 'test_compo',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'radio',
            id: 'g76kd1ua',
            text: 'Верно ли утверждение: «При регистрации договора ипотеки, когда собственность оформлена в долях, требуется нотариальное удостоверение»',
            description: '',
            answers: [{
                id: 'rytwrkud',
                text: 'Верно',
                correct: true
            }, {
                id: 'svxchdwx',
                text: 'Не верно',
                correct: false
            }, {
                id: '2s1kl3fs',
                text: 'По желанию',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: 'vnik4yxo',
            text: 'Нотариальное удостоверение договора для супругов не потребуется, если',
            description: '',
            answers: [{
                id: '9qbkz0mr',
                text: 'Имущество приобретается в совместную собственность',
                correct: true
            }, {
                id: '2wtwey9z',
                text: 'Супруги заключили брачный договор',
                correct: true
            }, {
                id: '0db5masf',
                text: 'Супруги заключили брачный договор',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'cujtq8rl',
            text: 'Какие сервисы можно использовать в нотариальных сделках',
            description: '',
            answers: [{
                id: 'yc5l52nv',
                text: 'Сервис электронной регистрации',
                correct: false
            }, {
                id: 'w8gcr41z',
                text: 'Сервис безопасных расчетов',
                correct: true
            }]
        }]
    }
};
