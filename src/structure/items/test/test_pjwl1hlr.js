export default {
    id: '/pjwl1hlr',
    name: 'Молодая семья. Тест',
    type: 'test',
    component: 'test_compo',
    oldId: '/3/0/1',
    content: {
        config: {
            name: 'Тест',
            type: 'test'
        },
        shuffle: true,
        questions: [{
            type: 'checkbox',
            id: '3y4hrlcl',
            text: 'Выберите семьи, которые подходят под условие «Молодая семья»:',
            description: '',
            answers: [{
                id: 'fg7ekbdw',
                text: 'Семья: мужчина 30 лет, женщина 27 лет, детей нет',
                correct: true
            }, {
                id: 'dnrpn58b',
                text: 'Семья: женщина 31 год, дочь 5 лет',
                correct: true
            }, {
                id: '4w5p9anv',
                text: 'Семья: мужчина 37 лет, женщина 35 лет, сын 8 лет',
                correct: false
            }, {
                id: 'rblrrk67',
                text: 'Семья: мужчина 35 лет, сын 7 лет',
                correct: false
            }]
        }, {
            type: 'radio',
            id: '29my1hpz',
            text: 'В рамках какого продукта предоставляется скидка на ставку по акции «Молодая семья»?',
            description: '',
            answers: [{
                id: 'yl30m4ic',
                text: '«Приобретение готового жилья»',
                correct: true
            }, {
                id: 'rysiokld',
                text: '«Приобретение строящегося жилья»',
                correct: false
            }, {
                id: 'ckjztlmg',
                text: '«Строительство жилого дома»',
                correct: false
            }, {
                id: 'wu139wsr',
                text: '«Загородная недвижимость»',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'hnbmuqjx',
            text: 'Максимальное количество созаемщиков в рамках особого условия «Молодая семья» составляет:',
            description: '',
            answers: [{
                id: 'gqv9nggp',
                text: 'Не более 6 (молодая семья и их родители)',
                correct: true
            }, {
                id: 'f7i2194c',
                text: 'Не более 6 (молодая семья и иные лица с учетом платежеспособности)',
                correct: false
            }, {
                id: 'u47pm07x',
                text: 'Не более 3',
                correct: false
            }, {
                id: 'g8si7uct',
                text: 'Только супруги — члены молодой семьи',
                correct: false
            }]
        }, {
            type: 'checkbox',
            id: '8yocldkx',
            text: 'Отсрочка погашения основного долга по условию «Молодая семья» предоставляется в рамках продукта:',
            description: '',
            answers: [{
                id: 'agcem5oh',
                text: '«Приобретение готового жилья»',
                correct: true
            }, {
                id: 'nbvsek5f',
                text: '«Приобретение строящегося жилья»',
                correct: true
            }, {
                id: '6gjj0xvl',
                text: '«Строительство жилого дома»',
                correct: false
            }, {
                id: 'pzmqodda',
                text: '«Загородная недвижимость»',
                correct: false
            }]
        }, {
            type: 'radio',
            id: 'j5xrapw8',
            text: 'По условию «Молодая семья» предусмотрена отсрочка погашения основного долга с увеличением срока кредитования на период до достижения ребенком (детьми) возраста:',
            description: '',
            answers: [{
                id: '5jm6bob3',
                text: 'Трех лет',
                correct: true
            }, {
                id: '1wwofnsi',
                text: 'Пяти лет',
                correct: false
            }, {
                id: 'khdh5mnn',
                text: 'Двух лет',
                correct: false
            }, {
                id: 'fsaa2b0i',
                text: 'Четырех лет',
                correct: false
            }]
        }]
    }
};
