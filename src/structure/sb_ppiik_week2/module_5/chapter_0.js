import final_test_sv99nfve from '@items/test/final_test_sv99nfve.js';

export default {
    id: '/qtda0z1p',
    type: 'item',
    component: 'chapter',
    name: 'Итоговый тест',
    items: [
        final_test_sv99nfve
    ]
};
