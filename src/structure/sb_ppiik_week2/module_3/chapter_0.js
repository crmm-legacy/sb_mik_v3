import video_t8m11ubx from '@items/video/video_t8m11ubx.js';
import pdf_oia2v965 from '@items/pdf/pdf_oia2v965.js';
import test_4dh7yqoc from '@items/test/test_4dh7yqoc.js';

export default {
    id: '/3xtx0qoe',
    name: 'Покупка доли и комнаты',
    type: 'item',
    component: 'chapter',
    items: [
        video_t8m11ubx,
        pdf_oia2v965,
        test_4dh7yqoc
    ]
};
