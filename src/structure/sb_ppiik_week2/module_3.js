import chapter_0 from './module_3/chapter_0';

export default {
    id: '/ulaqahoa',
    name: 'Особенности сделок',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0
    ]
};
