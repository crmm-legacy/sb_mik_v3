import chapter_0 from './module_4/chapter_0';

export default {
    id: '/5x9v64t8',
    name: 'Виды сделок',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0
    ]
};
