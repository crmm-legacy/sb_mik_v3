import video_rwsi6tet from '@items/video/video_rwsi6tet.js';
import test_lg8y9fch from '@items/test/test_lg8y9fch.js';

export default {
    id: '/8spf0mgf',
    name: 'Преимущества одобрения объекта на сайте ДомКлик',
    type: 'item',
    component: 'chapter',
    items: [
        video_rwsi6tet,
        test_lg8y9fch
    ]
};
