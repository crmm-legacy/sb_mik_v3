import video_gin3a90w from '@items/video/video_gin3a90w.js';
import test_qnw9t877 from '@items/test/test_qnw9t877.js';

export default {
    id: '/xibiws0k',
    name: 'Субсидирование',
    type: 'item',
    component: 'chapter',
    items: [
        video_gin3a90w,
        test_qnw9t877
    ]
};
