import video_s7jj7dr5 from '@items/video/video_s7jj7dr5.js';
import test_lb0tqfvm from '@items/test/test_lb0tqfvm.js';

export default {
    id: '/txgevu87',
    name: 'Одобрение объекта недвижимости',
    type: 'item',
    component: 'chapter',
    items: [
        video_s7jj7dr5,
        test_lb0tqfvm
    ]
};
