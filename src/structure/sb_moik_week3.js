import module_0 from './sb_moik_week3/module_0.js';
// import module_1 from './sb_moik_week3/module_1.js';
// import module_2 from './sb_moik_week3/module_2.js';
// import module_3 from './sb_moik_week3/module_3.js';
// import module_4 from './sb_moik_week3/module_4.js';
import module_5 from './sb_moik_week3/module_5.js';

export default {
    id: 'https://crmm.ru/xapi/courses/sb_moik_week3',
    name: 'Комплексная программа обучения МОИК. Неделя 3',
    type: 'course',
    version: 1,
    virtual: 'https://crmm.ru/xapi/courses/sb_mik-3',
    config: {
        color: 'red'
    },
    items: [
        module_0, // Ипотечные программы (Уровень 3)
        // module_1, // После выдачи (Уровень 3)
        // module_2, // Взаимодействие с РКС (ЦОППиИК) (Уровень 2)
        // module_3, // Взаимодействие с РОСЕЕСТРОМ
        // module_4, // Взаимодействие с ПРПЗ
        module_5 // Итоговое тестирование
    ],
    objectives: [{
        id: 'https://crmm.ru/xapi/courses/sb_moik_week3/ms_0_0',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/wwenpusc',
        pass_time: '1',
        icon: './static/vshs_icons/moik_week3/0.png',
        color: '#ea6c6c',
        scoped: true
    },/* {
        id: 'https://crmm.ru/xapi/courses/sb_moik_week3/ms_0_1',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/je4fygl9',
        pass_time: '1',
        icon: './static/vshs_icons/moik_week3/1.png',
        color: '#ea6c6c',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_moik_week3/ms_0_2',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/r0bw4zyq',
        pass_time: '1',
        icon: './static/vshs_icons/moik_week3/2.png',
        color: '#ea6c6c',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_moik_week3/ms_0_3',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/dqpkes0d',
        pass_time: '1',
        icon: './static/vshs_icons/moik_week3/3.png',
        color: '#ea6c6c',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_moik_week3/ms_0_4',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/rr915q0h',
        pass_time: '1',
        icon: './static/vshs_icons/moik_week3/4.png',
        color: '#ea6c6c',
        scoped: true
    }, */{
        id: 'https://crmm.ru/xapi/courses/sb_moik_week3/ms_0_5',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/dy2bt9ml',
        pass_time: '1',
        icon: './static/vshs_icons/global/final_test_new.png',
        color: '#ea6c6c',
        scoped: true
    }]
};
