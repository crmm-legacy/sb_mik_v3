import module_0 from './sb_mik-3_week1/module_0.js';
import module_1 from './sb_mik-3_week1/module_1.js';
import module_2 from './sb_mik-3_week1/module_2.js';
import module_3 from './sb_mik-3_week1/module_3.js';
import module_4 from './sb_mik-3_week1/module_4.js';
import module_5 from './sb_mik-3_week1/module_5.js';
import module_6 from './sb_mik-3_week1/module_6.js';
import module_7 from './sb_mik-3_week1/module_7.js';
import module_8 from './sb_mik-3_week1/module_8.js';
import module_9 from './sb_mik-3_week1/module_9.js';

export default {
    id: 'https://crmm.ru/xapi/courses/sb_mik-3_week1',
    name: 'Комплексная программа обучения МИК. Неделя 1',
    type: 'course',
    version: 1,
    virtual: 'https://crmm.ru/xapi/courses/sb_mik-3',
    config: {
        color: 'blue'
    },
    items: [
        module_0,
        module_1,
        module_2,
        module_3,
        module_4,
        module_5,
        module_6,
        module_7,
        module_8,
        module_9
    ],
    objectives: [{
        id: 'https://crmm.ru/xapi/courses/sb_mik-3_week1/ms_0_0',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/qplp2988',
        pass_time: '1',
        icon: './static/vshs_icons/mik_week1/0.png',
        color: '#64a7db',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_mik-3_week1/ms_0_1',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/z4knz33u',
        pass_time: '1',
        icon: './static/vshs_icons/mik_week1/1.png',
        color: '#64a7db',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_mik-3_week1/ms_0_2',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/rcenc3df',
        pass_time: '1',
        icon: './static/vshs_icons/mik_week1/2.png',
        color: '#64a7db',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_mik-3_week1/ms_0_3',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/8dcdj4wh',
        pass_time: '1',
        icon: './static/vshs_icons/mik_week1/3.png',
        color: '#64a7db',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_mik-3_week1/ms_0_4',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/0677j8gd',
        pass_time: '1',
        icon: './static/vshs_icons/mik_week1/4.png',
        color: '#64a7db',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_mik-3_week1/ms_0_5',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/a96akh7n',
        pass_time: '1',
        icon: './static/vshs_icons/mik_week1/5.png',
        color: '#64a7db',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_mik-3_week1/ms_0_6',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/y1h8wbur',
        pass_time: '1',
        icon: './static/vshs_icons/mik_week1/6.png',
        color: '#64a7db',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_mik-3_week1/ms_0_7',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/5redtyoa',
        pass_time: '1',
        icon: './static/vshs_icons/mik_week1/7.png',
        color: '#64a7db',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_mik-3_week1/ms_0_8',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/kxm80uw2',
        pass_time: '1',
        icon: './static/vshs_icons/mik_week1/8.png',
        color: '#64a7db',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_mik-3_week1/ms_0_9',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/s4klv22w',
        pass_time: '1',
        icon: './static/vshs_icons/global/final_test_new.png',
        color: '#64a7db',
        scoped: true
    }]
};
