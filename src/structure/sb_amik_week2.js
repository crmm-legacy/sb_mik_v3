import module_0 from './sb_amik_week2/module_0.js';
import module_1 from './sb_amik_week2/module_1.js';
import module_2 from './sb_amik_week2/module_2.js';
import module_3 from './sb_amik_week2/module_3.js';
import module_4 from './sb_amik_week2/module_4.js';
import module_5 from './sb_amik_week2/module_5.js';
import module_6 from './sb_amik_week2/module_6.js';
import module_7 from './sb_amik_week2/module_7.js';

export default {
    id: 'https://crmm.ru/xapi/courses/sb_amik_week2',
    name: 'Комплексная программа обучения Менеджера по сделкам с недвижимостью АМИК Неделя 2',
    type: 'course',
    version: 1,
    virtual: 'https://crmm.ru/xapi/courses/sb_mik-3',
    config: {
        color: 'green'
    },
    items: [
        module_0,
        module_1,
        module_2,
        module_3,
        module_4,
        module_5,
        module_6,
        module_7
    ],
    objectives: [{
        id: 'https://crmm.ru/xapi/courses/sb_amik_week2/ms_0_0',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/pis1kgqm',
        pass_time: '1',
        icon: './static/vshs_icons/sb_amik_week2/0.png',
        color: '#64a7db',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_amik_week2/ms_0_1',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/dfu3p7pr',
        pass_time: '1',
        icon: './static/vshs_icons/sb_amik_week2/1.png',
        color: '#64a7db',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_amik_week2/ms_0_2',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/dfu3p7pr',
        pass_time: '1',
        icon: './static/vshs_icons/sb_amik_week2/2.png',
        color: '#64a7db',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_amik_week2/ms_0_3',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/s3cy6fvy',
        pass_time: '1',
        icon: './static/vshs_icons/sb_amik_week2/3.png',
        color: '#64a7db',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_amik_week2/ms_0_4',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/9fovij9d',
        pass_time: '1',
        icon: './static/vshs_icons/sb_amik_week2/4.png',
        color: '#64a7db',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_amik_week2/ms_0_5',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/2mpzv6c3',
        pass_time: '1',
        icon: './static/vshs_icons/sb_amik_week2/5.png',
        color: '#64a7db',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_amik_week2/ms_0_6',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/y5eyt6p1',
        pass_time: '1',
        icon: './static/vshs_icons/sb_amik_week2/6.png',
        color: '#64a7db',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_amik_week2/ms_0_7',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/9tzjl904',
        pass_time: '1',
        icon: './static/vshs_icons/global/final_test_new.png',
        color: '#64a7db',
        scoped: true
    }]
};
