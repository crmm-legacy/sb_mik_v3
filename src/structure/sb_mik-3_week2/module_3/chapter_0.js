import video_t8m11ubx from '@items/video/video_t8m11ubx.js';
import pdf_oia2v965 from '@items/pdf/pdf_oia2v965.js';
import test_4dh7yqoc from '@items/test/test_4dh7yqoc.js';

export default {
    id: '/whdvs6ls',
    name: 'Покупка доли и комнаты',
    type: 'item',
    component: 'chapter',
    oldId: '/23/0',
    items: [
        video_t8m11ubx,
        pdf_oia2v965,
        test_4dh7yqoc
    ]
};
