import video_o3jjd5tk from '@items/video/video_o3jjd5tk.js';
import test_9zbpz4kw from '@items/test/test_9zbpz4kw.js';

export default {
    id: '/9mfr2eoe',
    name: 'Визуальная оценка',
    type: 'item',
    component: 'chapter',
    oldId: '/22/2',
    items: [
        video_o3jjd5tk,
        test_9zbpz4kw
    ]
};
