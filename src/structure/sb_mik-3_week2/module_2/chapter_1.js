import video_7w9zukwa from '@items/video/video_7w9zukwa.js';
import test_mj2h2upg from '@items/test/test_mj2h2upg.js';

export default {
    id: '/27dk05cs',
    name: 'Доверенность от продавца и покупателя',
    type: 'item',
    component: 'chapter',
    oldId: '/22/1',
    items: [
        video_7w9zukwa,
        test_mj2h2upg
    ]
};
