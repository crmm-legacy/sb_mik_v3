import video_ye29nd8e from '@items/video/video_ye29nd8e.js';
import test_aovlgp46 from '@items/test/test_aovlgp46.js';

export default {
    id: '/z28lo2g2',
    name: 'Ипотека с господдержкой',
    type: 'item',
    component: 'chapter',
    oldId: '/21/1',
    items: [
        video_ye29nd8e,
        test_aovlgp46
    ]
};
