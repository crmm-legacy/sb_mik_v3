import video_gin3a90w from '@items/video/video_gin3a90w.js';
import test_qnw9t877 from '@items/test/test_qnw9t877.js';

export default {
    id: '/0w9ed7il',
    name: 'Субсидирование',
    type: 'item',
    component: 'chapter',
    oldId: '/21/2',
    items: [
        video_gin3a90w,
        test_qnw9t877
    ]
};
