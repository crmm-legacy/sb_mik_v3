import video_rwsi6tet from '@items/video/video_rwsi6tet.js';
import test_lg8y9fch from '@items/test/test_lg8y9fch.js';

export default {
    id: '/l2d31b1p',
    name: 'Преимущества одобрения объекта на сайте Домклик',
    type: 'item',
    component: 'chapter',
    oldId: '/21/0',
    items: [
        video_rwsi6tet,
        test_lg8y9fch
    ]
};
