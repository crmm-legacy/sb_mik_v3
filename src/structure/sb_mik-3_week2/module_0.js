import chapter_0 from './module_0/chapter_0';
import chapter_1 from './module_0/chapter_1';
import chapter_2 from './module_0/chapter_2';

export default {
    id: '/tcloclw4',
    name: 'Продукты (уровень 2)',
    type: 'item',
    component: 'chapter',
    config: {},
    oldId: '/20',
    items: [
        chapter_0,
        chapter_1,
        chapter_2
    ]
};
