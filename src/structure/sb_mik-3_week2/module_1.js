import chapter_0 from './module_1/chapter_0';
import chapter_1 from './module_1/chapter_1';
import chapter_2 from './module_1/chapter_2';

export default {
    id: '/k5gyxswj',
    name: 'Особые условия и акции (уровень 2)',
    type: 'item',
    component: 'chapter',
    config: {},
    oldId: '/21',
    items: [
        chapter_0,
        chapter_1,
        chapter_2
    ]
};
