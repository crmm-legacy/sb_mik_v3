import chapter_0 from './module_3/chapter_0';

export default {
    id: '/oz4g9p1r',
    name: 'Особенности сделок',
    type: 'item',
    component: 'chapter',
    config: {},
    oldId: '/23',
    items: [
        chapter_0
    ]
};
