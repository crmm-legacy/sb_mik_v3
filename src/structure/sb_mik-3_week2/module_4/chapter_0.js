import video_6rv7x4xv from '@items/video/video_6rv7x4xv.js';
import test_8xqhn3hf from '@items/test/test_8xqhn3hf.js';

export default {
    id: '/ezz7lhfx',
    name: 'Прямая и альтернативная сделки',
    type: 'item',
    component: 'chapter',
    oldId: '/24/0',
    items: [
        video_6rv7x4xv,
        test_8xqhn3hf
    ]
};
