import chapter_0 from './module_5/chapter_0';

export default {
    id: '/bvkt5rve',
    name: 'Итоговое тестирование',
    type: 'item',
    component: 'chapter',
    config: {
        finalTest: true
    },
    oldId: '/25',
    items: [
        chapter_0
    ]
};
