import video_i8m8l6jv from '@items/video/video_i8m8l6jv.js';
import pdf_0st7y0s9 from '@items/pdf/pdf_0st7y0s9.js';
import test_1u5ontk6 from '@items/test/test_1u5ontk6.js';

export default {
    id: '/s61j8sh3',
    name: 'Нецелевой кредит под залог',
    type: 'item',
    component: 'chapter',
    oldId: '/20/2',
    items: [
        video_i8m8l6jv,
        pdf_0st7y0s9,
        test_1u5ontk6
    ]
};
