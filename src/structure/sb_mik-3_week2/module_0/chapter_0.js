import video_b0xvbrgv from '@items/video/video_b0xvbrgv.js';
import pdf_74rymn6u from '@items/pdf/pdf_74rymn6u.js';
import test_o6uzp13h from '@items/test/test_o6uzp13h.js';

export default {
    id: '/ftgfev5w',
    name: 'Загородная недвижимость',
    type: 'item',
    component: 'chapter',
    oldId: '/20/0',
    items: [
        video_b0xvbrgv,
        pdf_74rymn6u,
        test_o6uzp13h
    ]
};
