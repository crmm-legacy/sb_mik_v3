import final_test_3nco570s from '@items/test/final_test_3nco570s.js';

export default {
    id: '/zykzqgas',
    name: 'Итоговый тест',
    type: 'item',
    component: 'chapter',
    items: [
        final_test_3nco570s
    ]
};
