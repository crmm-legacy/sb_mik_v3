import chapter_0 from './module_5/chapter_0';

export default {
    id: '/7rrfmw7l',
    name: 'Итоговое тестирование',
    type: 'item',
    component: 'chapter',
    config: {
        finalTest: true
    },
    items: [
        chapter_0
    ]
};
