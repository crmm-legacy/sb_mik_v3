import video_zaqqvdwg from '@items/video/video_zaqqvdwg.js';
import test_wqgmt1l4 from '@items/test/test_wqgmt1l4.js';

export default {
    id: '/bqw2nrsi',
    name: 'Анализ отчета об оценке. Часть 2',
    type: 'item',
    component: 'chapter',
    items: [
        video_zaqqvdwg,
        test_wqgmt1l4
    ]
};
