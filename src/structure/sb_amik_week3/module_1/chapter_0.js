import video_l7jbx8m8 from '@items/video/video_l7jbx8m8.js';
import test_0z2x3v4t from '@items/test/test_0z2x3v4t.js';

export default {
    id: '/fi0q7cli',
    name: 'Анализ отчета об оценке. Часть 1',
    type: 'item',
    component: 'chapter',
    items: [
        video_l7jbx8m8,
        test_0z2x3v4t
    ]
};
