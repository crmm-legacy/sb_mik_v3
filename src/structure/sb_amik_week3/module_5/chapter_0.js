import final_test_boaeml8r from '@items/test/final_test_boaeml8r.js';

export default {
    id: '/q8hsmvxo',
    type: 'item',
    component: 'chapter',
    name: 'Итоговый тест',
    config: {},
    items: [
        final_test_boaeml8r
    ]
};
