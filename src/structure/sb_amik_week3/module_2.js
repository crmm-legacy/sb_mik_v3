import chapter_0 from './module_2/chapter_0';
import chapter_1 from './module_2/chapter_1';
import chapter_2 from './module_2/chapter_2';

export default {
    id: '/g1a6927g',
    name: 'Особенности сделок (уровень 2)',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0,
        chapter_1,
        chapter_2
    ]
};
