import video_lwm1r271 from '@items/video/video_lwm1r271.js';
/* import pdf_ from '@items/pdf/pdf_.js' */
import test_t6ickhq7 from '@items/test/test_t6ickhq7.js';

export default {
    id: '/dqpkdz0r',
    name: 'Нотариальная сделка',
    type: 'item',
    component: 'chapter',
    items: [
        video_lwm1r271,
        /* pdf_, */
        test_t6ickhq7
    ]
};
