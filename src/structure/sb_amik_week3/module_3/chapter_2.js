import video_6oj3xbho from '@items/video/video_6oj3xbho.js';
// import pdf_ from '@items/pdf/pdf_.js'
import test_7nglymzx from '@items/test/test_7nglymzx.js';

export default {
    id: '/p1ykl1zd',
    name: 'УВС',
    type: 'item',
    component: 'chapter',
    items: [
        video_6oj3xbho,
        // pdf_,
        test_7nglymzx
    ]
};
