import video_mz94d6hk from '@items/video/video_mz94d6hk.js';
import pdf_q6816kyq from '@items/pdf/pdf_q6816kyq.js';
import test_zt29248t from '@items/test/test_zt29248t.js';

export default {
    id: '/qcoz6dem',
    name: 'Гараж',
    type: 'item',
    component: 'chapter',
    items: [
        video_mz94d6hk,
        pdf_q6816kyq,
        test_zt29248t
    ]
};
