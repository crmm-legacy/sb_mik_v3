import chapter_0 from './module_4/chapter_0';

export default {
    id: '/eg03i22s',
    name: 'Особые условия и акции (уровень 4)',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0
    ]
};
