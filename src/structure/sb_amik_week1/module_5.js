import chapter_0 from './module_5/chapter_0';

export default {
    id: '/jf5bpdc2',
    name: 'Программы',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0
    ]
};
