import chapter_0 from './module_9/chapter_0';
import chapter_1 from './module_9/chapter_1';
import chapter_2 from './module_9/chapter_2';

export default {
    id: '/rcre18xb',
    name: 'Сервисы',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0,
        chapter_1,
        chapter_2
    ]
};
