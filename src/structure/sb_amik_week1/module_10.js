import chapter_0 from './module_10/chapter_0';

export default {
    id: '/c9iqbhn3',
    name: 'Программы',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0
    ]
};
