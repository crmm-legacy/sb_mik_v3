import video_2qzpgk2q from '@items/video/video_2qzpgk2q.js';
// import pdf_ from '@items/pdf/pdf_.js';
import test_9augcfc7 from '@items/test/test_9augcfc7.js';

export default {
    id: '/zpb2q3pl',
    type: 'item',
    component: 'chapter',
    name: 'Клиенту одобрена сумма меньше запрошенной',
    config: {},
    items: [
        video_2qzpgk2q,
        // pdf_,
        test_9augcfc7
    ]
};
