import video_6xviao91 from '@items/video/video_6xviao91.js';
// import pdf_ from '@items/pdf/pdf_.js';
import test_7uzbb0mw from '@items/test/test_7uzbb0mw.js';

export default {
    id: '/92lmaes0',
    type: 'item',
    component: 'chapter',
    name: 'Подтверждение трудовой занятости',
    config: {},
    items: [
        video_6xviao91,
        // pdf_,
        test_7uzbb0mw
    ]
};
