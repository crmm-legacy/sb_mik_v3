import video_lc08y04h from '@items/video/video_lc08y04h.js';
// import pdf_ from '@items/pdf/pdf_.js';
import test_9miu85fw from '@items/test/test_9miu85fw.js';

export default {
    id: '/q4bssg69',
    type: 'item',
    component: 'chapter',
    name: 'Путь Клиента',
    config: {},
    items: [
        video_lc08y04h,
        // pdf_,
        test_9miu85fw
    ]
};
