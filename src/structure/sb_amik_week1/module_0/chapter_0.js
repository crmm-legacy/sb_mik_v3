
import video_dwsdy9oh from '@items/video/video_dwsdy9oh.js';
// import pdf_ from '@items/pdf/pdf_.js';
import test_qgjkzxwz from '@items/test/test_qgjkzxwz.js';

export default {
    id: '/m0kwxd8g',
    type: 'item',
    component: 'chapter',
    name: 'О курсе и ипотечном кредитовании',
    config: {},
    items: [
        video_dwsdy9oh,
        // pdf_,
        test_qgjkzxwz
    ]
};
