import video_4u4cdclq from '@items/video/video_4u4cdclq.js';
// import pdf_ from '@items/pdf/pdf_.js';
import test_88h8v304 from '@items/test/test_88h8v304.js';

export default {
    id: '/n80zgoes',
    type: 'item',
    component: 'chapter',
    name: 'Сервисная модель',
    config: {},
    items: [
        video_4u4cdclq,
        // pdf_,
        test_88h8v304
    ]
};
