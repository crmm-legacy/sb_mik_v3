import video_eswqkbxo from '@items/video/video_eswqkbxo.js';
// import pdf_ from '@items/pdf/pdf_.js';
import test_gvop93j1 from '@items/test/test_gvop93j1.js';

export default {
    id: '/bxwyfhnx',
    type: 'item',
    component: 'chapter',
    name: 'Работа МИК',
    config: {},
    items: [
        video_eswqkbxo,
        // pdf_,
        test_gvop93j1
    ]
};
