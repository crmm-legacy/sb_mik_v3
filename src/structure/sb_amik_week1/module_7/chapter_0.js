import video_o7vqm8mf from '@items/video/video_o7vqm8mf.js';
// import pdf_ from '@items/pdf/pdf_.js';
import test_ugu1vzlp from '@items/test/test_ugu1vzlp.js';

export default {
    id: '/xnpcx5yx',
    type: 'item',
    component: 'chapter',
    name: 'Подтверждение первоначального взноса',
    config: {},
    items: [
        video_o7vqm8mf,
        // pdf_,
        test_ugu1vzlp
    ]
};
