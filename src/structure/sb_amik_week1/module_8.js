import chapter_0 from './module_8/chapter_0';
import chapter_1 from './module_8/chapter_1';

export default {
    id: '/0me69cnr',
    name: 'Сервисы',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0,
        chapter_1
    ]
};
