import chapter_0 from './module_13/chapter_0';
import chapter_1 from './module_13/chapter_1';
import chapter_2 from './module_13/chapter_2';
import chapter_3 from './module_13/chapter_3';

export default {
    id: '/tagf96ph',
    name: 'Тренажеры',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0,
        chapter_1,
        chapter_2,
        chapter_3
    ]
};
