import video_91wh03b9 from '@items/video/video_91wh03b9.js';
// import pdf_ from '@items/pdf/pdf_.js';
import test_bdujs276 from '@items/test/test_bdujs276.js';

export default {
    id: '/h0o63bd0',
    type: 'item',
    component: 'chapter',
    name: 'Страхование жизни и здоровья (Защищенный заемщик)',
    config: {},
    items: [
        video_91wh03b9,
        // pdf_,
        test_bdujs276
    ]
};
