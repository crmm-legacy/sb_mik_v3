import video_62gfzy1u from '@items/video/video_62gfzy1u.js';
// import pdf_ from '@items/pdf/pdf_.js';
import test_jtkl5wsa from '@items/test/test_jtkl5wsa.js';

export default {
    id: '/xrtanjgf',
    type: 'item',
    component: 'chapter',
    name: 'Страхование залога',
    config: {},
    items: [
        video_62gfzy1u,
        // pdf_,
        test_jtkl5wsa
    ]
};
