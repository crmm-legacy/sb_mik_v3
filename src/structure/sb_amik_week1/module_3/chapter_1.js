import video_uiom487c from '@items/video/video_uiom487c.js';
import pdf_pew4klrq from '@items/pdf/pdf_pew4klrq.js';
import test_0o40quhw from '@items/test/test_0o40quhw.js';

export default {
    id: '/2y0skmcc',
    type: 'item',
    component: 'chapter',
    name: 'Строящееся жилье',
    config: {},
    items: [
        video_uiom487c,
        pdf_pew4klrq,
        test_0o40quhw
    ]
};
