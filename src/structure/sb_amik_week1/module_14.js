import chapter_0 from './module_14/chapter_0';

export default {
    id: '/qg6t6vdw',
    name: 'Итоговое тестирование',
    type: 'item',
    component: 'chapter',
    config: {
        finalTest: true
    },
    items: [
        chapter_0
    ]
};
