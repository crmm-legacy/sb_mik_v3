import video_d83yj0ie from '@items/video/video_d83yj0ie.js';
// import pdf_ from '@items/pdf/pdf_.js';
import test_nt29t2nk from '@items/test/test_nt29t2nk.js';

export default {
    id: '/y1nm5c3s',
    type: 'item',
    component: 'chapter',
    name: 'Сейфовая ячейка',
    config: {},
    items: [
        video_d83yj0ie,
        // pdf_,
        test_nt29t2nk
    ]
};
