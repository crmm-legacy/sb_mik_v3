import video_2v1j2whb from '@items/video/video_2v1j2whb.js';
// import pdf_ from '@items/pdf/pdf_.js';
import test_f8x5w2c4 from '@items/test/test_f8x5w2c4.js';

export default {
    id: '/81ttdklo',
    type: 'item',
    component: 'chapter',
    name: 'Требования к участникам сделки',
    config: {},
    items: [
        video_2v1j2whb,
        // pdf_,
        test_f8x5w2c4
    ]
};
