import video_f2tdgfyj from '@items/video/video_f2tdgfyj.js';
// import pdf_ from '@items/pdf/pdf_.js';
import test_x53c1hfb from '@items/test/test_x53c1hfb.js';

export default {
    id: '/ieokjckk',
    type: 'item',
    component: 'chapter',
    name: 'Обеспечение ипотечного кредита',
    config: {},
    items: [
        video_f2tdgfyj,
        // pdf_,
        test_x53c1hfb
    ]
};
