import video_5bn3fizq from '@items/video/video_5bn3fizq.js';
// import pdf_ from '@items/pdf/pdf_.js';
import test_ngxld3rc from '@items/test/test_ngxld3rc.js';

export default {
    id: '/5smpzvex',
    type: 'item',
    component: 'chapter',
    name: 'Участники сделки',
    config: {},
    items: [
        video_5bn3fizq,
        // pdf_,
        test_ngxld3rc
    ]
};
