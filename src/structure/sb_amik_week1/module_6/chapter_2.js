import video_nlijrz3s from '@items/video/video_nlijrz3s.js';
// import pdf_ from '@items/pdf/pdf_.js';
import test_pf1jmr3x from '@items/test/test_pf1jmr3x.js';

export default {
    id: '/dmicirwp',
    type: 'item',
    component: 'chapter',
    name: 'Жилищный кредит по 2 документам',
    config: {},
    items: [
        video_nlijrz3s,
        // pdf_,
        test_pf1jmr3x
    ]
};
