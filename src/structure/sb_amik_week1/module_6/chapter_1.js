import video_2pfh6ani from '@items/video/video_2pfh6ani.js';
// import pdf_ from '@items/pdf/pdf_.js';
import test_gjuvrr6c from '@items/test/test_gjuvrr6c.js';

export default {
    id: '/sjem1djk',
    type: 'item',
    component: 'chapter',
    name: 'Ипотека + материнский капитал',
    config: {},
    items: [
        video_2pfh6ani,
        // pdf_,
        test_gjuvrr6c
    ]
};
