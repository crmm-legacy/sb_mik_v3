import video_ovx7q17o from '@items/video/video_ovx7q17o.js';
import video_xi92mrzq from '@items/video/video_xi92mrzq.js';
import pdf_x50k7pii from '@items/pdf/pdf_x50k7pii.js';
import test_829y1dd9 from '@items/test/test_829y1dd9.js';

export default {
    id: '/cv9ikvea',
    type: 'item',
    component: 'chapter',
    name: 'Оценка объекта недвижимости',
    config: {},
    items: [
        video_ovx7q17o,
        video_xi92mrzq,
        pdf_x50k7pii,
        test_829y1dd9
    ]
};
