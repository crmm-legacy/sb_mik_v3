import video_32ys3z8o from '@items/video/video_32ys3z8o.js';
import video_icapy0l5 from '@items/video/video_icapy0l5.js';
import pdf_2iy17rag from '@items/pdf/pdf_2iy17rag.js';
import test_0hyhwnu1 from '@items/test/test_0hyhwnu1.js';

export default {
    id: '/tooibp7n',
    type: 'item',
    component: 'chapter',
    name: 'Правовая экспертиза',
    config: {},
    items: [
        video_32ys3z8o,
        video_icapy0l5,
        pdf_2iy17rag,
        test_0hyhwnu1
    ]
};
