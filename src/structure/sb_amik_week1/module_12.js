import chapter_0 from './module_12/chapter_0';

export default {
    id: '/apjrdpon',
    name: 'Стандарты обслуживания клиентов',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0
    ]
};
