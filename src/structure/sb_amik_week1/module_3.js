import chapter_0 from './module_3/chapter_0';
import chapter_1 from './module_3/chapter_1';

export default {
    id: '/y7yqen69',
    name: 'Продукты',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0,
        chapter_1
    ]
};
