import video_s7jj7dr5 from '@items/video/video_s7jj7dr5.js';
// import pdf_ from '@items/pdf/pdf_.js';
import test_lb0tqfvm from '@items/test/test_lb0tqfvm.js';

export default {
    id: '/84gkw1yg',
    name: 'Одобрение объекта недвижимости',
    type: 'item',
    component: 'chapter',
    items: [
        video_s7jj7dr5,
        // pdf_,
        test_lb0tqfvm
    ]
};
