import chapter_0 from './module_6/chapter_0';
import chapter_1 from './module_6/chapter_1';
import chapter_2 from './module_6/chapter_2';

export default {
    id: '/a76gwnwm',
    name: 'Особые условия и акции',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0,
        chapter_1,
        chapter_2
    ]
};
