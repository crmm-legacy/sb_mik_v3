import chapter_0 from './module_1/chapter_0';
import chapter_1 from './module_1/chapter_1';
import chapter_2 from './module_1/chapter_2';

export default {
    id: '/v4qca4vt',
    name: 'Участники и обеспечение',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0,
        chapter_1,
        chapter_2
    ]
};
