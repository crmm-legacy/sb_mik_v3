import chapter_0 from './module_0/chapter_0';
import chapter_1 from './module_0/chapter_1';
import chapter_2 from './module_0/chapter_2';
import chapter_3 from './module_0/chapter_3';

export default {
    id: '/h4t6uc38',
    name: 'Введение',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0,
        chapter_1,
        chapter_2,
        chapter_3
    ]
};
