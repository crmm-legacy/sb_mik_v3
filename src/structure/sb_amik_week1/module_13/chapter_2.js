import train_in15k5db from '@items/train/train_in15k5db.js';
import train_wjfhwyes from '@items/train/train_wjfhwyes.js';
import train_fxhke631 from '@items/train/train_fxhke631.js';
import train_xqu94tl4 from '@items/train/train_xqu94tl4.js';
import train_5tjvsgvb from '@items/train/train_5tjvsgvb.js';
import train_09figpdw from '@items/train/train_09figpdw.js';

export default {
    id: '/3qolzfri',
    type: 'item',
    component: 'chapter',
    name: 'Тренажеры',
    config: {},
    items: [
        train_in15k5db,
        train_wjfhwyes,
        train_fxhke631,
        train_xqu94tl4,
        train_5tjvsgvb,
        train_09figpdw
    ]
};
