import train_4316sj1s from '@items/train/train_4316sj1s.js';
import train_3vyk13vj from '@items/train/train_3vyk13vj.js';
import train_qvsyvlx8 from '@items/train/train_qvsyvlx8.js';
import train_iabyl6vv from '@items/train/train_iabyl6vv.js';
import train_m6mqqmfk from '@items/train/train_m6mqqmfk.js';
import train_20fx003i from '@items/train/train_20fx003i.js';

export default {
    id: '/pee82zvv',
    type: 'item',
    component: 'chapter',
    name: 'Тренажеры',
    config: {},
    items: [
        train_4316sj1s,
        train_3vyk13vj,
        train_qvsyvlx8,
        train_iabyl6vv,
        train_m6mqqmfk,
        train_20fx003i
    ]
};
