import final_test_tv0qcfc0 from '@items/test/final_test_tv0qcfc0.js';

export default {
    id: '/bmwn3aws',
    type: 'item',
    component: 'chapter',
    name: 'Итоговый тест',
    config: {},
    items: [
        final_test_tv0qcfc0
    ]
};
