import chapter_0 from './module_5/chapter_0';

export default {
    id: '/2mpzv6c3',
    name: 'Особые условия и акции (уровень 2)',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0
    ]
};
