import chapter_0 from './module_7/chapter_0';

export default {
    id: '/9tzjl904',
    name: 'Итоговое тестирование',
    type: 'item',
    component: 'chapter',
    config: {
        finalTest: true
    },
    items: [
        chapter_0
    ]
};
