import chapter_0 from './module_3/chapter_0';

export default {
    id: '/s3cy6fvy',
    name: 'Документы (уровень 2)',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0
    ]
};
