import final_test_s9a45fsd from '@items/test/final_test_s9a45fsd.js';

export default {
    id: '/6077jq82',
    type: 'item',
    component: 'chapter',
    name: 'Итоговый тест',
    config: {},
    items: [
        final_test_s9a45fsd
    ]
};
