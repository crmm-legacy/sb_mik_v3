import chapter_0 from './module_6/chapter_0';

export default {
    id: '/y5eyt6p1',
    name: 'Особенности сделок',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0
    ]
};
