import video_ye29nd8e from '@items/video/video_ye29nd8e.js';
// import pdf_ from '@items/pdf/pdf_.js';
import test_aovlgp46 from '@items/test/test_aovlgp46.js';

export default {
    id: '/uv874glh',
    name: 'Ипотека с господдержкой',
    type: 'item',
    component: 'chapter',
    items: [
        video_ye29nd8e,
        // pdf_,
        test_aovlgp46
    ]
};
