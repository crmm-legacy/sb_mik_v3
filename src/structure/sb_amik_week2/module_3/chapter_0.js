import video_7w9zukwa from '@items/video/video_7w9zukwa.js';
// import pdf_ from '@items/pdf/pdf_.js';
import test_mj2h2upg from '@items/test/test_mj2h2upg.js';

export default {
    id: '/k2dw6zce',
    name: 'Доверенность от продавца и покупателя',
    type: 'item',
    component: 'chapter',
    items: [
        video_7w9zukwa,
        // pdf_,
        test_mj2h2upg
    ]
};
