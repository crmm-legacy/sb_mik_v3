import video_6rv7x4xv from '@items/video/video_6rv7x4xv.js';
// import pdf_ from '@items/pdf/pdf_.js';
import test_8xqhn3hf from '@items/test/test_8xqhn3hf.js';

export default {
    id: '/064wcati',
    name: 'Прямая и альтернативная сделки',
    type: 'item',
    component: 'chapter',
    items: [
        video_6rv7x4xv,
        // pdf_,
        test_8xqhn3hf
    ]
};
