import chapter_0 from './module_2/chapter_0';

export default {
    id: '/dfu3p7pr',
    name: 'Особые условия и акции (уровень 2)',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0
    ]
};
