import video_gin3a90w from '@items/video/video_gin3a90w.js';
// import pdf_ from '@items/pdf/pdf_.js';
import test_qnw9t877 from '@items/test/test_qnw9t877.js';

export default {
    id: '/tqt64rui',
    name: 'Субсидирование',
    type: 'item',
    component: 'chapter',
    items: [
        video_gin3a90w,
        // pdf_,
        test_qnw9t877
    ]
};
