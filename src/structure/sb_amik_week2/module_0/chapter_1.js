import video_nz1s897n from '@items/video/video_nz1s897n.js';
import pdf_2srrwanf from '@items/pdf/pdf_2srrwanf.js';
import test_n83l3o4h from '@items/test/test_n83l3o4h.js';

export default {
    id: '/zfzt0qca',
    name: 'Рефинансирование',
    type: 'item',
    component: 'chapter',
    items: [
        video_nz1s897n,
        pdf_2srrwanf,
        test_n83l3o4h
    ]
};
