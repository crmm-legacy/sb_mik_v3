import video_rwsi6tet from '@items/video/video_rwsi6tet.js';
// import pdf_ from '@items/pdf/pdf_.js';
import test_lg8y9fch from '@items/test/test_lg8y9fch.js';

export default {
    id: '/bmlo7z9j',
    name: 'Преимущества одобрения объекта на сайте ДомКлик',
    type: 'item',
    component: 'chapter',
    items: [
        video_rwsi6tet,
        // pdf_,
        test_lg8y9fch
    ]
};
