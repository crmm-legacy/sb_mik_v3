import module_0 from './sb_mik-3_week3/module_0.js';
import module_1 from './sb_mik-3_week3/module_1.js';
import module_2 from './sb_mik-3_week3/module_2.js';
import module_3 from './sb_mik-3_week3/module_3.js';
import module_4 from './sb_mik-3_week3/module_4.js';
import module_5 from './sb_mik-3_week3/module_5.js';

export default {
    id: 'https://crmm.ru/xapi/courses/sb_mik-3_week3',
    name: 'Комплексная программа обучения МИК. Неделя 3',
    type: 'course',
    version: 1,
    virtual: 'https://crmm.ru/xapi/courses/sb_mik-3',
    config: {
        color: 'red'
    },
    items: [
        module_0,
        module_1,
        module_2,
        module_3,
        module_4,
        module_5
    ],
    objectives: [{
        id: 'https://crmm.ru/xapi/courses/sb_mik-3_week3/ms_0_0',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/8zh2yl6k',
        pass_time: '1',
        icon: './static/vshs_icons/mik_week3/0.png',
        color: '#ea6c6c',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_mik-3_week3/ms_0_1',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/d190f32a',
        pass_time: '1',
        icon: './static/vshs_icons/mik_week3/1.png',
        color: '#ea6c6c',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_mik-3_week3/ms_0_2',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/ok5zqziw',
        pass_time: '1',
        icon: './static/vshs_icons/mik_week3/2.png',
        color: '#ea6c6c',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_mik-3_week3/ms_0_3',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/cxybsu2j',
        pass_time: '1',
        icon: './static/vshs_icons/mik_week3/3.png',
        color: '#ea6c6c',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_mik-3_week3/ms_0_4',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/oxb3ngow',
        pass_time: '1',
        icon: './static/vshs_icons/mik_week3/4.png',
        color: '#ea6c6c',
        scoped: true
    },
    {
        id: 'https://crmm.ru/xapi/courses/sb_mik-3_week3/ms_0_5',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/j3df69s4',
        pass_time: '1',
        icon: './static/vshs_icons/global/final_test_new.png',
        color: '#ea6c6c',
        scoped: true
    }
    ]
};
