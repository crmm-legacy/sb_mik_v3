import chapter_0 from './module_2/chapter_0';
import chapter_1 from './module_2/chapter_1';

export default {
    id: '/rcenc3df',
    name: 'Ипотечные программы',
    type: 'item',
    component: 'chapter',
    config: {},
    oldId: '/2',
    items: [
        chapter_0,
        chapter_1
    ]
};
