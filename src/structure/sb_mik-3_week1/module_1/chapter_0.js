import video_ulg8sjc8 from '@items/video/video_ulg8sjc8.js';
import test_5dl67yc1 from '@items/test/test_5dl67yc1.js';

export default {
    id: '/sv9opoyz',
    type: 'item',
    component: 'chapter',
    name: 'Подача заявки на ипотечный кредит',
    config: {},
    oldId: '/1/0',
    items: [
        video_ulg8sjc8,
        test_5dl67yc1
    ]
};
