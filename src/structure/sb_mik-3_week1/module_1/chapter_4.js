import video_2qzpgk2q from '@items/video/video_2qzpgk2q.js';
import test_9augcfc7 from '@items/test/test_9augcfc7.js';

export default {
    id: '/9d4z3ov7',
    type: 'item',
    component: 'chapter',
    name: 'Клиенту одобрена сумма меньше запрошенной',
    config: {},
    oldId: '/1/4',
    items: [
        video_2qzpgk2q,
        test_9augcfc7
    ]
};
