import video_6xviao91 from '@items/video/video_6xviao91.js';
import test_7uzbb0mw from '@items/test/test_7uzbb0mw.js';

export default {
    id: '/91xitnw8',
    type: 'item',
    component: 'chapter',
    name: 'Подтверждение трудовой занятости',
    config: {},
    oldId: '/1/3',
    items: [
        video_6xviao91,
        test_7uzbb0mw
    ]
};
