import train_7i70u56l from '@items/train/train_7i70u56l.js';
import train_dtan3w6d from '@items/train/train_dtan3w6d.js';
import train_6a6dw1qh from '@items/train/train_6a6dw1qh.js';
import train_23c8ziv6 from '@items/train/train_23c8ziv6.js';
import train_q5lhnx9u from '@items/train/train_q5lhnx9u.js';
import train_xev68pfa from '@items/train/train_xev68pfa.js';

export default {
    id: '/flx1pdbc',
    type: 'item',
    component: 'chapter',
    name: 'Тренажеры',
    config: {},
    items: [
        train_7i70u56l,
        train_dtan3w6d,
        train_6a6dw1qh,
        train_23c8ziv6,
        train_q5lhnx9u,
        train_xev68pfa
    ]
};
