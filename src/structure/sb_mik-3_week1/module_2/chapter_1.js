import video_awhxizga from '@items/video/video_awhxizga.js';
import pdf_8i2s1pmd from '@items/pdf/pdf_8i2s1pmd.js';
import test_u7o16god from '@items/test/test_u7o16god.js';

export default {
    id: '/en8s5skj',
    type: 'item',
    component: 'chapter',
    name: 'Готовое жилье',
    config: {},
    oldId: '/2/1',
    items: [
        video_awhxizga,
        pdf_8i2s1pmd,
        test_u7o16god
    ]
};
