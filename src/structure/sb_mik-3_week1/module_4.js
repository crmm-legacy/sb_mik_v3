import chapter_0 from './module_4/chapter_0';

export default {
    id: '/0677j8gd',
    name: 'Документы',
    type: 'item',
    component: 'chapter',
    config: {},
    oldId: '/4',
    items: [
        chapter_0
    ]
};
