import chapter_0 from './module_6/chapter_0';
import chapter_1 from './module_6/chapter_1';

export default {
    id: '/y1h8wbur',
    name: 'Способы расчета',
    type: 'item',
    component: 'chapter',
    config: {},
    oldId: '/6',
    items: [
        chapter_0,
        chapter_1
    ]
};
