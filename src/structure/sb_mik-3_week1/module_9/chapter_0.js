import final_test_5zi29wg3 from '@items/test/final_test_5zi29wg3.js';

export default {
    id: '/4lds6din',
    type: 'item',
    component: 'chapter',
    name: 'Итоговый тест',
    config: {},
    items: [
        final_test_5zi29wg3
    ]
};
