import video_zl7o7i1d from '@items/video/video_zl7o7i1d.js';
import video_rxhmi903 from '@items/video/video_rxhmi903.js';
// import video_d6w5jgn3 from '@items/video/video_d6w5jgn3.js';

export default {
    id: '/0vevt591',
    type: 'item',
    component: 'chapter',
    name: 'Программы (Партнер онлайн, Транзакт)',
    config: {},
    oldId: '/5/5',
    items: [
        video_zl7o7i1d,
        video_rxhmi903,
        // video_d6w5jgn3
    ]
};
