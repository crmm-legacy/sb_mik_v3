import video_ovx7q17o from '@items/video/video_ovx7q17o.js';
import video_xi92mrzq from '@items/video/video_xi92mrzq.js';
import pdf_x50k7pii from '@items/pdf/pdf_x50k7pii.js';
import test_829y1dd9 from '@items/test/test_829y1dd9.js';

export default {
    id: '/5ogjpcwm',
    type: 'item',
    component: 'chapter',
    name: 'Оценка объекта недвижимости',
    config: {},
    oldId: '/5/2',
    items: [
        video_ovx7q17o,
        video_xi92mrzq,
        pdf_x50k7pii,
        test_829y1dd9
    ]
};
