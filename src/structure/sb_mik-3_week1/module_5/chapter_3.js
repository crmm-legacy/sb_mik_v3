import video_zz94tep6 from '@items/video/video_zz94tep6.js';
import video_48c7tk2w from '@items/video/video_48c7tk2w.js';
import pdf_29jl23ha from '@items/pdf/pdf_29jl23ha.js';
import test_3jab0fsf from '@items/test/test_3jab0fsf.js';

export default {
    id: '/u680isna',
    type: 'item',
    component: 'chapter',
    name: 'Электронная регистрация',
    config: {},
    oldId: '/5/3',
    items: [
        video_zz94tep6,
        video_48c7tk2w,
        pdf_29jl23ha,
        test_3jab0fsf
    ]
};
