import video_91wh03b9 from '@items/video/video_91wh03b9.js';
import test_bdujs276 from '@items/test/test_bdujs276.js';

export default {
    id: '/kab16qik',
    type: 'item',
    component: 'chapter',
    name: 'Страхование жизни и здоровья (Защищенный заемщик)',
    config: {},
    oldId: '/5/0',
    items: [
        video_91wh03b9,
        test_bdujs276
    ]
};
