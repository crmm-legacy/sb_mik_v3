import chapter_0 from './module_9/chapter_0';

export default {
    id: '/s4klv22w',
    name: 'Итоговое тестирование',
    type: 'item',
    component: 'chapter',
    config: {
        finalTest: true
    },
    oldId: '/7',
    items: [
        chapter_0
    ]
};
