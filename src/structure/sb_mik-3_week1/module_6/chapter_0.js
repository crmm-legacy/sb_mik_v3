import video_7w89e4k6 from '@items/video/video_7w89e4k6.js';
import video_kmpvcfj6 from '@items/video/video_kmpvcfj6.js';
import pdf_k55wr0om from '@items/pdf/pdf_k55wr0om.js';
import test_hvciy8co from '@items/test/test_hvciy8co.js';

export default {
    id: '/mgbg2sgf',
    type: 'item',
    component: 'chapter',
    name: 'Сервис безопасных расчетов',
    config: {},
    oldId: '/6/0',
    items: [
        video_7w89e4k6,
        video_kmpvcfj6,
        pdf_k55wr0om,
        test_hvciy8co
    ]
};
