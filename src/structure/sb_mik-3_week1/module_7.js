import chapter_0 from './module_7/chapter_0';
import chapter_1 from './module_7/chapter_1';
import chapter_2 from './module_7/chapter_2';

export default {
    id: '/5redtyoa',
    name: 'Стандарты обслуживания клиентов',
    type: 'item',
    component: 'chapter',
    config: {},
    oldId: '/8',
    items: [
        chapter_0,
        chapter_1,
        chapter_2
    ]
};
