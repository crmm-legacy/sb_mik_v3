import chapter_0 from './module_8/chapter_0';
import chapter_1 from './module_8/chapter_1';
import chapter_2 from './module_8/chapter_2';

export default {
    id: '/kxm80uw2',
    name: 'АС «Единый кредитный портфель»',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0,
        chapter_1,
        chapter_2
    ]
};
