import chapter_0 from './module_3/chapter_0';
import chapter_1 from './module_3/chapter_1';
import chapter_2 from './module_3/chapter_2';

export default {
    id: '/8dcdj4wh',
    name: 'Особые условия и акции',
    type: 'item',
    component: 'chapter',
    config: {},
    oldId: '/3',
    items: [
        chapter_0,
        chapter_1,
        chapter_2
    ]
};
