import video_p7z8613v from '@items/video/video_p7z8613v.js';
import test_ixxp3uaa from '@items/test/test_ixxp3uaa.js';

export default {
    id: '/052wcark',
    type: 'item',
    component: 'chapter',
    name: 'Стандарты общения с Клиентом по телефону',
    config: {},
    oldId: '/8/0',
    items: [
        video_p7z8613v,
        test_ixxp3uaa
    ]
};
