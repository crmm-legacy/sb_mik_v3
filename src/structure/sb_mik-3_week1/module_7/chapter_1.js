import video_kab82cb2 from '@items/video/video_kab82cb2.js';
import test_r3r7t5oy from '@items/test/test_r3r7t5oy.js';

export default {
    id: '/b7e4qoja',
    type: 'item',
    component: 'chapter',
    name: 'Выявление причин выставленной Клиентом оценки',
    config: {},
    oldId: '/8/1',
    items: [
        video_kab82cb2,
        test_r3r7t5oy
    ]
};
