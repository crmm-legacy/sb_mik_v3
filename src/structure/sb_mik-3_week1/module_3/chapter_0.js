import video_xekhgoq8 from '@items/video/video_xekhgoq8.js';
import test_pjwl1hlr from '@items/test/test_pjwl1hlr.js';

export default {
    id: '/w7bagifb',
    type: 'item',
    component: 'chapter',
    name: 'Молодая семья',
    config: {},
    oldId: '/3/0',
    items: [
        video_xekhgoq8,
        test_pjwl1hlr
    ]
};
