import video_dwsdy9oh from '@items/video/video_dwsdy9oh.js';
import test_qgjkzxwz from '@items/test/test_qgjkzxwz.js';

export default {
    id: '/4w118pmr',
    type: 'item',
    component: 'chapter',
    name: 'О курсе и ипотечном кредитовании',
    config: {},
    oldId: '/0/0',
    items: [
        video_dwsdy9oh,
        test_qgjkzxwz
    ]
};
