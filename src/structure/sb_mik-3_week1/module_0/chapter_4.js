import video_5bn3fizq from '@items/video/video_5bn3fizq.js';
import test_ngxld3rc from '@items/test/test_ngxld3rc.js';

export default {
    id: '/gqdxo4gj',
    type: 'item',
    component: 'chapter',
    name: 'Участники сделки',
    config: {},
    oldId: '/0/4',
    items: [
        video_5bn3fizq,
        test_ngxld3rc
    ]
};
