import video_f2tdgfyj from '@items/video/video_f2tdgfyj.js';
import test_x53c1hfb from '@items/test/test_x53c1hfb.js';

export default {
    id: '/2axplyqt',
    type: 'item',
    component: 'chapter',
    name: 'Обеспечение ипотечного кредита',
    config: {},
    oldId: '/0/6',
    items: [
        video_f2tdgfyj,
        test_x53c1hfb
    ]
};
