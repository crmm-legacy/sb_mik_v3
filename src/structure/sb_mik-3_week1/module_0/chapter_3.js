import video_eswqkbxo from '@items/video/video_eswqkbxo.js';
import test_gvop93j1 from '@items/test/test_gvop93j1.js';

export default {
    id: '/ojhjk26o',
    type: 'item',
    component: 'chapter',
    name: 'Работа Мик',
    config: {},
    oldId: '/0/3',
    items: [
        video_eswqkbxo,
        test_gvop93j1
    ]
};
