import video_o7vqm8mf from '@items/video/video_o7vqm8mf.js';
import test_ugu1vzlp from '@items/test/test_ugu1vzlp.js';

export default {
    id: '/rw9vnt0b',
    type: 'item',
    component: 'chapter',
    name: 'Подтверждение первоначального взноса',
    config: {},
    oldId: '/4/0',
    items: [
        video_o7vqm8mf,
        test_ugu1vzlp
    ]
};
