import chapter_0 from './module_4/chapter_0';

export default {
    id: '/8xnedizx',
    name: 'Итоговое тестирование',
    type: 'item',
    component: 'chapter',
    config: {
        finalTest: true
    },
    items: [
        chapter_0
    ]
};
