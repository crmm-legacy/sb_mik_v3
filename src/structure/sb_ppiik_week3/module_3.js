import chapter_0 from './module_3/chapter_0';
import chapter_1 from './module_3/chapter_1';
import chapter_2 from './module_3/chapter_2';

export default {
    id: '/kfdhwsfg',
    name: 'Виды сделок (уровень 2)',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0,
        chapter_1,
        chapter_2
    ]
};
