import final_test_s1lrqukk from '@items/test/final_test_s1lrqukk.js';

export default {
    id: '/efhbwe2j',
    name: 'Итоговый тест',
    type: 'item',
    component: 'chapter',
    items: [
        final_test_s1lrqukk
    ]
};
