import video_57j2br07 from '@items/video/video_57j2br07.js';
// import pdf_ from '@items/pdf/pdf_.js';
import test_fikg7w7q from '@items/test/test_fikg7w7q.js';

export default {
    id: '/k5tvnls8',
    name: 'Межрегиональная сделка',
    type: 'item',
    component: 'chapter',
    items: [
        video_57j2br07,
        // pdf_,
        test_fikg7w7q
    ]
};
