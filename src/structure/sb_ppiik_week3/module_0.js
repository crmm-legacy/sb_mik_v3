import chapter_0 from './module_0/chapter_0';
import chapter_1 from './module_0/chapter_1';
import chapter_2 from './module_0/chapter_2';

export default {
    id: '/0xhe7vjp',
    name: 'Продукты (уровень 3)',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0,
        chapter_1,
        chapter_2
    ]
};
