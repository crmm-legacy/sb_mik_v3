import video_afeuetjm from '@items/video/video_afeuetjm.js';
import pdf_eyxkdvp0 from '@items/pdf/pdf_eyxkdvp0.js';
import test_smamddlo from '@items/test/test_smamddlo.js';

export default {
    id: '/bmw5bf2d',
    name: 'Залоговые объекты',
    type: 'item',
    component: 'chapter',
    items: [
        video_afeuetjm,
        pdf_eyxkdvp0,
        test_smamddlo
    ]
};
