import video_zk0jx8fv from '@items/video/video_zk0jx8fv.js';
import pdf_7j3s1cx7 from '@items/pdf/pdf_7j3s1cx7.js';
import test_3nkhx0f4 from '@items/test/test_3nkhx0f4.js';

export default {
    id: '/vafxf99n',
    name: 'Несовершеннолетний в сделке',
    type: 'item',
    component: 'chapter',
    items: [
        video_zk0jx8fv,
        pdf_7j3s1cx7,
        test_3nkhx0f4
    ]
};
