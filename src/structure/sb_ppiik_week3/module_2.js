import chapter_0 from './module_2/chapter_0';
import chapter_1 from './module_2/chapter_1';

export default {
    id: '/heb38iaj',
    name: 'Особенности сделок (уровень 2)',
    type: 'item',
    component: 'chapter',
    config: {},
    items: [
        chapter_0,
        chapter_1
    ]
};
