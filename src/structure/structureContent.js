import sb_mik_week1 from './sb_mik-3_week1';
import sb_mik_week2 from './sb_mik-3_week2';
import sb_mik_week3 from './sb_mik-3_week3';
import sb_moik_week1 from './sb_moik_week1';
import sb_moik_week2 from './sb_moik_week2';
import sb_moik_week3 from './sb_moik_week3';
import sb_ppiik_week1 from './sb_ppiik_week1';
import sb_ppiik_week2 from './sb_ppiik_week2';
import sb_ppiik_week3 from './sb_ppiik_week3';
import sb_amik_week1 from './sb_amik_week1';
import sb_amik_week2 from './sb_amik_week2';
import sb_amik_week3 from './sb_amik_week3';

export default {
    metadata: {
        schemaversion: 1
    },
    config: {
        version: 2,
        debug: false,
        completionThreshold: {
            test: 80,
            pract: 80
        },
        contentGroups: {
            'knowledge': [
                {
                    name: 'Видео',
                    type: 'videoplayer'
                },
                {
                    name: 'Тренажер-теория',
                    type: 'td_compo'
                },
                {
                    name: 'Презентация',
                    type: 'pdf_compo'
                },
                {
                    name: 'Документ на скачивание',
                    type: 'download_compo'
                }
            ],
            'practice': [
                {
                    name: 'Тестирование',
                    type: 'test_compo'
                },
                {
                    name: 'Тренажер-практика',
                    type: 'td_compo'
                }
            ],
            'modules': [
                {
                    name: 'Модуль',
                    type: 'chapter'
                }
            ],
            'cmi.interaction': [
                {
                    name: 'Вопрос',
                    type: 'question'
                }
            ]
        }
    },
    courses: [
        sb_mik_week1,
        sb_mik_week2,
        sb_mik_week3,
        sb_moik_week1,
        sb_moik_week2,
        sb_moik_week3,
        sb_ppiik_week1,
        sb_ppiik_week2,
        sb_ppiik_week3,
        sb_amik_week1,
        sb_amik_week2,
        sb_amik_week3
    ]
};
