import module_0 from './sb_mik-3_week2/module_0.js';
import module_1 from './sb_mik-3_week2/module_1.js';
import module_2 from './sb_mik-3_week2/module_2.js';
import module_3 from './sb_mik-3_week2/module_3.js';
import module_4 from './sb_mik-3_week2/module_4.js';
import module_5 from './sb_mik-3_week2/module_5.js';

export default {
    id: 'https://crmm.ru/xapi/courses/sb_mik-3_week2',
    name: 'Комплексная программа обучения МИК. Неделя 2',
    type: 'course',
    version: 1,
    virtual: 'https://crmm.ru/xapi/courses/sb_mik-3',
    config: {
        color: 'green'
    },
    items: [
        module_0,
        module_1,
        module_2,
        module_3,
        module_4,
        module_5
    ],
    objectives: [{
        id: 'https://crmm.ru/xapi/courses/sb_mik-3_week2/ms_0_0',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/tcloclw4',
        pass_time: '1',
        icon: './static/vshs_icons/mik_week2/0.png',
        color: '#47b27e',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_mik-3_week2/ms_0_1',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/k5gyxswj',
        pass_time: '1',
        icon: './static/vshs_icons/mik_week2/1.png',
        color: '#47b27e',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_mik-3_week2/ms_0_2',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/hmogfnrj',
        pass_time: '1',
        icon: './static/vshs_icons/mik_week2/2.png',
        color: '#47b27e',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_mik-3_week2/ms_0_3',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/oz4g9p1r',
        pass_time: '1',
        icon: './static/vshs_icons/mik_week2/3.png',
        color: '#47b27e',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_mik-3_week2/ms_0_4',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/awbxv8x5',
        pass_time: '1',
        icon: './static/vshs_icons/mik_week2/4.png',
        color: '#47b27e',
        scoped: true
    }, {
        id: 'https://crmm.ru/xapi/courses/sb_mik-3_week2/ms_0_5',
        target: 'https://crmm.ru/xapi/courses/sb_mik-3/bvkt5rve',
        pass_time: '1',
        icon: './static/vshs_icons/global/final_test_new.png',
        color: '#47b27e',
        scoped: true
    }]
};
