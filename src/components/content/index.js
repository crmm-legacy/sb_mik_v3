export { default as MainpageCompo } from './MainpageCompo';
export { default as MenuCompo } from './MenuCompo';
export { default as HeaderCompo } from './HeaderCompo';

export { default as PdfPlayerCompo } from './Pdf/PdfPlayerCompo';
export { default as DownloadCompo } from './Download/DownloadCompo';
export { default as VideoPlayerCompo } from './Video/VideoPlayerCompo';
export { default as TestCompo } from './Test/TestCompo';
export { default as TrainCompo } from './Train/TrainCompo';
