export const CONTENT_MAP = {
    videoplayer: 'VideoPlayerCompo',
    test_compo: 'TestCompo',
    pdf_compo: 'PdfPlayerCompo',
    download_compo: 'DownloadCompo',
    td_compo: 'TrainCompo'
};
