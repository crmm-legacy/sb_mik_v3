export const SCREENS_MAP = {
    feedback: 'FeedbackCompo',
    starmarks: 'StarmarksCompo'
};

export const FEEDBACK_SCREENS_MAP = {
    tech: 'FeedbackTechCompo',
    content: 'FeedbackContentCompo',
    other: 'FeedbackOtherCompo',
    like: 'FeedbackLikeCompo'
};
