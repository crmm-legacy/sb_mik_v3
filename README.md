# sb_mik_v3

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run dev
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### ПП МИК 2.0

[Google Sylesheets Link](https://docs.google.com/spreadsheets/d/1aq_SLQlZEj1I7X8mkhx8EVeZYWbFDs9JERMjgZdXwAU/edit#gid=1682526348)
